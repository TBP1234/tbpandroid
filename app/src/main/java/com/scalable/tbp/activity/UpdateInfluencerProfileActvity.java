package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.ads.InterstitialAd;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.county.Datum;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.UpdateInfuencerProfilerepository;
import com.scalable.tbp.dagger.DaggerUpdateInfluencerProfileScreenComponent;
import com.scalable.tbp.dagger.UpdateInfluencerProfileSreenModule;
import com.scalable.tbp.ui.UpdateInfluencerProfilePresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.SearchableListDialog;
import com.steelkiwi.instagramhelper.InstagramHelper;
import com.steelkiwi.instagramhelper.InstagramHelperConstants;
import com.steelkiwi.instagramhelper.model.InstagramUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class UpdateInfluencerProfileActvity extends AppCompatActivity implements UpdateInfuencerProfilerepository.UpdateInfuencerProfileView, Validator.ValidationListener, View.OnClickListener {


    @Inject
    UpdateInfluencerProfilePresenterImp updateInfluencerProfilePresenterImp;

    @NotEmpty
    @BindView(R.id.edt_firstname)
    EditText edtFirstname;

    @NotEmpty
    @BindView(R.id.edt_lastname)
    EditText edtLastname;


    @BindView(R.id.rbtn_male)
    RadioButton rbtnMale;

    @BindView(R.id.rbtn_female)
    RadioButton rbtnFemale;

    @BindView(R.id.rbtn_other)
    RadioButton rbtnOther;

    @BindView(R.id.rdt_gender)
    RadioGroup rdtGender;

    @NotEmpty
    @BindView(R.id.edt_country)
    EditText edtCountry;


    @NotEmpty
    @BindView(R.id.edt_city)
    EditText edtCity;


    @NotEmpty
    @BindView(R.id.edt_about)
    EditText edtAbout;


    @BindView(R.id.btn_insta)
    Button btnInsta;


    @Min(value = 1,message = "should be greater than 0")
    @BindView(R.id.edt_insta)
    EditText edtInsta;

    @BindView(R.id.btn_facebook)
    Button btnFacebook;


    @Min(value = 1,message = "should be greater than 0")
    @BindView(R.id.edt_facebook)
    EditText edtFacebook;

    @BindView(R.id.btn_snapchat)
    Button btnSnapchat;


    @Min(value = 1,message = "should be greater than 0")
    @BindView(R.id.edt_snapchat)
    EditText edtSnapchat;

    @BindView(R.id.btn_update)
    Button btnupdate;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.edt_snapchatid)
    EditText edtSnapchatid;

    @BindView(R.id.text_insta_var)
    TextView textInstaVar;

    @BindView(R.id.text_fac_var)
    TextView textFacVar;

    @BindView(R.id.text_snap_var)
    TextView textSnapVar;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    @BindView(R.id.chang_instagram_update)
    TextView changInstagramUpdate;

    @BindView(R.id.chang_faceb_update)
    TextView changFacebUpdate;

    @BindView(R.id.chang_snap_update)
    TextView changSnapUpdate;


    private ProgressDialog progressDialog;

    private Validator validator;

    private LoginButton loginButton;

    private CallbackManager callbackManager;

    private AccessTokenTracker accessTokenTracker;

    private String email = "";

    private String name = "";

    private String CLIENT_ID = "14cd98c190644ddea1be3a037d5aa7a4";

    private String REDIRECT_URL = "http://www.impetrosys.com/";

    private InstagramHelper instagramHelper;

    private String profieImage = "";

    private SearchableListDialog _searchableCountryListDialog;

    private SearchableListDialog _searchableCityListDialog;
    private String cityCode;
    private String countryCode;
    private String instagramprofileUrl="";
    private String facebookProfileUrl="";

    @Inject
    Connectivity connectivity;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_influencer_profile);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        ButterKnife.bind(this);

        //inject Dagger
        DaggerUpdateInfluencerProfileScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .updateInfluencerProfileSreenModule(new UpdateInfluencerProfileSreenModule(this))
                .build().inject(this);

        String scope = "basic";
        //scope is for the permissions
        instagramHelper = new InstagramHelper.Builder()
                .withClientId(CLIENT_ID)
                .withRedirectUrl(REDIRECT_URL)
                .withScope(scope)
                .build();

        //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");


        validator = new Validator(this);
        validator.setValidationListener(this);

        init();

        updateInfluencerProfilePresenterImp.getCountry();

        callbackManager = CallbackManager.Factory.create();

        bindevent();


        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                //-------name and email swhow in dialoge box-----------------

                                try {

                                    //email = object.getString("email");
                                    name = object.getString("name");
                                    profieImage = "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large";
                                    facebookProfileUrl=object.getString("link");


                                    if (name.equals("")) {

                                        edtFacebook.setFocusable(false);
                                        edtFacebook.setFocusableInTouchMode(false); // user touches widget on phone with touch screen
                                        edtFacebook.setClickable(false); // use

                                    } else {

                                        textFacVar.setVisibility(View.VISIBLE);

                                        edtFacebook.setFocusable(true);
                                        edtFacebook.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
                                        edtFacebook.setClickable(true); // use


                                        edtSnapchat.setFocusable(false);
                                        edtSnapchat.setFocusableInTouchMode(false); // user touches widget on phone with touch screen
                                        edtSnapchat.setClickable(false); // use


                                        edtSnapchatid.setFocusable(true);
                                        edtSnapchatid.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
                                        edtSnapchatid.setClickable(true); // use

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday,link");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {

            }
        });




        Intent intent = getIntent();
        if (intent != null) {

            com.scalable.tbp.Pojo.registrationPojo.Datum datum = (com.scalable.tbp.Pojo.registrationPojo.Datum) intent.getSerializableExtra("data");
            showdata(datum);

        }

        updateInfluencerProfilePresenterImp.getCity(countryCode);

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onValidationSucceeded() {


        if (connectivity.isConnected()){



//        if (name.equals("")) {
//
//            showError("Please add at least one Social Media. ");
//            return;
//
//        }

        String fname = edtFirstname.getText().toString().trim();
        String lname = edtLastname.getText().toString().trim();
        String county = edtCountry.getText().toString().trim();
        String city = edtCity.getText().toString().trim();
        String about = edtAbout.getText().toString().trim();
        String instagram = edtInsta.getText().toString().trim();
        String facebbok = edtFacebook.getText().toString().trim();
        String snapchat = edtSnapchat.getText().toString().trim();
        String snapchatId = edtSnapchatid.getText().toString().trim();
        int checkedRadioButtonId = rdtGender.getCheckedRadioButtonId();

        if (!facebbok.equals("") ||!instagram.equals("") ||!snapchat.equals("")) {
            updateInfluencerProfilePresenterImp.doUpdate(fname, lname, checkedRadioButtonId, instagram, facebbok, snapchat, county, city, about, profieImage.replaceAll(" ", "%20"), countryCode, cityCode, snapchatId,facebookProfileUrl,instagramprofileUrl);
            return;
        }
            showError("Please add followers. ");


        }else {

            showError(getString(R.string.intenet_not_conected));
        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void showProgressbar() {


        if (progressDialog != null)
            progressDialog.show();

    }

    @Override
    public void hideProgressbar() {

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

    }

    @Override
    public void showError(String s) {

        Msg.t(this, s);

    }

    @Override
    public void init() {

        progressDialog = HelperMethods.getPrgressDiloge(this, "please wait ...");

        title.setText("Update Account");
        toolbar.setBackgroundColor(getResources().getColor(R.color.second));

        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);


        String first = "Verify <font color=#ee2b7a> Instagram </font> account ";
        changInstagramUpdate.setText(Html.fromHtml(first));

        String second = "Varify <font color=#ee2b7a> Facebook </font>  Account";
        changFacebUpdate.setText(Html.fromHtml(second));

        String third = "Varify <font color=#ee2b7a> Snapchat </font> Account";
        changSnapUpdate.setText(Html.fromHtml(third));



    }

    @Override
    public void bindevent() {

        btnFacebook.setOnClickListener(this);
        btnInsta.setOnClickListener(this);

        btnupdate.setOnClickListener(this);
        edtCity.setOnClickListener(this);
        edtCountry.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgToplogo.setOnClickListener(this);

    }

    @Override
    public void clearEditText() {

        edtFirstname.setText("");
        edtLastname.setText("");
        edtInsta.setText("");
        edtCountry.setText("");
        edtCity.setText("");
        edtFacebook.setText("");
        edtSnapchat.setText("");
        edtAbout.setText("");
        edtSnapchatid.setText("");

    }

    @Override
    public void moveToDashboardView() {


        Intent intent = new Intent(UpdateInfluencerProfileActvity.this, MainActivity.class);
        intent.putExtra("data", Constants.INFLUENCER);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
        finish();

    }

    @Override
    public void setCountry(final Country country, List list) {


        _searchableCountryListDialog = SearchableListDialog.newInstance(list);
        _searchableCountryListDialog.setTitle("Select Country");
        _searchableCountryListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {

                edtCountry.setText(item.toString());
                edtCity.setText("");

                for (Datum datum : country.getData()) {
                    if (datum.getCountryName().equals(item.toString())) {

                        updateInfluencerProfilePresenterImp.getCity(datum.getCountryId());
                        countryCode = datum.getCountryId();

                    }
                }

            }
        });

    }

    @Override
    public void showCountry() {

        if (_searchableCountryListDialog != null) {

            _searchableCountryListDialog.show(getSupportFragmentManager(), "");

        }
    }

    @Override
    public void setCity(final City city, List list) {

        _searchableCityListDialog = SearchableListDialog.newInstance(list);
        _searchableCityListDialog.setTitle("Select City");
        _searchableCityListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {


                for (com.scalable.tbp.Pojo.city.Datum datum : city.getData()) {
                    if (datum.getCityName().equals(item.toString())) {

                        edtCity.setText(item.toString());

                        cityCode = datum.getCityId();


                    }
                }

            }
        });
    }

    @Override
    public void showCity() {

        if (_searchableCityListDialog != null) {

            _searchableCityListDialog.show(getSupportFragmentManager(), "");
        }

    }

    @Override
    public void showdata(com.scalable.tbp.Pojo.registrationPojo.Datum datum) {

        edtFirstname.setText(datum.getFirstname());
        edtLastname.setText(datum.getLastName());
        edtCountry.setText(datum.getCountry());
        edtCity.setText(datum.getCity());

        facebookProfileUrl=datum.getFacebookprofileurl();
        instagramprofileUrl=datum.getInstagramprofileurl();

        edtAbout.setText(datum.getAbout());

        if (datum.getGender().equals("male")) {

            ((RadioButton) findViewById(R.id.rbtn_male)).setChecked(true);


        } else if (datum.getGender().equals("female")) {

            ((RadioButton) findViewById(R.id.rbtn_female)).setChecked(true);

        } else if (datum.getGender().equals("others")) {

            ((RadioButton) findViewById(R.id.rbtn_other)).setChecked(true);
        }


        if (datum.getInstagramFollowers() != null && !datum.getInstagramFollowers().equals("")) {


            textInstaVar.setVisibility(View.VISIBLE);
            edtInsta.setText(datum.getInstagramFollowers());

            edtInsta.setFocusable(true);
            edtInsta.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtInsta.setClickable(true); // use
            email = datum.getInstagramFollowers();

            textSnapVar.setVisibility(View.INVISIBLE);
            edtSnapchat.setFocusable(true);
            edtSnapchat.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtSnapchat.setClickable(true); // use

            edtSnapchatid.setFocusable(true);
            edtSnapchatid.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtSnapchatid.setClickable(true); // use

        }

        if (datum.getFacebookFollowers() != null && !datum.getFacebookFollowers().equals("")) {

            textFacVar.setVisibility(View.VISIBLE);
            edtFacebook.setText(datum.getFacebookFollowers());

            edtFacebook.setFocusable(true);
            edtFacebook.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtFacebook.setClickable(true); // use

            email = datum.getFacebookFollowers();

            textSnapVar.setVisibility(View.INVISIBLE);
            edtSnapchat.setFocusable(true);
            edtSnapchat.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtSnapchat.setClickable(true); // use

            edtSnapchatid.setFocusable(true);
            edtSnapchatid.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtSnapchatid.setClickable(true); // use

        }

        if (datum.getSnapchatFollowers() != null && !datum.getSnapchatFollowers().equals("")) {


            textSnapVar.setVisibility(View.INVISIBLE);
            edtSnapchatid.setText(datum.getSnapchatid());
            edtSnapchat.setText(datum.getSnapchatFollowers());

            email = datum.getSnapchatFollowers();


            edtSnapchat.setFocusable(true);
            edtSnapchat.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtSnapchat.setClickable(true); // use

            edtSnapchatid.setFocusable(true);
            edtSnapchatid.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtSnapchatid.setClickable(true); // use

        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_update:

                validator.validate();

                break;

            case R.id.edt_city:

                showCity();

                break;
            case R.id.edt_country:

                showCountry();

                break;

            case R.id.btn_facebook:

                loginButton.performClick();

                break;

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.img_toplogo:

                onBackPressed();

                break;

            case R.id.btn_insta:

                instagramHelper.loginFromActivity(UpdateInfluencerProfileActvity.this);
                break;
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == InstagramHelperConstants.INSTA_LOGIN && resultCode == RESULT_OK) {
            InstagramUser user = instagramHelper.getInstagramUser(this);


            profieImage = user.getData().getProfilePicture();
            instagramprofileUrl=Constants.INSTAGRAM_PROFILE_URL+user.getData().getUsername();

            email = "data";
            textInstaVar.setVisibility(View.VISIBLE);

            edtInsta.setFocusable(true);
            edtInsta.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtInsta.setClickable(true); // use


            edtSnapchat.setFocusable(true);
            edtSnapchat.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtSnapchat.setClickable(true); // use


            edtSnapchatid.setFocusable(true);
            edtSnapchatid.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
            edtSnapchatid.setClickable(true); // use


            //  Toast.makeText(this, user.getData().getUsername(), Toast.LENGTH_LONG).show();

        } else {

            //Toast.makeText(this, "Login failed", Toast.LENGTH_LONG).show();
        }
    }
}
