package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.ForgotPasswordrepository.ForgotPasswordView;
import com.scalable.tbp.dagger.DaggerForgotPasswordScreenComponent;
import com.scalable.tbp.dagger.ForgotPasswordSreenModule;
import com.scalable.tbp.ui.ForgotPasswordPresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.HelperMethods;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordActvity extends AppCompatActivity implements ForgotPasswordView, Validator.ValidationListener, View.OnClickListener {


    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.btn_company)
    Button btnCompany;

    @BindView(R.id.btn_influencer)
    Button btnInfluencer;

    @Email
    @BindView(R.id.edt_email)
    EditText edtEmail;

    @BindView(R.id.btn_go)
    Button btnGo;

    @BindView(R.id.btn_sign)
    TextView btnSign;

    @BindView(R.id.btn_company1)
    Button btnCompany1;

    @BindView(R.id.btn_influencer1)
    Button btnInfluencer1;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    private ProgressDialog progressDialog;


    @Inject
    ForgotPasswordPresenterImp forgotPasswordPresenterImp;
    private Validator validator;

    @Inject
    Connectivity connectivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_actvity);
        ButterKnife.bind(this);

        //inject Dagger
        DaggerForgotPasswordScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .forgotPasswordSreenModule(new ForgotPasswordSreenModule(this))
                .build().inject(this);


        title.setText("Forgot Password");


        init();
        bindevent();



    }

    @Override
    public void showProgressbar() {


        if (progressDialog != null)
            progressDialog.show();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void hideProgressbar() {

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

    }

    @Override
    public void showError(String s) {

        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void init() {

        validator = new Validator(this);
        validator.setValidationListener(this);
        progressDialog = HelperMethods.getPrgressDiloge(this, "Please wait ...");

        imgToplogo.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.VISIBLE);

    }

    @Override
    public void bindevent() {


        btnGo.setOnClickListener(this);
        btnSign.setOnClickListener(this);
        btnCompany.setOnClickListener(this);
        btnInfluencer.setOnClickListener(this);
        btnCompany1.setOnClickListener(this);
        btnInfluencer1.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgToplogo.setOnClickListener(this);


    }

    @Override
    public void clearEditText() {

        edtEmail.setText("");
    }

    @Override
    public void moveToSignInView() {


        Intent intent = new Intent(ForgotPasswordActvity.this, SelectUserActvity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void moveToresetpasswordView(String email) {

        Intent intent = new Intent(ForgotPasswordActvity.this, ResetPasswordActvity.class);
        intent.putExtra("email",email);
        startActivity(intent);


    }


    @Override
    public void onValidationSucceeded() {

        if (connectivity.isConnected()) {

            String email = edtEmail.getText().toString().trim();
            forgotPasswordPresenterImp.doforgotPassword(email);

        } else {

            showError(getString(R.string.intenet_not_conected));
        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }



    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_company:

                forgotPasswordPresenterImp.setType(Constants.COMPANY);

                break;
            case R.id.btn_sign:

                moveToSignInView();

                break;

            case R.id.btn_company1:

                forgotPasswordPresenterImp.setType(Constants.COMPANY);
                moveToSignInView();

                break;

            case R.id.btn_influencer:

                forgotPasswordPresenterImp.setType(Constants.INFLUENCER);

                break;


            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.img_toplogo:

                onBackPressed();

                break;

            case R.id.btn_influencer1:

                forgotPasswordPresenterImp.setType(Constants.INFLUENCER);
                moveToSignInView();

                break;

            case R.id.btn_go:

                validator.validate();

                break;

        }

    }

}
