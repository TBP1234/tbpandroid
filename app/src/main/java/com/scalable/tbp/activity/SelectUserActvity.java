package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.scalable.tbp.MyApplication;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.SelectUserRepository;
import com.scalable.tbp.dagger.DaggerSelectUserScreenComponent;
import com.scalable.tbp.dagger.SelectUserSreenModule;
import com.scalable.tbp.ui.SelectUserPresenterImp;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.HelperMethods;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SelectUserActvity extends AppCompatActivity implements SelectUserRepository.SelectUserView, View.OnClickListener {

    @Inject
    SelectUserPresenterImp selectUserPresenterImp;

    @BindView(R.id.btn_company)
    Button btnCompany;

    @BindView(R.id.btn_influencer)
    Button btnInfluencer;
    @BindView(R.id.title)
    TextView title;

    private ProgressDialog progressDialog;
    private int backButtonCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_user_actvity);
        ButterKnife.bind(this);


        //inject Dagger
        DaggerSelectUserScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .selectUserSreenModule(new SelectUserSreenModule(this))
                .build().inject(this);

        btnCompany.setOnClickListener(this);
        btnInfluencer.setOnClickListener(this);

        progressDialog = HelperMethods.getPrgressDiloge(this, "Please wait...");

        title.setText("Select User Type");

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_company:

                selectUserPresenterImp.doSignInclick(Constants.COMPANY);
                break;

            case R.id.btn_influencer:

                selectUserPresenterImp.doSignInclick(Constants.INFLUENCER);
                break;
        }
    }

    @Override
    public void showProgressbar() {

        if (progressDialog != null) {

            progressDialog.show();

        }

    }


    @Override
    public void hideProgressbar() {

        if (progressDialog != null && progressDialog.isShowing()) {

            progressDialog.dismiss();
        }

    }

    @Override
    public void showError(String s) {

        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void init() {

    }

    @Override
    public void bindevent() {

    }

    @Override
    public void moveToSignInView() {
        Intent intent = new Intent(SelectUserActvity.this, SignInActvity.class);
        startActivity(intent);
        finish();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if(backButtonCount >= 1)
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(SelectUserActvity.this, AboutActivity.class);
            startActivity(intent);
        }
    }
}
