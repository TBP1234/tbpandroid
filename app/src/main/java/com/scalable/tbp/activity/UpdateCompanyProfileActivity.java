package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.InterstitialAd;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.county.Datum;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.UpdateCompanyProfilerepository;
import com.scalable.tbp.dagger.DaggerUpdateCompanyProfileScreenComponent;
import com.scalable.tbp.dagger.UpdateCompanyProfileSreenModule;
import com.scalable.tbp.ui.UpdateCompanyProfilePresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.SearchableListDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class UpdateCompanyProfileActivity extends AppCompatActivity implements Validator.ValidationListener, UpdateCompanyProfilerepository.UpdateCompanyProfileView, View.OnClickListener {


    @NotEmpty
    @BindView(R.id.edt_fname)
    EditText edtFname;

    @NotEmpty
    @BindView(R.id.edt_lname)
    EditText edtLname;

    @NotEmpty
    @BindView(R.id.edt_cname)
    EditText edtCname;

    @NotEmpty
    @BindView(R.id.edt_typeProduct)
    EditText edtTypeProduct;

    @NotEmpty
    @BindView(R.id.edt_cnt)
    EditText edtCnt;

    @NotEmpty
    @BindView(R.id.edt_cty)
    EditText edtCty;

    @BindView(R.id.btn_update)
    Button btnSave;

    @BindView(R.id.title)
    TextView title;

    @NotEmpty
    @BindView(R.id.edt_abt)
    EditText edtAbt;

    @NotEmpty
    @BindView(R.id.edt_branch)
    EditText edtBranch;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    private Validator validator;

    @Inject
    UpdateCompanyProfilePresenterImp updateCompanyProfilePresenterImp;

    ProgressDialog progressDialog;

    @Inject
    Connectivity connectivity;

    private SearchableListDialog _searchableCountryListDialog;

    private SearchableListDialog _searchableCityListDialog;

    private String countryCode;
    private String cityCode;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_company_profile);

        ButterKnife.bind(this);
        //inject Dagger
        DaggerUpdateCompanyProfileScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .updateCompanyProfileSreenModule(new UpdateCompanyProfileSreenModule(this))
                .build().inject(this);


        init();
        bindevent();

        Intent intent = getIntent();
        if (intent != null) {
            com.scalable.tbp.Pojo.registrationPojo.Datum datum = (com.scalable.tbp.Pojo.registrationPojo.Datum) intent.getSerializableExtra("data");
            showData(datum);
        }

        updateCompanyProfilePresenterImp.getCountry();
        updateCompanyProfilePresenterImp.getCity(countryCode);





    }


    @Override
    public void onValidationSucceeded() {

        if (connectivity.isConnected()) {

            String fname = edtFname.getText().toString().trim();
            String lastname = edtLname.getText().toString().trim();
            String companyname = edtCname.getText().toString().trim();
            String typeOfproduct = edtTypeProduct.getText().toString().trim();
            String country = edtCnt.getText().toString().trim();
            String city = edtCty.getText().toString().trim();
            String about = edtAbt.getText().toString().trim();
            String brach = edtBranch.getText().toString().trim();

            updateCompanyProfilePresenterImp.doUpdate(fname, lastname, companyname, "", typeOfproduct, country, city, about, brach, countryCode, cityCode);


        } else {

            showError(getString(R.string.intenet_not_conected));

        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void showProgressbar() {


        if (progressDialog != null)
            progressDialog.show();

    }

    @Override
    public void hideProgressbar() {

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

    }

    @Override
    public void showError(String s) {

        Msg.t(this, s);

    }

    @Override
    public void init() {

        validator = new Validator(this);
        validator.setValidationListener(this);

        progressDialog = HelperMethods.getPrgressDiloge(this, "Please wait ...");

        title.setText("Update Profile");

        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);
        toolbar.setBackgroundColor(getResources().getColor(R.color.first));


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void bindevent() {

        btnSave.setOnClickListener(this);
        edtCty.setOnClickListener(this);
        edtCnt.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgToplogo.setOnClickListener(this);
    }

    @Override
    public void clearEditText() {


        edtFname.setText("");
        edtLname.setText("");
        edtCname.setText("");
        edtTypeProduct.setText("");
        edtCty.setText("");
        edtCnt.setText("");
        edtAbt.setText("");
        edtBranch.setText("");

    }

    @Override
    public void moveToDashboardView() {


        Intent intent = new Intent(UpdateCompanyProfileActivity.this, MainActivity.class);
        intent.putExtra("data", Constants.COMPANY);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    @Override
    public void showCountry() {

        if (_searchableCountryListDialog != null) {

            _searchableCountryListDialog.show(getSupportFragmentManager(), "");

        }

    }

    @Override
    public void setCity(final City city, List list) {

        _searchableCityListDialog = SearchableListDialog.newInstance(list);
        _searchableCityListDialog.setTitle("Select City");
        _searchableCityListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {


                for (com.scalable.tbp.Pojo.city.Datum datum : city.getData()) {
                    if (datum.getCityName().equals(item.toString())) {

                        edtCty.setText(datum.getCityName());
                        cityCode = datum.getCityId();


                    }
                }

            }
        });
    }

    @Override
    public void showData(com.scalable.tbp.Pojo.registrationPojo.Datum datum) {


        edtFname.setText(datum.getFirstname());
        edtLname.setText(datum.getLastName());
        edtCname.setText(datum.getCompanyName());
        edtTypeProduct.setText(datum.getTypeOfProduct());
        edtCty.setText(datum.getCity());
        edtCnt.setText(datum.getCountry());
        cityCode = datum.getCityCode();
        edtAbt.setText(datum.getAbout());
        edtBranch.setText(datum.getBranch());
        countryCode = datum.getCountryCode();

    }

    @Override
    public void showCity() {

        if (_searchableCityListDialog != null) {

            _searchableCityListDialog.show(getSupportFragmentManager(), "");
        }

    }

    @Override
    public void setCountry(final Country country, List list) {


        _searchableCountryListDialog = SearchableListDialog.newInstance(list);
        _searchableCountryListDialog.setTitle("Select Country");
        _searchableCountryListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {

                edtCnt.setText(item.toString());
                edtCty.setText("");

                for (Datum datum : country.getData()) {
                    if (datum.getCountryName().equals(item.toString())) {

                        updateCompanyProfilePresenterImp.getCity(datum.getCountryId());
                        countryCode = datum.getCountryId();

                    }
                }

            }
        });
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_update:

                validator.validate();

                break;
            case R.id.edt_cty:

                showCity();

                break;

            case R.id.edt_cnt:

                showCountry();

                break;

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.img_toplogo:
                onBackPressed();

                break;
        }
    }
}
