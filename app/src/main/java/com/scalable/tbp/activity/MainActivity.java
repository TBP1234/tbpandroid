package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.companyList.CompanyList;
import com.scalable.tbp.Pojo.influecncerList.InfluecncerList;
import com.scalable.tbp.Pojo.notificationmsg.NotificationMsg;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.Mainrepository;
import com.scalable.tbp.adapter.MainScreenAdapter;
import com.scalable.tbp.dagger.DaggerMainScreenComponent;
import com.scalable.tbp.dagger.MainSreenModule;
import com.scalable.tbp.ui.MainPresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements Mainrepository.MainView, View.OnClickListener {

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.img_profle)
    ImageView imgProfle;

    boolean doubleBackToExitPressedOnce = false;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_filter)
    Button btnFilter;

    @BindView(R.id.rec_list)
    RecyclerView recList;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    @BindView(R.id.img_chat)
    ImageView imgChat;

    @BindView(R.id.btn_reset)
    Button btnReset;

    @BindView(R.id.count)
    TextView count;

    @BindView(R.id.counterValuePanel)
    RelativeLayout counterValuePanel;
    @BindView(R.id.counterBackground)
    ImageView counterBackground;

    private ProgressDialog progressDialog;

    @Inject
    MainPresenterImp mainPresenter;

    @Inject
    Connectivity connectivity;
    private int COMPANY_ACTIVITY_RESULT_CODE = 101;
    private int INFLUENCER_ACTIVITY_RESULT_CODE = 201;
    private BroadcastReceiver mReceiver;
    private String TAG1 = MainActivity.class.getSimpleName();
    InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        // MobileAds.initialize(this,
               // "ca-app-pub-5871644239810117/7518338152");
        mInterstitialAd = new InterstitialAd(this);
        //  set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.banner_ad_unit_id));
        //inject Dagger
        DaggerMainScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .mainSreenModule(new MainSreenModule(this))
                .build().inject(this);
        init();
        bindevent();
        AdRequest adRequest = new AdRequest.Builder()
               // .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
              //  .addTestDevice("1082AEA199A2D33C6C69795FB7C61B28")
                .build();

        //  Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }

            private void showInterstitial() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });



        if (connectivity.isConnected()) {

            mainPresenter.getObjectList();

        } else {

            showError(getString(R.string.intenet_not_conected));
        }



    }


    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.dosetColor();

        updateCount();

        IntentFilter intentFilter = new IntentFilter(
                Constants.MSG_BROADCAST_Main);

        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                NotificationMsg notificationMsg = (NotificationMsg) intent.getSerializableExtra("data");
                //log our message value
                Msg.l("Broadcast Msg", notificationMsg.getPayload().getMessage());

                updateCount();


            }

        };
        //registering our receiver
        this.registerReceiver(mReceiver, intentFilter);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(this.mReceiver);
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void showProgressbar() {


        if (progressDialog != null)
            progressDialog.show();

    }

    @Override
    public void hideProgressbar() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

    }

    @Override
    public void showError(String s) {

        Msg.t(this, s);
    }

    @Override
    public void init() {

        imgProfle.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);
        imgChat.setVisibility(View.VISIBLE);

        progressDialog = HelperMethods.getPrgressDiloge(this, "Please wait ...");


    }

    @Override
    public void bindevent() {

        imgProfle.setOnClickListener(this);
        imgChat.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
        btnReset.setOnClickListener(this);

    }

    @Override
    public void clearEditText() {

        //Todo

    }

    @Override
    public void moveToCompanyProfileView() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.first));
        Intent intent = new Intent(MainActivity.this, CompanyProfileActvity.class);
        startActivity(intent);

    }

    @Override
    public void moveToInfluencerProfileView() {
        toolbar.setBackgroundColor(getResources().getColor(R.color.second));
        Intent intent = new Intent(MainActivity.this, InfluencerProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void moveToCompanyFilterView() {

        Intent intent = new Intent(MainActivity.this, CompanyFiltterActivity.class);
        startActivityForResult(intent, COMPANY_ACTIVITY_RESULT_CODE);

    }

    @Override
    public void moveToInfluencerFilterView() {

        Intent intent = new Intent(MainActivity.this, InfluencerFillterActivity.class);
        startActivityForResult(intent, INFLUENCER_ACTIVITY_RESULT_CODE);
    }

    @Override
    public void moveToChatView() {

        Intent intent = new Intent(MainActivity.this, ChatListActivity.class);
        startActivity(intent);

    }

    @Override
    public void showLIst(List<Object> list, int type) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(layoutManager);

        MainScreenAdapter mainScreenAdapter = new MainScreenAdapter(MainActivity.this, list, type);
        recList.setAdapter(mainScreenAdapter);
    }

    @Override
    public void showInfluencerColor() {

        title.setText("Find Company");
        toolbar.setBackgroundColor(getResources().getColor(R.color.second));
        btnFilter.setBackgroundColor(getResources().getColor(R.color.second));
        btnReset.setBackgroundColor(getResources().getColor(R.color.second));

        GradientDrawable bgShape = (GradientDrawable)counterBackground.getBackground();
        bgShape.setColor(getResources().getColor(R.color.first));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the SecondActivity with an OK result
        if (requestCode == COMPANY_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {

                if (data.hasExtra("data")) {
                    CompanyList companyList = (CompanyList) data.getSerializableExtra("data");
                    List<Object> list = new ArrayList<>();
                    list.addAll(companyList.getData());
                    showLIst(list, 1);

                } else {

                    mainPresenter.getObjectList();
                }

            }
        }

        // check that it is the SecondActivity with an OK result
        if (requestCode == INFLUENCER_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {

                if (data.hasExtra("data")) {

                    InfluecncerList influecncerList = (InfluecncerList) data.getSerializableExtra("data");
                    List<Object> list = new ArrayList<>();
                    list.addAll(influecncerList.getData());
                    showLIst(list, 2);

                } else {

                    mainPresenter.getObjectList();
                }

            }
        }

    }

    @Override
    public void showComapanyColor() {

        title.setText("Find Influencer");
        toolbar.setBackgroundColor(getResources().getColor(R.color.first));
        btnFilter.setBackgroundColor(getResources().getColor(R.color.first));
        btnReset.setBackgroundColor(getResources().getColor(R.color.first));

        GradientDrawable bgShape = (GradientDrawable)counterBackground.getBackground();
        bgShape.setColor(getResources().getColor(R.color.second));
    }

    @Override
    public void updateCount() {

        if (Constants.COUNT > 0) {

            counterValuePanel.setVisibility(View.VISIBLE);
            count.setText(""+Constants.COUNT);

        } else {

            counterValuePanel.setVisibility(View.GONE);

        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_profle:

                mainPresenter.doProfile();

                break;

            case R.id.img_chat:

                mainPresenter.dochatButtonClick();
                AdRequest adRequest = new AdRequest.Builder()
                        // .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        // Check the LogCat to get your test device ID
                        //  .addTestDevice("1082AEA199A2D33C6C69795FB7C61B28")
                        .build();

                //  Load ads into Interstitial Ads
                mInterstitialAd.loadAd(adRequest);

                mInterstitialAd.setAdListener(new AdListener() {
                    public void onAdLoaded() {
                        showInterstitial();
                    }

                    private void showInterstitial() {
                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                        }
                    }
                });

                break;

            case R.id.btn_filter:

                mainPresenter.doFilterButtonClick();

                break;

            case R.id.btn_reset:

                if (connectivity.isConnected()) {

                    mainPresenter.getObjectList();

                } else {

                    showError(getString(R.string.intenet_not_conected));
                }

                break;
        }
    }


}
