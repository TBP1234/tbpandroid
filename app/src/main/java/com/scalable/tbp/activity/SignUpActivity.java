package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.SignUprepository;
import com.scalable.tbp.dagger.DaggerSignUpScreenComponent;
import com.scalable.tbp.dagger.SignUpSreenModule;
import com.scalable.tbp.fragment.ConfirmDilogeFragmnet;
import com.scalable.tbp.ui.SignUpPresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUpActivity extends AppCompatActivity implements SignUprepository.SignUpView, View.OnClickListener, Validator.ValidationListener {

    @NotEmpty
    @BindView(R.id.edt_uname)
    EditText edtUname;


    @NotEmpty
    @Password(min = 1)
    @BindView(R.id.edt_password)
    EditText edtPassword;

    @NotEmpty
    @ConfirmPassword
    @BindView(R.id.edt_cnfpass)
    EditText edtCnfpass;

    @NotEmpty
    @Email
    @BindView(R.id.edt_email)
    EditText edtEmail;

    @BindView(R.id.chk_act)
    CheckBox chkAct;

    @BindView(R.id.btn_submit)
    Button btnSubmit;

    ProgressDialog dialog;

    @Inject
    SignUpPresenterImp signUpPresenterImp;

    @Inject
    Connectivity connectivity;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.text_info)
    TextView textInfo;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;


    private Validator validator;

    @BindView(R.id.textView15)
    TextView txt_terms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signupactivity);
        ButterKnife.bind(this);

        //inject Dagger
        DaggerSignUpScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .signUpSreenModule(new SignUpSreenModule(this))
                .build().inject(this);

        init();

        bindevent();



        signUpPresenterImp.setInfoMsg();
        signUpPresenterImp.setColor();

    }

    @Override
    public void showProgressbar() {
        if (dialog != null) {
            dialog.show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));

    }

    @Override
    public void hideProgressbar() {

        if (dialog != null && dialog.isShowing()) {

            dialog.dismiss();
        }
    }

    @Override
    public void showError(String s) {

        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void init() {

        dialog = HelperMethods.getPrgressDiloge(this, "Please wait ...");
        validator = new Validator(this);

        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);

        title.setText("Create Account");

    }

    @Override
    public void bindevent() {

        validator.setValidationListener(this);
        btnSubmit.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        txt_terms.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {



        switch (v.getId()) {
            case R.id.img_back:
                // Statements
                onBackPressed();
                break; // optional

            case R.id.btn_submit:
                // Statements
                validator.validate();
                break; // optional

            case R.id.textView15:
                // Statements
                openWebView();
                break;
            // You can have any number of case statements.
            default: // Optional
                // Statements
        }

    }

    @Override
    public void onValidationSucceeded() {

        String name = edtUname.getText().toString().trim();
        String pass = edtPassword.getText().toString().trim();
        String email = edtEmail.getText().toString().trim();

        if (!chkAct.isChecked()) {

            Msg.t(this, "Please accept Terms and Conditions.");

            return;
        }


        if (connectivity.isConnected()) {

            signUpPresenterImp.doSignUp("", name, pass, email);

        } else {

            Msg.t(SignUpActivity.this, "Internet not available on this device...");
        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void clearEditText() {

        edtUname.setText("");
        edtPassword.setText("");
        edtCnfpass.setText("");
        edtEmail.setText("");
        chkAct.setChecked(false);

    }

    @Override
    public void moveToConfimView(int userId) {

        FragmentManager fm = getSupportFragmentManager();
        ConfirmDilogeFragmnet myDialogFragment = new ConfirmDilogeFragmnet(userId);
        myDialogFragment.setCancelable(false);
        myDialogFragment.show(fm, "dialog_fragment");

    }

    @Override
    public void InfoMsg(String infMsg) {

        textInfo.setText(infMsg);
    }

    @Override
    public void setColor(int code) {

        int color = 0;
        if (code == 0) {

            color = getResources().getColor(R.color.first);
            toolbar.setBackgroundColor(color);
            btnSubmit.setBackgroundColor(color);
            txt_terms.setTextColor(color);
            chkAct.setBackground(getResources().getDrawable(R.drawable.checkbox_b1));


        } else {
            color = getResources().getColor(R.color.second);
            toolbar.setBackgroundColor(color);
            btnSubmit.setBackgroundColor(color);
            txt_terms.setTextColor(color);
            chkAct.setBackground(getResources().getDrawable(R.drawable.checkbox_a1));
        }
    }

    @Override
    public void openWebView() {
        Intent intent= new Intent(SignUpActivity.this,WebViewActivity.class);
        intent.putExtra("data","");
        startActivity(intent);
    }
}
