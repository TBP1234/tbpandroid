package com.scalable.tbp.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.notificationmsg.NotificationMsg;
import com.scalable.tbp.Pojo.registrationPojo.Datum;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.ChatRepository;
import com.scalable.tbp.adapter.ChatAdapter;
import com.scalable.tbp.dagger.ChatSreenModule;
import com.scalable.tbp.dagger.DaggerChatScreenComponent;
import com.scalable.tbp.fragment.ConfirmDilogeFragmnet;
import com.scalable.tbp.fragment.LeftMessageDialogFragment;
import com.scalable.tbp.ui.ChatPresenterImp;
import com.scalable.tbp.ui.CompanyProfilePresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.SharedPreferenceHelper;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatActivity extends AppCompatActivity implements ChatRepository.ChatView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.img_profle)
    ImageView imgProfle;

    @BindView(R.id.rec_chat)
    RecyclerView recChat;

    @Inject
    ChatPresenterImp chatPresenterImp;

    @BindView(R.id.edt_send)
    EditText edtSend;

    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.send_req_btn)
    Button send_req_btn;

   @BindView(R.id.chat_constraint)
    ConstraintLayout chat_constraint;
    @BindView(R.id.send_request_constraint)
    ConstraintLayout send_request_constraint;

    @BindView(R.id.request_accept_constraint)
    ConstraintLayout request_accept_constraint;

    @BindView(R.id.request_pending_constraint)
    ConstraintLayout request_pending_constraint;
    @BindView(R.id.send_req_img)
    ImageView send_req_img;
    @BindView(R.id.req_pending_img)
    ImageView req_pending_img;
    @BindView(R.id.req_accept_img)
    ImageView req_accept_img;


    @BindView(R.id.img_chat)
    ImageView imgChat;
    @BindView(R.id.accept_btn)
    Button accept_btn;
    @BindView(R.id.ignore_btn)
    Button ignore_btn;

    private ProgressDialog progressDialog;
    DataManager dataManager;
    SharedPreferenceHelper sharedPreferenceHelper;

    @Inject
    Connectivity connectivity;

    

    private String id;
    private String toid;
    private BroadcastReceiver mReceiver;
    private String request;
    String blockId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_actvity);
        ButterKnife.bind(this);
        DaggerChatScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .chatSreenModule(new ChatSreenModule(this))
                .build().inject(this);

        init();

        bindevent();
        Intent intent = getIntent();
        if (intent != null) {

            String name = intent.getStringExtra("data");
            title.setText(name);

            id = intent.getStringExtra("id");

        }

        chatPresenterImp.setColor();
        chatPresenterImp.setChatUser(id);
        chatPresenterImp.fetchChatData(id);

    }

    @Override
    public void showProgressbar() {

        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    @Override
    public void hideProgressbar() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showError(String s) {

        Msg.t(this, s);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void init() {

        progressDialog = HelperMethods.getPrgressDiloge(this, getString(R.string.pleasewait));


        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);
        //imgProfle.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setReverseLayout(true);
        recChat.setLayoutManager(layoutManager);


    }

    @Override
    public void bindevent() {

        sharedPreferenceHelper= new SharedPreferenceHelper(MyApplication.get(ChatActivity.this));
        dataManager = new DataManager(sharedPreferenceHelper);
    }

    @Override
    public void moveToDashboardView(String s, Datum datum) {

    }
    @Override
    public void showChat(ArrayList<Object> arrayList) {
        ChatAdapter chatAdapter = new ChatAdapter(this, arrayList);
        recChat.setAdapter(chatAdapter);

    }
    @Override
    public void moveToCompanyProfileView() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.first));
        Intent intent = new Intent(ChatActivity.this, CompanyProfileActvity.class);
        startActivity(intent);

    }

    @Override
    public void clearEdittext() {
        edtSend.setText("");
    }

    @Override
    public void moveToInfluencerProfileView() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.second));
        Intent intent = new Intent(ChatActivity.this, InfluencerProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void moveToChatListView() {

        Intent intent = new Intent(ChatActivity.this, ChatListActivity.class);
        startActivity(intent);

    }

    @Override
    public void showInfluencerColor() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.second));
        btnSend.setBackgroundColor(getResources().getColor(R.color.second));
        send_req_btn.setBackgroundColor(getResources().getColor(R.color.second));
        accept_btn.setBackgroundColor(getResources().getColor(R.color.second));
        send_req_img.setImageResource(R.drawable.send_request_b);
        req_pending_img.setImageResource(R.drawable.invite_pending_b);
        req_accept_img.setImageResource(R.drawable.request_accept_b);
    }

    @Override
    protected void onDestroy() {
        chatPresenterImp.setChatUser("0");
        super.onDestroy();

    }

    @Override
    public void showComapanyColor() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.first));
        btnSend.setBackgroundColor(getResources().getColor(R.color.first));
        send_req_btn.setBackgroundColor(getResources().getColor(R.color.first));
        accept_btn.setBackgroundColor(getResources().getColor(R.color.first));
        send_req_img.setImageResource(R.drawable.send_request_a);
        req_pending_img.setImageResource(R.drawable.invite_pending_a);
        req_accept_img.setImageResource(R.drawable.request_accept_a);
    }

    @Override
    public void layoutVisiblechat() {
        String typee = dataManager.getType();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (typee.equalsIgnoreCase(Constants.INFLUENCER)){
            showInfluencerColor();
            chat_constraint.setVisibility(View.VISIBLE);
            send_request_constraint.setVisibility(View.GONE);
            request_accept_constraint.setVisibility(View.GONE);
            request_pending_constraint.setVisibility(View.GONE);
        }else if (typee.equalsIgnoreCase(Constants.COMPANY)){
            showComapanyColor();
            chat_constraint.setVisibility(View.VISIBLE);
            send_request_constraint.setVisibility(View.GONE);
            request_accept_constraint.setVisibility(View.GONE);
            request_pending_constraint.setVisibility(View.GONE);
        }
    }

    @Override
    public void layoutvisiblesendrequest() {
        String type = dataManager.getType();
        if (type.equals(Constants.INFLUENCER)){
            showInfluencerColor();
            chat_constraint.setVisibility(View.GONE);
            send_request_constraint.setVisibility(View.VISIBLE);
            request_accept_constraint.setVisibility(View.GONE);
            request_pending_constraint.setVisibility(View.GONE);
        }else if (type.equals(Constants.COMPANY)){
            showComapanyColor();
            chat_constraint.setVisibility(View.GONE);
            send_request_constraint.setVisibility(View.VISIBLE);
            request_accept_constraint.setVisibility(View.GONE);
            request_pending_constraint.setVisibility(View.GONE);

        }

    }

    @Override
    public void layoutvisiblependingrequest() {
        String type = dataManager.getType();
        if (type.equals(Constants.INFLUENCER)){
            showInfluencerColor();
            chat_constraint.setVisibility(View.GONE);
            send_request_constraint.setVisibility(View.GONE);
            request_accept_constraint.setVisibility(View.GONE);
            request_pending_constraint.setVisibility(View.VISIBLE);
        }else if (type.equals(Constants.COMPANY)){
            showComapanyColor();
            chat_constraint.setVisibility(View.GONE);
            send_request_constraint.setVisibility(View.GONE);
            request_accept_constraint.setVisibility(View.GONE);
            request_pending_constraint.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void layoutvisibleacceptrequest() {
        String type = dataManager.getType();
        if (type.equals(Constants.INFLUENCER)){
            showInfluencerColor();
            chat_constraint.setVisibility(View.GONE);
            send_request_constraint.setVisibility(View.GONE);
            request_accept_constraint.setVisibility(View.VISIBLE);
            request_pending_constraint.setVisibility(View.GONE);

        }else if (type.equals(Constants.COMPANY)){
            showComapanyColor();
            chat_constraint.setVisibility(View.GONE);
            send_request_constraint.setVisibility(View.GONE);
            request_accept_constraint.setVisibility(View.VISIBLE);
            request_pending_constraint.setVisibility(View.GONE);

        }

    }

    @Override
    public void getPendingRequestMessage() {

        Set<String> list = dataManager.getTimeList();

            int abc = 25 - list.size();
            String msg = "you have" +" "+String.valueOf(abc)+" " +"request left";

        FragmentManager fm = getSupportFragmentManager();
        LeftMessageDialogFragment myDialogFragment = new LeftMessageDialogFragment(msg,"0",this);
        myDialogFragment.setCancelable(false);
        myDialogFragment.show(fm, "dialog_fragment");

    }

    @Override
    public void moveToLoginView() {

        Intent intent = new Intent(ChatActivity.this, AboutActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void showCustomDialog(final String msg) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.customdialog);
        dialog.setTitle("Approve Via...");

        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.BOTTOM);

        lp.x = 100;
        lp.y = 100;
        lp.width = 700;
        lp.height = 250;
        dialogWindow.setAttributes(lp);

        TextView unblock = (TextView)dialog.findViewById(R.id.unblock);
        TextView cancel = (TextView)dialog.findViewById(R.id.cancel);

        unblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                chatPresenterImp.blockUser(id,"unblock",dialog,msg);


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clearEdittext();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void returnedBlockIdFromHistory(String id) {

        this.blockId=id;
    }


    @OnClick({R.id.img_back, R.id.img_toplogo, R.id.img_profle, R.id.btn_send,R.id.send_req_btn,R.id.accept_btn,R.id.ignore_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.img_toplogo:

                onBackPressed();

                break;

            case R.id.img_chat:

                chatPresenterImp.doChatButtunClicked();

                break;

            case R.id.btn_send:

                if (connectivity.isConnected()) {

                    if (!edtSend.getText().toString().trim().isEmpty()) {

                        String msg = edtSend.getText().toString().trim();
                        chatPresenterImp.doSendButtunClicked(id, msg);
                        edtSend.setText("");

                    }

                } else {

                    showError(getString(R.string.intenet_not_conected));

                }


                break;
            case R.id.img_profle:

                chatPresenterImp.doprofleButtunClicked();

                break;
           case R.id.send_req_btn:

               SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

               Calendar now = Calendar.getInstance();
               String formattedDate = df.format(now.getTime());
               dataManager.setTimeList(formattedDate);

               Set<String> list = dataManager.getTimeList();

                   for (Iterator<String> iterator = list.iterator(); iterator.hasNext(); ) {
                       String time = iterator.next();
                   try {
                       Date date = df.parse(time);
                       now.add(Calendar.MINUTE, -30);
                       String formattedDate1 = df.format(now.getTime());
                       Date date1 = df.parse(formattedDate1);
                       if(date.before(date1)){

                           iterator.remove();
                       }
                   } catch (ParseException e) {
                       e.printStackTrace();
                   }
               }
               if(list.size()>25){

//                   new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
//                           .setTitleText("Message")
//                           .setContentText("You have exceed limit")
//                           .show();
                   String msg = "You have exceed your limit,";

                   FragmentManager fm = getSupportFragmentManager();
                   LeftMessageDialogFragment myDialogFragment = new LeftMessageDialogFragment(msg,"1",this);
                   myDialogFragment.setCancelable(false);
                   myDialogFragment.show(fm, "dialog_fragment");

                   //chatPresenterImp.blockSelf();


               }else {

                   chatPresenterImp.sendChatRequest(id);



               }


              break;
            case R.id.accept_btn:
                chatPresenterImp.acceptChatRequest(id);
                break;
            case R.id.ignore_btn:
                chatPresenterImp.ignoreChatRequest(id);
                break;

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // TODO Auto-generated method stub
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(
                Constants.MSG_BROADCAST);

        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                NotificationMsg notificationMsg= (NotificationMsg) intent.getSerializableExtra("data");

                //log our message value
                Msg.l("Broadcast Msg", notificationMsg.getPayload().getMessage());

                chatPresenterImp.fetchChatData(notificationMsg.getPayload().getFromId());

            }

        };
        //registering our receiver
        this.registerReceiver(mReceiver, intentFilter);
    }

   @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        //unregister our receiver
        this.unregisterReceiver(this.mReceiver);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chat_menu, menu);

        return true;
    }

    public void dialogOkButtonListner(){

        chatPresenterImp.blockSelf();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.menu_block:


                if(blockId!=null && !blockId.equals("") && blockId.equals(id)){

                    showError("You are already blocked by this user");
                }else {
                    chatPresenterImp.blockUser(id,"block",null,"");
                }

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
