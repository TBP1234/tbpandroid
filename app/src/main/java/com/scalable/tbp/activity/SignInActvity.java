package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.InterstitialAd;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.registrationPojo.Datum;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.SignInrepository;
import com.scalable.tbp.dagger.DaggerSignInScreenComponent;
import com.scalable.tbp.dagger.SignInSreenModule;
import com.scalable.tbp.ui.SignInPresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class SignInActvity extends AppCompatActivity implements SignInrepository.SignInView, View.OnClickListener, Validator.ValidationListener, CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.title)
    TextView title;

    @NotEmpty
    @BindView(R.id.edt_username)
    EditText edtUsername;

    @NotEmpty
    @BindView(R.id.edt_pass)
    EditText edtPass;

    @BindView(R.id.txt_frpass)
    TextView txtFrpass;

    @BindView(R.id.btn_sign)
    Button btnSign;

    @BindView(R.id.btn_newsign)
    TextView btnNewsign;

    @Inject
    SignInPresenterImp signInPresenterImp;

    @BindView(R.id.btn_company)
    Button btnCompany;

    @BindView(R.id.btn_influencer)
    Button btnInfluencer;

    @BindView(R.id.txt_funame)
    TextView txtFuname;

    @BindView(R.id.btn_company1)
    Button btnCompany1;

    @BindView(R.id.btn_influencer1)
    Button btnInfluencer1;

    @Inject
    Connectivity connectivity;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    @BindView(R.id.chk_remember)
    CheckBox chkRemember;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Validator validator;
    private ProgressDialog progressDialog;
    private int code=0;
    private String TAG1 = SignInActvity.class.getSimpleName();
    InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_actvity);
        ButterKnife.bind(this);

        DaggerSignInScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .signInSreenModule(new SignInSreenModule(this))
                .build().inject(this);
        init();
        bindevent();

        signInPresenterImp.geyType();
        signInPresenterImp.setColor();
        signInPresenterImp.showRememberMe();

    }




    @Override
    public void showProgressbar() {

        if (progressDialog != null)
            progressDialog.show();

    }

    @Override
    public void hideProgressbar() {

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void showError(String s) {

        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void init() {


        title.setText("SignIn");
        progressDialog = HelperMethods.getPrgressDiloge(this, "Please wait ...");

        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);

        validator = new Validator(this);
        validator.setValidationListener(this);


    }

    @Override
    public void bindevent() {
        btnSign.setOnClickListener(this);
        btnNewsign.setOnClickListener(this);
        txtFrpass.setOnClickListener(this);
        btnCompany.setOnClickListener(this);
        btnInfluencer.setOnClickListener(this);
        btnInfluencer1.setOnClickListener(this);
        btnCompany1.setOnClickListener(this);
        txtFuname.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        chkRemember.setOnCheckedChangeListener(this);

    }

    @Override
    public void clearEditText() {

        edtUsername.setText("");
        edtPass.setText("");

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void moveToDashboardView(String type, Datum datum) {

        Intent intent = new Intent(SignInActvity.this, MainActivity.class);
        intent.putExtra("data", type);
        intent.putExtra("datum", datum);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }

    @Override
    public void moveToForgotPassWordView() {

        Intent intent = new Intent(SignInActvity.this, ForgotPasswordActvity.class);
        startActivity(intent);

    }

    @Override
    public void moveToForgotUsernameView() {

        Intent intent = new Intent(SignInActvity.this, ForgotUserNameActvity.class);
        startActivity(intent);

    }

    @Override
    public void moveToSignUPView() {

        Intent intent = new Intent(SignInActvity.this, SignUpActivity.class);
        startActivity(intent);

    }

    @Override
    public void moveToCompanyCreatProfileView(int userId) {

        Intent intent = new Intent(SignInActvity.this, CreateCompanyProfileActivity.class);
       intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
       intent.putExtra("userId",userId);
        startActivity(intent);


    }

    @Override
    public void moveToInfluencerCreatProfileView(int userId) {

        Intent intent = new Intent(SignInActvity.this, CreateInfluencerProfileActivity.class);
       // hgfhgfghfhy
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("userId",userId);
        startActivity(intent);
       //  finish();

    }

    @Override
    public void showTypeCompany() {

//        btnCompany.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//        btnInfluencer.setBackgroundColor(getResources().getColor(R.color.white));


    }

    @Override
    public void showTypeInfluencer() {


//        btnCompany.setBackgroundColor(getResources().getColor(R.color.white));
//        btnInfluencer.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

    }

    @Override
    public void setrememberMe(Boolean aBoolean) {

        if (aBoolean) {

            if (code==0){

                chkRemember.setBackground(getResources().getDrawable(R.drawable.checkbox_b2));

            }else {

                chkRemember.setBackground(getResources().getDrawable(R.drawable.checkbox_a2));

            }
                 chkRemember.setChecked(true);

        } else {

            if (code==0){

                chkRemember.setBackground(getResources().getDrawable(R.drawable.checkbox_b1));

            }else {

                chkRemember.setBackground(getResources().getDrawable(R.drawable.checkbox_a1));

            }
               chkRemember.setChecked(false);
        }
    }

    @Override
    public void setUserPassword(String username, String password) {

        edtPass.setText(password);
        edtUsername.setText(username);

    }

    @Override
    public void setColor(int code) {
        int color = 0;

        this.code=code;
        if (code == 0) {

            color = getResources().getColor(R.color.first);
            toolbar.setBackgroundColor(color);
            btnSign.setBackgroundColor(color);
            txtFrpass.setTextColor(color);
            txtFuname.setTextColor(color);


        } else {

            color = getResources().getColor(R.color.second);
            toolbar.setBackgroundColor(color);
            btnSign.setBackgroundColor(color);
            txtFrpass.setTextColor(color);
            txtFuname.setTextColor(color);

        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_sign:
                //Todo

                validator.validate();
                break;

            case R.id.btn_newsign:

              //  moveToSignUPView();

                break;
            case R.id.txt_frpass:

                moveToForgotPassWordView();

                break;
            case R.id.txt_funame:

                moveToForgotUsernameView();

                break;

            case R.id.btn_influencer:

                signInPresenterImp.setType(Constants.INFLUENCER);

                break;

            case R.id.btn_company:

                signInPresenterImp.setType(Constants.COMPANY);


                break;

            case R.id.btn_company1:

                signInPresenterImp.setType(Constants.COMPANY);
                moveToSignUPView();

                break;
            case R.id.btn_influencer1:

                signInPresenterImp.setType(Constants.INFLUENCER);
                moveToSignUPView();
                break;

            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {

        //Todo

        if (connectivity.isConnected()) {

            String name = edtUsername.getText().toString().trim();
            String pass = edtPass.getText().toString().trim();
            signInPresenterImp.doSignIn(name, pass);

        } else {

            Msg.t(this, getString(R.string.intenet_not_conected));

        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(SignInActvity.this, SelectUserActvity.class);
        startActivity(intent);
        finish();


    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {

            signInPresenterImp.setRememberMe(true);

        } else {

            signInPresenterImp.setRememberMe(false);

        }

    }


}
