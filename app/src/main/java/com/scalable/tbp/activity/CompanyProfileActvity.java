package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.registrationPojo.Datum;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.CompanyProfileRepository;
import com.scalable.tbp.dagger.CompanyProfileScreenModule;
import com.scalable.tbp.dagger.DaggerCompanyProfileScreenComponent;
import com.scalable.tbp.ui.CompanyProfilePresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CompanyProfileActvity extends AppCompatActivity implements CompanyProfileRepository.CompanyProfileView, View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.txt_header)
    TextView txtHeader;

    @BindView(R.id.text_header1)
    TextView textHeader1;

    @BindView(R.id.text_header2)
    TextView textHeader2;

    @BindView(R.id.btn_chat)
    Button btnChat;

    @BindView(R.id.txt_abt)
    TextView txtAbt;

    @BindView(R.id.txt_cnt_name)
    TextView txtCntName;

    @BindView(R.id.txt_cnt_email)
    TextView txtCntEmail;

    @Inject
    Connectivity connectivity;

    @Inject
    CompanyProfilePresenterImp companyProfilePresenterImp;

    @BindView(R.id.btn_logout)
    TextView btnLogout;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    @BindView(R.id.textView25)
    TextView textView25;

    @BindView(R.id.textView26)
    TextView textView26;

    @BindView(R.id.textView27)
    TextView textView27;

    @BindView(R.id.textView28)
    TextView textView28;

    @BindView(R.id.textView30)
    TextView textView30;

    @BindView(R.id.textView31)
    TextView textView31;

    @BindView(R.id.textView32)
    TextView textView32;

    @BindView(R.id.textView35)
    TextView textView35;

    @BindView(R.id.btn_edit_profile)
    TextView btnEditProfile;

    @BindView(R.id.btn_change_password)
    TextView btnChangePassword;

    @BindView(R.id.txt_type)
    TextView txtType;

    private ProgressDialog progressDialog;

    private Datum datum;
    private String uid="";
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_profile_actvity);
        ButterKnife.bind(this);

        mInterstitialAd = new InterstitialAd(this);
        //  set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.banner_ad_unit_id));




        //inject Dagger
        DaggerCompanyProfileScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .companyProfileScreenModule(new CompanyProfileScreenModule(this))
                .build()
                .inject(this);


        init();

        bindevent();
        AdRequest adRequest = new AdRequest.Builder()
                // .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                //  .addTestDevice("1082AEA199A2D33C6C69795FB7C61B28")
                .build();

        //  Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }

            private void showInterstitial() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });


        companyProfilePresenterImp.dosetColor();

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("uid")) {

            if (connectivity.isConnected()) {

                 uid = intent.getStringExtra("uid");
                companyProfilePresenterImp.fetchaUserdetail(uid);
                btnChat.setText(getString(R.string.CHAT_WITH_ME));

            } else {

                showError(getString(R.string.intenet_not_conected));
            }


        } else {

            if (connectivity.isConnected()) {

                companyProfilePresenterImp.fetchaUserdetail(null);
                btnChat.setText(getString(R.string.MY_CHAT));

            } else {

                showError(getString(R.string.intenet_not_conected));
            }

        }

    }

    @Override
    public void showProgressbar() {

        if (progressDialog != null) {

            progressDialog.show();

        }
    }

    @Override
    public void hideProgressbar() {

        if (progressDialog != null && progressDialog.isShowing()) {

            progressDialog.dismiss();
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void showError(String s) {

        Msg.t(this, s);
    }

    @Override
    public void init() {

        title.setText("Company Profile");
        toolbar.setBackgroundColor(getResources().getColor(R.color.first));
        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);


        progressDialog = HelperMethods.getPrgressDiloge(CompanyProfileActvity.this, "Please wait...");
    }

    @Override
    public void bindevent() {

        btnLogout.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        btnEditProfile.setOnClickListener(this);
        btnChangePassword.setOnClickListener(this);
        btnChat.setOnClickListener(this);
    }

    @Override
    public void clearEditText() {


    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public void showData(Datum datum) {

        this.datum = datum;

        txtAbt.setText(datum.getAbout());
        txtHeader.setText(datum.getCompanyName());
        textHeader1.setText(datum.getCity() + " ," + datum.getCountry());
        textHeader2.setText("Branch :" + datum.getBranch());
        txtCntEmail.setText(datum.getEmail());
        txtType.setText("Product Type : "+datum.getTypeOfProduct());
        txtCntName.setText(datum.getFirstname() + " " + datum.getLastName());

    }

    @Override
    public void showlogEdit() {

        title.setText("My Profile");
        btnEditProfile.setVisibility(View.VISIBLE);
        btnLogout.setVisibility(View.VISIBLE);
        btnChangePassword.setVisibility(View.VISIBLE);

    }

    @Override
    public void moveToLoginView() {

        Intent intent = new Intent(CompanyProfileActvity.this, AboutActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }

    @Override
    public void moveToEditProfileView(Datum datum) {

        Intent intent = new Intent(CompanyProfileActvity.this, UpdateCompanyProfileActivity.class);
        intent.putExtra("data", datum);
        startActivity(intent);

    }

    @Override
    public void moveToChangePasswordView() {

        Intent intent = new Intent(CompanyProfileActvity.this, ChangePasswordActvity.class);
        startActivity(intent);

    }

    @Override
    public void moveToChatView(String name) {

        Intent intent = new Intent(CompanyProfileActvity.this, ChatActivity.class);
        intent.putExtra("data", datum.getCompanyName());
        intent.putExtra("id",uid);
        startActivity(intent);

    }

    @Override
    public void moveToChatListView() {

        Intent intent= new Intent(CompanyProfileActvity.this,ChatListActivity.class);
        startActivity(intent);

    }

    @Override
    public void showInfluencerColor() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.second));
        btnChat.setBackgroundColor(getResources().getColor(R.color.second));



    }

    @Override
    public void showComapanyColor() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.first));
        btnChat.setBackgroundColor(getResources().getColor(R.color.first));

    }

    @Override
    protected void onDestroy() {

        companyProfilePresenterImp = null;
        super.onDestroy();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_logout:

                companyProfilePresenterImp.logOut();
//
                break;

            case R.id.img_back:

                onBackPressed();

                break;
            case R.id.btn_edit_profile:

                moveToEditProfileView(datum);

                break;

            case R.id.btn_change_password:

                companyProfilePresenterImp.GoChangePassword();

                break;

            case R.id.btn_chat:

                companyProfilePresenterImp.dochatButtonClicked(uid);

                break;
        }
    }




}
