package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.chatList.Datum;
import com.scalable.tbp.Pojo.requestList.SendList;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.ChatListRepository;
import com.scalable.tbp.adapter.ChatListAdapter;
import com.scalable.tbp.dagger.ChatLIstSreenModule;
import com.scalable.tbp.dagger.DaggerChatListScreenComponent;
import com.scalable.tbp.ui.ChatListPresenterImp;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.RecyclerItemClickListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatListActivity extends AppCompatActivity implements ChatListRepository.ChatListView{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.img_profle)
    ImageView imgProfle;

    @BindView(R.id.img_chat)
    ImageView imgChat;

    @BindView(R.id.rec_chat_list)
    RecyclerView recChatList;

    ProgressDialog progressDialog;

    @Inject
    ChatListPresenterImp chatListPresenterImp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        ButterKnife.bind(this);

        //inject Dagger
        DaggerChatListScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .chatLIstSreenModule(new ChatLIstSreenModule(this))
                .build().inject(this);


        init();
        bindevent();
        chatListPresenterImp.setColor();
        chatListPresenterImp.fetchChatData();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void showProgressbar() {

        if (progressDialog!=null){
            progressDialog.show();
        }

    }

    @Override
    public void hideProgressbar() {

        if (progressDialog!=null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));

    }

    @Override
    public void showError(String msg) {

        Msg.t(this,msg);

    }

    @Override
    public void init() {

        progressDialog = HelperMethods.getPrgressDiloge(this, getString(R.string.pleasewait));

        imgProfle.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.INBOX));
        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);

    }

    @Override
    public void bindevent() {


    }

    @Override
    public void showChat(final List<Object> arrayList) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recChatList.setLayoutManager(layoutManager);

        if(arrayList!=null && arrayList.size()>0) {

            ChatListAdapter chatListAdapter = new ChatListAdapter(this, arrayList);
            recChatList.setAdapter(chatListAdapter);
        }else {

            showError("No chat available");
        }

        recChatList.addOnItemTouchListener(
                new RecyclerItemClickListener(ChatListActivity.this, recChatList ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        if (arrayList.get(position) instanceof Datum){

                            Datum datum= (Datum) arrayList.get(position);

                            Intent intent= new Intent(ChatListActivity.this,ChatActivity.class);
                            intent.putExtra("data",datum.getFname()+" "+datum.getLname());
                            intent.putExtra("id",datum.getUserid());
                            startActivity(intent);

                        }else if(arrayList.get(position) instanceof SendList){

                            SendList datum = (SendList) arrayList.get(position);
                            Intent intent= new Intent(ChatListActivity.this,ChatActivity.class);
                            intent.putExtra("data",datum.getFname()+" "+datum.getLname());
                            intent.putExtra("id",datum.getUserId());
                            startActivity(intent);

                        }else if(arrayList.get(position) instanceof com.scalable.tbp.Pojo.requestList.Datum){

                            com.scalable.tbp.Pojo.requestList.Datum datum = (com.scalable.tbp.Pojo.requestList.Datum) arrayList.get(position);
                            Intent intent= new Intent(ChatListActivity.this,ChatActivity.class);
                            intent.putExtra("data",datum.getFname()+" "+datum.getLname());
                            intent.putExtra("id",datum.getUserId());
                            startActivity(intent);

                        }

                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever

                    }
                })
        );

    }

    @Override
    public void clearEdittext() {


    }


    @Override
    public void moveToInfluencerProfileView() {

        Intent intent= new Intent(ChatListActivity.this,InfluencerProfileActivity.class);
        startActivity(intent);

    }

    @Override
    public void moveToComapanyProfileView() {

        Intent intent= new Intent(ChatListActivity.this,CompanyProfileActvity.class);
        startActivity(intent);

    }

    @Override
    public void showInfluencerColor() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.second));
    }

    @Override
    public void showComapanyColor() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.first));
    }

    @OnClick({R.id.img_back, R.id.img_toplogo, R.id.img_profle})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();

                break;
            case R.id.img_toplogo:

                onBackPressed();

                break;
            case R.id.img_profle:

                chatListPresenterImp.doProfileButtunClicked();
                break;
            }

    }
}
