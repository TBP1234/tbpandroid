package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.registrationPojo.Datum;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.InfluencerProfileRepository;
import com.scalable.tbp.dagger.DaggerInfluencerProfileScreenComponent;
import com.scalable.tbp.dagger.InfluencerProfileScreenModule;
import com.scalable.tbp.ui.InfulencerProfilePresenterImp;
import com.scalable.tbp.utility.CircleTransform;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InfluencerProfileActivity extends AppCompatActivity implements InfluencerProfileRepository.InfluencerProfileView, View.OnClickListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.prof)
    ImageView imgProfle;

    @BindView(R.id.txt_name)
    TextView txtName;

    @BindView(R.id.txt_gndr)
    TextView txtGndr;

    @BindView(R.id.txt_snp)
    TextView txtSnp;

    @BindView(R.id.txt_insta)
    TextView txtInsta;

    @BindView(R.id.txt_fack)
    TextView txtFack;

    @BindView(R.id.txt_adss)
    TextView txtAdss;

    @BindView(R.id.btn_chat)
    Button btnChat;

    @BindView(R.id.abt)
    TextView abt;

    @BindView(R.id.edt_insta)
    EditText edtInsta;

    @BindView(R.id.edt_facebook)
    EditText edtFacebook;

    @BindView(R.id.btn_snapchat)
    Button btnSnapchat;

    @BindView(R.id.edt_snapchat)
    EditText edtSnapchat;

    @BindView(R.id.logout)
    TextView logout;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    @BindView(R.id.btn_insta)
    Button btnInsta;

    @BindView(R.id.btn_facebook)
    Button btnFacebook;

    @BindView(R.id.text_insta_var)
    TextView textInstaVar;

    @BindView(R.id.text_fac_var)
    TextView textFacVar;

    @BindView(R.id.text_snap_var)
    TextView textSnapVar;

    @BindView(R.id.btn_edit_profile)
    TextView btnEditProfile;

    @BindView(R.id.btn_influ_change_password)
    TextView btnInfluChangePassword;

    @BindView(R.id.inf_pro_changeInsta)
    TextView infProChangeInsta;

    @BindView(R.id.inf_change_face)
    TextView infChangeFace;

    @BindView(R.id.inf_change_snap)
    TextView infChangeSnap;

    @BindView(R.id.img_snap_prf)
    ImageView imgSnapPrf;

    @BindView(R.id.img_insta_prf)
    ImageView imgInstaPrf;

    @BindView(R.id.img_fck_prf)
    ImageView imgFckPrf;

    private ProgressDialog progressDialog;

    @Inject
    InfulencerProfilePresenterImp infulencerProfilePresenterImp;

    private Datum datum;
    private String uid = "";
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inflencer_profile);
        ButterKnife.bind(this);

        mInterstitialAd = new InterstitialAd(this);
        //  set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.banner_ad_unit_id));


        //inject Dagger
        DaggerInfluencerProfileScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .influencerProfileScreenModule(new InfluencerProfileScreenModule(this))
                .build()
                .inject(this);

        title.setText("Influencer Profile");


        init();

        bindevent();


        infulencerProfilePresenterImp.setColor();

        AdRequest adRequest = new AdRequest.Builder()
                // .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                //  .addTestDevice("1082AEA199A2D33C6C69795FB7C61B28")
                .build();

        //  Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }

            private void showInterstitial() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("uid")) {

            uid = intent.getStringExtra("uid");
            infulencerProfilePresenterImp.fetchaUserdetail(uid);
            btnChat.setText(getString(R.string.CHAT_WITH_ME));


        } else {

            infulencerProfilePresenterImp.fetchaUserdetail(null);
            btnChat.setText(getString(R.string.MY_CHAT));

        }

    }

    @Override
    public void showProgressbar() {

        if (progressDialog != null) {
            progressDialog.show();
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void hideProgressbar() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();

        }
    }

    @Override
    public void showError(String s) {

        Msg.t(this, s);

    }

    @Override
    public void init() {


        toolbar.setBackgroundColor(getResources().getColor(R.color.second));
        progressDialog = HelperMethods.getPrgressDiloge(this, "Please wait ...");


        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);

    }

    @Override
    public void bindevent() {

        logout.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        btnEditProfile.setOnClickListener(this);
        btnInfluChangePassword.setOnClickListener(this);
        btnChat.setOnClickListener(this);
        imgToplogo.setOnClickListener(this);
        imgInstaPrf.setOnClickListener(this);
        imgFckPrf.setOnClickListener(this);
        imgSnapPrf.setOnClickListener(this);

    }


    @Override
    public void showData(final Datum datum) {


        this.datum = datum;
        if (datum.getGender() != null) {

            txtGndr.setText("Gender : " + datum.getGender());

        } else {

            txtGndr.setText("");

        }

        txtName.setText(datum.getFirstname() + " " + datum.getLastName());
       String lstname= datum.getLastName();
        Log.e("lstname",lstname);
        txtAdss.setText(datum.getCountry() + ", " + datum.getCity());
        abt.setText(datum.getAbout());

        txtInsta.setText(HelperMethods.getFollowers(datum.getInstagramFollowers()));
        txtFack.setText(HelperMethods.getFollowers(datum.getFacebookFollowers()));
        txtSnp.setText(HelperMethods.getFollowers(datum.getSnapchatFollowers()));

        edtFacebook.setText(HelperMethods.getFollowers(datum.getFacebookFollowers()));
        edtInsta.setText(HelperMethods.getFollowers(datum.getInstagramFollowers()));
        edtSnapchat.setText(HelperMethods.getFollowers(datum.getSnapchatFollowers()));

        if (!datum.getFacebookFollowers().equals("")) {

            textFacVar.setVisibility(View.VISIBLE);
        }

        if (!datum.getSnapchatFollowers().equals("")) {

            textSnapVar.setVisibility(View.INVISIBLE);
        }

        if (!datum.getInstagramFollowers().equals("")) {

            textInstaVar.setVisibility(View.VISIBLE);
        }


        if (datum.getImage() != null && !datum.getImage().equals("")) {

            Msg.l("image url ", "" + datum.getImage());

            Picasso.with(InfluencerProfileActivity.this).load(datum.getImage().trim()).transform(new CircleTransform()).into(imgProfle);

        }

    }

    @Override
    public void moveToLoginView() {

        Intent intent = new Intent(InfluencerProfileActivity.this, AboutActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void moveToEditProfileView(Datum datum) {

        Intent intent = new Intent(InfluencerProfileActivity.this, UpdateInfluencerProfileActvity.class);
        intent.putExtra("data", datum);
        startActivity(intent);

    }

    @Override
    public void moveToChangePasswordView() {

        Intent intent = new Intent(InfluencerProfileActivity.this, ChangePasswordActvity.class);
        startActivity(intent);
    }

    @Override
    public void moveToChatView() {

        Intent intent = new Intent(InfluencerProfileActivity.this, ChatActivity.class);
        intent.putExtra("data", datum.getFirstname());
        intent.putExtra("id", uid);
        startActivity(intent);
    }

    @Override
    public void moveToChatListView() {

        Intent intent = new Intent(InfluencerProfileActivity.this, ChatListActivity.class);
        startActivity(intent);

    }

    @Override
    public void showLogEdit() {

        title.setText("My Profile");
        btnEditProfile.setVisibility(View.VISIBLE);
        logout.setVisibility(View.VISIBLE);
        btnInfluChangePassword.setVisibility(View.VISIBLE);
    }

    @Override
    public void showInfluencerColor() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.second));
        btnChat.setBackgroundColor(getResources().getColor(R.color.second));
        btnFacebook.setBackgroundColor(getResources().getColor(R.color.second));
        btnInsta.setBackgroundColor(getResources().getColor(R.color.second));
        btnSnapchat.setBackgroundColor(getResources().getColor(R.color.second));
        imgProfle.setBackground(getResources().getDrawable(R.drawable.default_profile_b));
        imgFckPrf.setBackground(getResources().getDrawable(R.drawable.facebook3));
        imgInstaPrf.setBackground(getResources().getDrawable(R.drawable.instagram3));
        imgSnapPrf.setBackground(getResources().getDrawable(R.drawable.snapchat3));

        String first = "Verify <font color=#ee2b7a> Instagram </font> account ";
        infProChangeInsta.setText(Html.fromHtml(first));

        String second = "Varify <font color=#ee2b7a> Facebook </font>  Account";
        infChangeFace.setText(Html.fromHtml(second));

        String third = "Varify <font color=#ee2b7a> Snapchat </font> Account";
        infChangeSnap.setText(Html.fromHtml(third));



    }

    @Override
    public void showComapanyColor() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.first));
        btnChat.setBackgroundColor(getResources().getColor(R.color.first));
        btnFacebook.setBackgroundColor(getResources().getColor(R.color.first));
        btnInsta.setBackgroundColor(getResources().getColor(R.color.first));
        btnSnapchat.setBackgroundColor(getResources().getColor(R.color.first));
        imgProfle.setBackground(getResources().getDrawable(R.drawable.default_profile_a));
        imgFckPrf.setBackground(getResources().getDrawable(R.drawable.facebook2));
        imgInstaPrf.setBackground(getResources().getDrawable(R.drawable.instagram2));
        imgSnapPrf.setBackground(getResources().getDrawable(R.drawable.snapchat2));

        String first = "Verify <font color=#f1592a> Instagram </font> account ";
        infProChangeInsta.setText(Html.fromHtml(first));

        String second = "Varify <font color=#f1592a> Facebook </font>  Account";
        infChangeFace.setText(Html.fromHtml(second));

        String third = "Varify <font color=#f1592a> Snapchat </font> Account";
        infChangeSnap.setText(Html.fromHtml(third));



    }

    @Override
    public void showWebUrl(String url) {

        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.logout:

                infulencerProfilePresenterImp.logOut();

                break;

            case R.id.img_back:

                onBackPressed();

                break;


            case R.id.img_toplogo:

                onBackPressed();

                break;

            case R.id.btn_influ_change_password:

                infulencerProfilePresenterImp.goToChangePassword();

                break;

            case R.id.btn_edit_profile:

                moveToEditProfileView(datum);

                break;


            case R.id.img_fck_prf:

                infulencerProfilePresenterImp.dofaceProfileButtonClicked(datum);

                break;

            case R.id.img_insta_prf:


                infulencerProfilePresenterImp.doInstaProfileButtonClicked(datum);
                break;

            case R.id.img_snap_prf:

                infulencerProfilePresenterImp.dosnapProfileButtonClicked(datum);

                break;


            case R.id.btn_chat:

                infulencerProfilePresenterImp.dochatButtonClicked(uid);

                break;

        }


    }
}
