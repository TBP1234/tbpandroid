package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.companyList.CompanyList;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.county.Datum;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.CompanyFilterRepository;
import com.scalable.tbp.dagger.ComapanyFilterSreenModule;
import com.scalable.tbp.dagger.DaggerCompanyfilterScreenComponent;
import com.scalable.tbp.ui.CompanyfilterPresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.SearchableListDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CompanyFiltterActivity extends AppCompatActivity implements CompanyFilterRepository.CompanyFilterView ,View.OnClickListener{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.img_profle)
    ImageView imgProfle;

    @BindView(R.id.img_chat)
    ImageView imgChat;

    @BindView(R.id.edt_country)
    EditText edtCountry;

    @BindView(R.id.edt_city)
    EditText edtCity;

    @BindView(R.id.edt_branche)
    EditText edtBranche;

    @BindView(R.id.btn_search)
    Button btnSearch;

    ProgressDialog progressDialog;
    @BindView(R.id.edt_cname)
    EditText edtCname;

    private SearchableListDialog _searchableCountryListDialog;

    private SearchableListDialog _searchableCityListDialog;

    private String countryCode;
    private String cityCode;


    @Inject
    Connectivity connectivity;

    @Inject
    CompanyfilterPresenterImp companyfilterPresenterImp;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_filtter);
        ButterKnife.bind(this);

        mInterstitialAd = new InterstitialAd(this);
        //  set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.banner_ad_unit_id));
        init();
        bindevent();


//inject Dagger
        DaggerCompanyfilterScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .comapanyFilterSreenModule(new ComapanyFilterSreenModule(this))
                .build().inject(this);

        if (connectivity.isConnected()){

            companyfilterPresenterImp.getCountry();

        }else {

            showError(getResources().getString(R.string.intenet_not_conected));
        }
        AdRequest adRequest = new AdRequest.Builder()
                // .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                //  .addTestDevice("1082AEA199A2D33C6C69795FB7C61B28")
                .build();

        //  Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }

            private void showInterstitial() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void showProgressbar() {

        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    @Override
    public void hideProgressbar() {
        if (progressDialog != null && progressDialog.isShowing() ){
            progressDialog.dismiss();
        }
    }

    @Override
    public void showError(String s) {

        Msg.t(this,s);
    }

    @Override
    public void init() {
        progressDialog = HelperMethods.getPrgressDiloge(this, getResources().getString(R.string.pleasewait));
        toolbar.setBackgroundColor(getResources().getColor(R.color.second));

        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);
        title.setText("Find Company");

    }

    @Override
    public void bindevent() {

        btnSearch.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgToplogo.setOnClickListener(this);
        edtCountry.setOnClickListener(this);
        edtCity.setOnClickListener(this);
    }

    @Override
    public void clearEditText() {

        edtBranche.setText("");
        edtCity.setText("");
        edtCountry.setText("");
        edtCname.setText("");

    }

    @Override
    public void moveToMainView(CompanyList companyList) {

        Intent intent = new Intent();

        if (companyList!=null){
            // put the String to pass back into an Intent and close this activity
            intent.putExtra("data", companyList);
        }
        setResult(RESULT_OK, intent);
        finish();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_search:

                // Statements
                if (connectivity.isConnected()){

                    String name=edtCname.getText().toString();
                    String country=edtCountry.getText().toString();
                    String city=edtCity.getText().toString();
                    String branch=edtBranche.getText().toString();
                    companyfilterPresenterImp.doSearchButtonClick(name,country,city,branch);

                }else {

                    showError(getString(R.string.intenet_not_conected));
                }

                break; // optional


            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.img_toplogo:
                onBackPressed();
                break;

            case R.id.edt_country:

                showCountry();

                break;

            case R.id.edt_city:

                showCity();

                break;


        }
    }

    @Override
    public void showCountry() {

        if (_searchableCountryListDialog != null) {

            _searchableCountryListDialog.show(getSupportFragmentManager(), "");

        }

    }

    @Override
    public void setCity(final City city, List list) {

        _searchableCityListDialog = SearchableListDialog.newInstance(list);
        _searchableCityListDialog.setTitle("Select City");
        _searchableCityListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {


                for (com.scalable.tbp.Pojo.city.Datum datum : city.getData()) {
                    if (datum.getCityName().equals(item.toString())) {

                        edtCity.setText(datum.getCityName());
                        cityCode = datum.getCityId();


                    }
                }

            }
        });
    }

    @Override
    public void showCity() {

        if (_searchableCityListDialog != null) {

            _searchableCityListDialog.show(getSupportFragmentManager(), "");
        }

    }

    @Override
    public void setCountry(final Country country, List list) {


        _searchableCountryListDialog = SearchableListDialog.newInstance(list);
        _searchableCountryListDialog.setTitle("Select Country");
        _searchableCountryListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {

                edtCountry.setText(item.toString());

                for (Datum datum : country.getData()) {
                    if (datum.getCountryName().equals(item.toString())) {

                        companyfilterPresenterImp.getCity(datum.getCountryId());
                        countryCode = datum.getCountryId();

                    }
                }

            }
        });
    }

}
