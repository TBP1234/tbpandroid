package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.county.Datum;
import com.scalable.tbp.Pojo.influecncerList.InfluecncerList;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.InluencerFilterRepository;
import com.scalable.tbp.dagger.DaggerInfluencerfilterScreenComponent;
import com.scalable.tbp.dagger.InfluencerFilterSreenModule;
import com.scalable.tbp.ui.InfluencerfilterPresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.SearchableListDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InfluencerFillterActivity extends AppCompatActivity implements InluencerFilterRepository.InfluencerFilterView, View.OnClickListener, View.OnTouchListener {

    @Inject
    Connectivity connectivity;

    ProgressDialog progressDialog;
    @BindView(R.id.toolbar)

    Toolbar toolbar;
    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.img_profle)
    ImageView imgProfle;

    @BindView(R.id.img_chat)
    ImageView imgChat;

    @BindView(R.id.edit_influ_name)
    EditText editInfluName;

    @BindView(R.id.edit_influ_country)
    EditText editInfluCountry;

    @BindView(R.id.edit_influ_city)
    EditText editInfluCity;

    @BindView(R.id.rbtn_male)
    RadioButton rbtnMale;

    @BindView(R.id.rbtn_female)
    RadioButton rbtnFemale;

    @BindView(R.id.rbtn_other)
    RadioButton rbtnOther;

    @BindView(R.id.edit_influ_insta)
    EditText editInfluInsta;

    @BindView(R.id.edit_influ_face)
    EditText editInfluFace;

    @BindView(R.id.edit_influ_snap)
    EditText editInfluSnap;

    @BindView(R.id.btn_serach_influ)
    Button btnSerachInflu;
    @BindView(R.id.rdt_gender)
    RadioGroup rdtGender;

    private SearchableListDialog _searchableCountryListDialog;

    private SearchableListDialog _searchableCityListDialog;


    @Inject
    InfluencerfilterPresenterImp influencerFilterPresenter;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_influencer_fillter);
        ButterKnife.bind(this);

        mInterstitialAd = new InterstitialAd(this);
        //  set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.banner_ad_unit_id));

        //inject Dagger
        DaggerInfluencerfilterScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .influencerFilterSreenModule(new InfluencerFilterSreenModule(this))
                .build().inject(this);

        init();
        bindevent();


        influencerFilterPresenter.getCountry();

        AdRequest adRequest = new AdRequest.Builder()
                // .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                //  .addTestDevice("1082AEA199A2D33C6C69795FB7C61B28")
                .build();

        //  Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }

            private void showInterstitial() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void showProgressbar() {
        if (progressDialog != null) {
            progressDialog.show();
        }

    }

    @Override
    public void hideProgressbar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();

        }

    }

    @Override
    public void showError(String s) {

        Msg.t(this, s);
    }

    @Override
    public void init() {

        progressDialog = HelperMethods.getPrgressDiloge(this, getString(R.string.pleasewait));
        toolbar.setBackgroundColor(getResources().getColor(R.color.first));
        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);
        title.setText("Find Influencer");


//        editInfluInsta.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                final int DRAWABLE_LEFT = 0;
//                final int DRAWABLE_TOP = 1;
//                final int DRAWABLE_RIGHT = 2;
//                final int DRAWABLE_BOTTOM = 3;
//
//                if(event.getAction() == MotionEvent.ACTION_UP) {
//                    if(event.getRawX() >= (editInfluInsta.getRight() - editInfluInsta.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//
//                        Msg.t(InfluencerFillterActivity.this,"insta right");
//
//                        return true;
//                    }
//
//                    if(event.getRawX() >= (editInfluInsta.getLeft() - editInfluInsta.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
//                        // your action here
//
//                        Msg.t(InfluencerFillterActivity.this,"insta left");
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });
//


    }

    @Override
    public void bindevent() {

        imgToplogo.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        btnSerachInflu.setOnClickListener(this);
        editInfluCountry.setOnClickListener(this);
        editInfluCity.setOnClickListener(this);

        editInfluInsta.setOnTouchListener(this);
        editInfluFace.setOnTouchListener(this);
        editInfluSnap.setOnTouchListener(this);

    }

    @Override
    public void clearEditText() {


    }

    @Override
    public void moveToCompanyProfileView() {

    }

    @Override
    public void moveToInfluencerProfileView() {

    }

    @Override
    public void moveToChatView() {


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_serach_influ:
                // Statements

                if (connectivity.isConnected()) {

                    String name=editInfluName.getText().toString();
                    String country=editInfluCountry.getText().toString();
                    String city=editInfluCity.getText().toString();
                    String snap=editInfluSnap.getText().toString().replace("K","");
                    String insta=editInfluInsta.getText().toString().replace("K","");
                    String face=editInfluFace.getText().toString().replace("K","");


                    int checkedRadioButtonId = rdtGender.getCheckedRadioButtonId();

                    influencerFilterPresenter.doSearchButtonClick(name, country, city, checkedRadioButtonId, insta, face, snap);


                } else {

                    showError(getString(R.string.intenet_not_conected));
                }

                break; // optional

            case R.id.img_back:
                // Statements
                onBackPressed();

                break; // optional

            case R.id.img_toplogo:
                // Statements
                onBackPressed();
                break; // optional


            case R.id.edit_influ_city:
                // Statements

                showCity();
                break; // optional


            case R.id.edit_influ_country:
                // Statements
                showCountry();
                break; // optional


        }
    }

    @Override
    public void showCountry() {

        if (_searchableCountryListDialog != null) {

            _searchableCountryListDialog.show(getSupportFragmentManager(), "");

        }

    }

    @Override
    public void setCity(final City city, List list) {

        _searchableCityListDialog = SearchableListDialog.newInstance(list);
        _searchableCityListDialog.setTitle("Select City");
        _searchableCityListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {


                for (com.scalable.tbp.Pojo.city.Datum datum : city.getData()) {
                    if (datum.getCityName().equals(item.toString())) {

                        editInfluCity.setText(datum.getCityName());


                    }
                }

            }
        });
    }

    @Override
    public void showCity() {

        if (_searchableCityListDialog != null) {

            _searchableCityListDialog.show(getSupportFragmentManager(), "");
        }

    }

    @Override
    public void moveToMainView(InfluecncerList influecncerList) {
        Intent intent = new Intent();

        if (influecncerList != null) {
            // put the String to pass back into an Intent and close this activity
            intent.putExtra("data", influecncerList);
        }
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void setCountry(final Country country, List list) {


        _searchableCountryListDialog = SearchableListDialog.newInstance(list);
        _searchableCountryListDialog.setTitle("Select Country");
        _searchableCountryListDialog.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {

                editInfluCountry.setText(item.toString());

                for (Datum datum : country.getData()) {
                    if (datum.getCountryName().equals(item.toString())) {

                        influencerFilterPresenter.getCity(datum.getCountryId());

                    }
                }

            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // Statements
        final int DRAWABLE_LEFT = 0;
        final int DRAWABLE_TOP = 1;
        final int DRAWABLE_RIGHT = 2;
        final int DRAWABLE_BOTTOM = 3;


        switch (v.getId()) {
            case R.id.edit_influ_insta:
                // Statements


                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (editInfluInsta.getRight() - editInfluInsta.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                        // your action here
                        int insta = Integer.parseInt(editInfluInsta.getText().toString().replace("K", ""));

                        insta++;
                        editInfluInsta.setText(insta + "K");
                        return true;

                    }

                    if (event.getRawX() >= (editInfluInsta.getLeft() - editInfluInsta.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                        // your action here
                        int insta = Integer.parseInt(editInfluInsta.getText().toString().replace("K", ""));

                        if (insta != 0) {

                            insta--;
                            editInfluInsta.setText(insta + "K");

                            return true;
                        }


                    }
                }


                break; // optional

            case R.id.edit_influ_snap:

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (editInfluSnap.getRight() - editInfluSnap.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                        // your action here
                        int insta = Integer.parseInt(editInfluSnap.getText().toString().replace("K", ""));

                        insta++;
                        editInfluSnap.setText(insta + "K");
                        return true;

                    }

                    if (event.getRawX() >= (editInfluSnap.getLeft() - editInfluSnap.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                        // your action here
                        int insta = Integer.parseInt(editInfluSnap.getText().toString().replace("K", ""));

                        if (insta != 0) {

                            insta--;
                            editInfluSnap.setText(insta + "K");

                            return true;
                        }


                    }
                }
                break; // optional

            case R.id.edit_influ_face:
                // Statements
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (editInfluFace.getRight() - editInfluFace.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                        // your action here
                        int insta = Integer.parseInt(editInfluFace.getText().toString().replace("K", ""));

                        insta++;
                        editInfluFace.setText(insta + "K");
                        return true;

                    }

                    if (event.getRawX() >= (editInfluFace.getLeft() - editInfluFace.getCompoundDrawables()[DRAWABLE_LEFT].getBounds().width())) {
                        // your action here
                        int insta = Integer.parseInt(editInfluFace.getText().toString().replace("K", ""));

                        if (insta != 0) {

                            insta--;
                            editInfluFace.setText(insta + "K");

                            return true;
                        }


                    }
                }
                break; // optional

        }


        return false;
    }
}
