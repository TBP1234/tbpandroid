package com.scalable.tbp.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.R;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActvity extends AppCompatActivity {

    private String TAG = SplashActvity.class.getSimpleName();
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 5000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the ad unit ID
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_actvity);


        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.scalable.tbp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }





        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {


                // This method will be executed once the timer is over
                // Start your app main activity


                SharedPreferenceHelper sharedPreferenceHelper= new SharedPreferenceHelper(MyApplication.get(SplashActvity.this));
                DataManager dataManager= new DataManager(sharedPreferenceHelper);
                int userid=dataManager.getUserId();

                if (userid==0){

                    Intent i = new Intent(SplashActvity.this, AboutActivity.class);
                    startActivity(i);
                    finish();


                }else {

                    Intent intent = new Intent(SplashActvity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }

//                    Intent i = new Intent(SplashActvity.this, AboutActivity.class);
//                    startActivity(i);



                // close this activity
                finish();

            }
        }, SPLASH_TIME_OUT);
    }



}
