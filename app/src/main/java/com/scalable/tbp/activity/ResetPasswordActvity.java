package com.scalable.tbp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.scalable.tbp.MyApplication;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.ResetPasswordrepository;
import com.scalable.tbp.dagger.DaggerResetPasswordScreenComponent;
import com.scalable.tbp.dagger.ResetPasswordSreenModule;
import com.scalable.tbp.ui.ResetPasswordPresenterImp;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.HelperMethods;
import com.scalable.tbp.utility.Msg;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ResetPasswordActvity extends AppCompatActivity implements ResetPasswordrepository.ResetPasswordView, Validator.ValidationListener, View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_back)
    ImageView imgBack;

    @BindView(R.id.img_toplogo)
    ImageView imgToplogo;

    @BindView(R.id.title)
    TextView title;

    @NotEmpty
    @BindView(R.id.edt_pld_pass)
    EditText edtPldPass;

    @NotEmpty
    @Password(min = 1)
    @BindView(R.id.edt_new_pass)
    EditText edtNewPass;

    @NotEmpty
    @ConfirmPassword
    @BindView(R.id.edt_cnf_pass)
    EditText edtCnfPass;

    @BindView(R.id.btn_change_pass)
    Button btnChangePass;


    private Validator validator;
    private ProgressDialog progressDialog;

    @Inject
    Connectivity connectivity;


    @Inject
    ResetPasswordPresenterImp resetPasswordPresenterImp;
    private String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_actvity);
        ButterKnife.bind(this);

        init();
        bindevent();

        //inject Dagger
        DaggerResetPasswordScreenComponent.builder()
                .appComponent(((MyApplication) getApplicationContext()).getAppComponent())
                .resetPasswordSreenModule(new ResetPasswordSreenModule(this))
                .build().inject(this);

        resetPasswordPresenterImp.dosetColor();

    }

    @Override
    public void showProgressbar() {
        if (progressDialog != null)
            progressDialog.show();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void hideProgressbar() {

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void showError(String s) {

        Msg.t(this, s);
    }

    @Override
    public void clearEditText() {


        edtCnfPass.setText("");
        edtNewPass.setText("");
        edtPldPass.setText("");
    }

    @Override
    public void moveToSignInView() {


        Intent intent = new Intent(ResetPasswordActvity.this, SelectUserActvity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }

    @Override
    public void setcomapanyColor() {


        toolbar.setBackgroundColor(getResources().getColor(R.color.first));
        btnChangePass.setBackgroundColor(getResources().getColor(R.color.first));


    }

    @Override
    public void setInfluencerColor() {

        toolbar.setBackgroundColor(getResources().getColor(R.color.second));
        btnChangePass.setBackgroundColor(getResources().getColor(R.color.second));

    }

    @Override
    public void init() {


        validator = new Validator(this);
        validator.setValidationListener(this);

        title.setText("Change Password");
        progressDialog = HelperMethods.getPrgressDiloge(this, "Please wait ...");

        imgBack.setVisibility(View.VISIBLE);
        imgToplogo.setVisibility(View.VISIBLE);


        
        
        Intent intent= getIntent();
        if (intent!=null && intent.hasExtra("email")){
            
            email=intent.getStringExtra("email");
        }

    }

    @Override
    public void bindevent() {

        imgBack.setOnClickListener(this);
        btnChangePass.setOnClickListener(this);
    }

    @Override
    public void onValidationSucceeded() {


        if (connectivity.isConnected()) {

            String password = edtNewPass.getText().toString().trim();
            String otp=edtPldPass.getText().toString().trim();
            resetPasswordPresenterImp.doChangePasswordName(password,otp,email);

        } else {

            Msg.t(this, getString(R.string.intenet_not_conected));

        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_change_pass:
                // Statements
                validator.validate();

                break; // optional

            case R.id.img_back:

                onBackPressed();

                break; // optional

        }

    }
}
