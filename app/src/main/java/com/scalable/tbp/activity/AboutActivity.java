package com.scalable.tbp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.scalable.tbp.R;
import com.scalable.tbp.Repository.Aboutrepository;
import com.scalable.tbp.ui.AboutPresenterImp;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AboutActivity extends AppCompatActivity implements Aboutrepository.AboutView, View.OnClickListener {

    @BindView(R.id.btn_next)
    Button btnNext;
    Aboutrepository.AboutPresenter aboutPresenter;

    @BindView(R.id.title)
    TextView title;
    private int backButtonCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);


        title.setText("About Us");

        btnNext.setOnClickListener(this);
        aboutPresenter = new AboutPresenterImp(this);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void goToNextScreen() {

        Intent intent = new Intent(AboutActivity.this, SelectUserActvity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {

        aboutPresenter.onNextClicked();

    }

    @Override
    public void onBackPressed() {
        if(backButtonCount >= 1)
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }

}
