package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Repository.CompanyProfileRepository;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 11/10/17.
 */

public class CompanyProfilePresenterImp implements CompanyProfileRepository.CompanyProfilePresenter {

    private final NetworkHelper networkHelper;
    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    CompanyProfileRepository.CompanyProfileView companyProfileView;
    DataManager dataManager;

    @Inject
    public CompanyProfilePresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, CompanyProfileRepository.CompanyProfileView companyProfileView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.companyProfileView = companyProfileView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager = new DataManager(sharedPreferenceHelper);
    }

    @Override
    public void fetchaUserdetail(String uid) {

        companyProfileView.showProgressbar();

        int userId=0;

        if (uid!=null){

            userId=Integer.parseInt(uid);


        }else {

            userId=dataManager.getUserId();
            companyProfileView.showlogEdit();
        }


        Data data = new Data();
        data.setUserId(userId);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_USER_DETAIL);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.getUserDetail(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        companyProfileView.hideProgressbar();
                        companyProfileView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        companyProfileView.hideProgressbar();

                        if (pojo != null) {
                            Msg.l("result", pojo.getMessage());
                            if (pojo.getMsgCode() == 0) {

                                companyProfileView.showError(pojo.getMessage());

                            } else if (pojo.getMsgCode() == 1) {

                                //companyProfileView.clearEditText();
                           //     dataManager.setUserId(pojo.getUserid());
                                if (pojo.getData().size()>0){

                                    companyProfileView.showData(pojo.getData().get(0));

                                }else {

                                    companyProfileView.showError("fetching data error...");
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void logOut() {

        companyProfileView.showProgressbar();
        int  userId=dataManager.getUserId();
        Data data = new Data();
        data.setUserId(userId);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_LOGOUT);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.getUserDetail(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {

                        companyProfileView.hideProgressbar();
                        companyProfileView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        companyProfileView.hideProgressbar();

                        if (pojo != null) {
                            Msg.l("result", pojo.getMessage());

                            dataManager.logOut();
                            companyProfileView.moveToLoginView();

                            if (pojo.getMsgCode() == 0) {

                            //    companyProfileView.showError(pojo.getMessage());

                            } else if (pojo.getMsgCode() == 1) {

//                                dataManager.logOut();
//                                companyProfileView.moveToLoginView();

                            }
                        }
                    }
                });
    }

    @Override
    public void GoChangePassword() {

        companyProfileView.moveToChangePasswordView();

    }

    @Override
    public void dochatButtonClicked(String id) {

        if (id!=null && !id.isEmpty()){

            companyProfileView.moveToChatView("");

        }else {

            companyProfileView.moveToChatListView();

        }

    }

    @Override
    public void dosetColor() {
        String type=dataManager.getType();
        if (type.equals(Constants.COMPANY)){

            companyProfileView.showComapanyColor();

        }else {

            companyProfileView.showInfluencerColor();

        }

    }
}
