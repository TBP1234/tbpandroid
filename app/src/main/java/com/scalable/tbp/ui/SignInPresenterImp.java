package com.scalable.tbp.ui;

import android.content.Intent;

import com.google.gson.Gson;
import com.scalable.tbp.Repository.SignInrepository;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 11/10/17.
 */

public class SignInPresenterImp implements SignInrepository.SignInPresenter {

    private final NetworkHelper networkHelper;
    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;

    SignInrepository.SignInView signInView;
    DataManager dataManager;



    @Inject
    public SignInPresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, SignInrepository.SignInView signUpView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.signInView = signUpView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager = new DataManager(sharedPreferenceHelper);
    }

    @Override
    public void doSignIn(final String uname, final String password) {


        signInView.showProgressbar();

        final String type=dataManager.getType();
        final String token=dataManager.getDeviceToken();

        if (type==null || type.equals("")){

            signInView.showError("Select type first...");
            return;
        }

        final Data data = new Data();
        data.setType(type);
        data.setUsername(uname);
        data.setPassword(password);
        data.setDevicetoken(token);
        data.setDevicetype(Constants.DEVICE_TYPE);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName("userLogin");
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.getSignIndetail(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        signInView.hideProgressbar();
                        signInView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        signInView.hideProgressbar();

                        if (pojo != null) {

                            Msg.l("result", pojo.getMessage());

                            if (pojo.getMsgCode() == 0) {

                                signInView.showError(pojo.getMessage());



                            } else if (pojo.getMsgCode() == 1) {

                                signInView.clearEditText();
                                int userId = Integer.parseInt(pojo.getData().get(0).getId().trim());
                                //dataManager.setUserId(Integer.parseInt(pojo.getData().get(0).getId().trim()));


                                    if (pojo.getData().get(0).getActivestatus().trim().equals("1")){

                                        dataManager.setUserId(Integer.parseInt(pojo.getData().get(0).getId()));
                                        dataManager.setloginusername(pojo.getData().get(0).getUsername());
                                        dataManager.setDefaultImage(pojo.getData().get(0).getImage());
                                        dataManager.setDefaultImagePath(pojo.getData().get(0).getUrl());
                                        signInView.moveToDashboardView(type,pojo.getData().get(0));


                                        // code for remember me logic
                                        if (dataManager.getRememberMe()){

                                            dataManager.setUserName(uname);
                                            dataManager.setPassword(password);

                                        }

                                    }else {

                                        String type=dataManager.getType();
                                        if (type.equals(Constants.COMPANY)){

                                            signInView.moveToCompanyCreatProfileView(userId);
                                         //   dataManager.setUserId(Integer.parseInt(pojo.getData().get(0).getId()));

                                        }else {

                                            signInView.moveToInfluencerCreatProfileView(userId);
                                       //     dataManager.setUserId(Integer.parseInt(pojo.getData().get(0).getId()));

                                        }

                                    }

                            }
                        }
                    }
                });


    }
    @Override
    public void setType(String type) {

        dataManager.saveType(type);

        if (type.equals(Constants.COMPANY)) {

            signInView.showTypeCompany();

        } else {

            signInView.showTypeInfluencer();

        }


    }

    @Override
    public void geyType() {

        String type=dataManager.getType();

        if (type.equals(Constants.COMPANY)) {

            signInView.showTypeCompany();

        } else {

            signInView.showTypeInfluencer();

        }
    }



    @Override
    public void setRememberMe(Boolean aBoolean) {

        if (aBoolean){

            dataManager.setRemeberMe(true);
            signInView.setrememberMe(true);


        }else {

            dataManager.setRemeberMe(false);
            signInView.setrememberMe(false);
            dataManager.setUserName("");
            dataManager.setPassword("");

        }

    }

    @Override
    public void showRememberMe() {

        if (dataManager.getRememberMe()){
            signInView.setrememberMe(true);
            signInView.setUserPassword(dataManager.getUserName(),dataManager.getPassword());


        }else {

            signInView.setrememberMe(false);
            signInView.clearEditText();
        }


    }

    @Override
    public void setColor() {

        String type=dataManager.getType();

        if (type.equals(Constants.COMPANY)){

            signInView.setColor(0);

        }else {

            signInView.setColor(1);

        }

    }
}
