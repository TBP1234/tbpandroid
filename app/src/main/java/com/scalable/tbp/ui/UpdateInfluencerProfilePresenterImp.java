package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.UpdateInfuencerProfilerepository;
import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.county.Datum;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 6/10/17.
 */

public class UpdateInfluencerProfilePresenterImp implements UpdateInfuencerProfilerepository.UpdateInfuencerProfilePresenter {


    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    UpdateInfuencerProfilerepository.UpdateInfuencerProfileView updateInfuencerProfileView;


    private NetworkHelper networkHelper;
    private DataManager dataManager;


    @Inject
    public UpdateInfluencerProfilePresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, UpdateInfuencerProfilerepository.UpdateInfuencerProfileView updateInfuencerProfileView ) {

        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.updateInfuencerProfileView = updateInfuencerProfileView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager=new DataManager(sharedPreferenceHelper);
    }

    @Override
    public void doUpdate(String name,String lastName,int gender,String instagram,String facebook,String snapchat,String country,String city,String about,String image,String countryCode,String cityCode,String snapChatId,String facebookProfileUrl,String instaProfileURl) {

        String gendr="";
        switch (gender){

            case R.id.rbtn_male:
                gendr="male";
                break;

            case R.id.rbtn_female:
                gendr="female";
                break;


            case R.id.rbtn_other:
                gendr="other";
                break;

        }

        String type= dataManager.getType();
        int userid=dataManager.getUserId();
        updateInfuencerProfileView.showProgressbar();

        Data data = new Data();
        data.setUserId(userid);
        data.setName(name);
        data.setLastName(lastName);
        data.setGender(gendr);
        data.setType(type);
        data.setInstagramfollowers(instagram);
        data.setFacebookfollowers(facebook);
        data.setSnapchatfollowers(snapchat);
        data.setCountry(country);
        data.setCity(city);
        data.setAbout(about);
        data.setImage(image);
        data.setCountryCode(countryCode);
        data.setCityCode(cityCode);
        data.setSnapchatid(snapChatId);
        data.setFacebookprofileurl(facebookProfileUrl);
        data.setInstagramprofileurl(instaProfileURl);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setData(data);
        registrationPojo.setFuncName(Constants.FUN_EDIT_INFLUENCER_PROFILE);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getSignUpdata(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        updateInfuencerProfileView.showError(e.getMessage());
                        updateInfuencerProfileView.hideProgressbar();

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        updateInfuencerProfileView.clearEditText();
                        updateInfuencerProfileView.hideProgressbar();
                        updateInfuencerProfileView.moveToDashboardView();

                    }
                });

    }

    @Override
    public void setType(String type) {


    }

    @Override
    public void geyType() {


    }

    @Override
    public void getCountry() {

        //updateInfuencerProfileView.showProgressbar();

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_GET_COUNTY);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getCountries(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<Country>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        updateInfuencerProfileView.showError(e.getMessage());
                        updateInfuencerProfileView.hideProgressbar();

                    }

                    @Override
                    public void onNext(Country country) {

                        updateInfuencerProfileView.hideProgressbar();

                        if (country.getMessage().equals(Constants.SUCCESS)){

                            List<String> strings=new ArrayList<>();

                            for (Datum data:country.getData()) {

                                strings.add(data.getCountryName());
                            }

                            updateInfuencerProfileView.setCountry(country,strings);

                        }

                    }
                });
    }

    @Override
    public void getCity(String id) {


        updateInfuencerProfileView.showProgressbar();
        final Data data = new Data();
        data.setCountryId(id);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_GET_CITY);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getCities(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<City>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        updateInfuencerProfileView.showError(e.getMessage());
                        updateInfuencerProfileView.hideProgressbar();

                    }

                    @Override
                    public void onNext(City city) {

                        updateInfuencerProfileView.hideProgressbar();

                        if (city.getMessage().equals(Constants.SUCCESS)){

                            List<String> strings=new ArrayList<>();

                            for (com.scalable.tbp.Pojo.city.Datum data:city.getData()) {

                                strings.add(data.getCityName());
                            }

                            updateInfuencerProfileView.setCity(city,strings);

                        }

                    }
                });
    }
}
