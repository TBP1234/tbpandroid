package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.Repository.ResetPasswordrepository;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by shailendra on 10/10/17.
 */

public class ResetPasswordPresenterImp implements ResetPasswordrepository.ResetPasswordPresenter {

    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    ResetPasswordrepository.ResetPasswordView resetPasswordView;

    private NetworkHelper networkHelper;
    private DataManager dataManager;

    @Inject
    public ResetPasswordPresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, ResetPasswordrepository.ResetPasswordView signUpView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.resetPasswordView = signUpView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager=new DataManager(sharedPreferenceHelper);



    }


    @Override
    public void doChangePasswordName(String password,String otp,String email) {

        int uid=dataManager.getUserId();

        resetPasswordView.showProgressbar();

        Data data = new Data();
        data.setPassword(password);
        data.setEmail(email);
        data.setOtp(otp);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setData(data);
        registrationPojo.setFuncName(Constants.FUN_RESET_PASSWORD);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getSignUpdata(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        resetPasswordView.showError(e.getMessage());
                        resetPasswordView.hideProgressbar();

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        resetPasswordView.hideProgressbar();

                        if (pojo != null) {

                            if (pojo.getMsgCode() == 0) {

                                resetPasswordView.showError(pojo.getMessage());

                            } else {

                                resetPasswordView.clearEditText();
                                dataManager.logOut();
                                resetPasswordView.moveToSignInView();

                            }

                        }
                    }
                });
    }

    @Override
    public void dosetColor() {

        String type= dataManager.getType();
        if (type.equals(Constants.COMPANY)){

            resetPasswordView.setcomapanyColor();

        }else if (type.equals(Constants.INFLUENCER)){

            resetPasswordView.setInfluencerColor();

        }
    }
}
