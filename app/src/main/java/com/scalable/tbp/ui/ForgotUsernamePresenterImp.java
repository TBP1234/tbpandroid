package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Repository.ForgotUsernamerepository;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 6/10/17.
 */

public class ForgotUsernamePresenterImp implements ForgotUsernamerepository.ForgotUsernamePresenter {


    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    ForgotUsernamerepository.ForgotUsernameView forgotUsernameView ;

    private NetworkHelper networkHelper;
    private DataManager dataManager;

    @Inject
    public ForgotUsernamePresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson,ForgotUsernamerepository.ForgotUsernameView forgotUsernameView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.forgotUsernameView = forgotUsernameView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager=new DataManager(sharedPreferenceHelper);

    }


    @Override
    public void doforgotUserName(String email) {


        forgotUsernameView.showProgressbar();

        Data data = new Data();
        data.setEmail(email);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setData(data);
        registrationPojo.setFuncName(Constants.FUN_FORGOT_USER_NANME);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);


        networkHelper.getForgotpasswordDetail(json)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        Msg.l("error", e.getMessage());

                        forgotUsernameView.showError(e.getMessage());
                        forgotUsernameView.hideProgressbar();

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        Msg.l("data", pojo.getMessage());
                        forgotUsernameView.hideProgressbar();

                        if (pojo != null) {

                            if (pojo.getMsgCode() == 0) {

                                forgotUsernameView.showError(pojo.getMessage());

                            } else {

                                forgotUsernameView.clearEditText();
                                forgotUsernameView.moveToSignInView();

                            }

                        }
                    }
                });



    }

    @Override
    public void setType(String type) {

    }

    @Override
    public void geyType() {

    }
}
