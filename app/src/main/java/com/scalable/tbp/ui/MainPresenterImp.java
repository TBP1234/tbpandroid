package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Repository.Mainrepository;
import com.scalable.tbp.Pojo.companyList.CompanyList;
import com.scalable.tbp.Pojo.influecncerList.InfluecncerList;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 11/10/17.
 */

public class MainPresenterImp implements Mainrepository.MainPresenter {

    private final NetworkHelper networkHelper;
    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    Mainrepository.MainView mainView;

    DataManager dataManager;

    @Inject
    public MainPresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, Mainrepository.MainView mainView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.mainView = mainView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager = new DataManager(sharedPreferenceHelper);
    }



    @Override
    public void getObjectList() {

        mainView.showProgressbar();

        final String type=dataManager.getType();


        final Data data = new Data();

        if (type.equals(Constants.COMPANY) ){

            data.setType(Constants.INFLUENCER);

            getInfluencer(data);

        }else {

            data.setType(Constants.COMPANY);
            getComapany(data);

        }
    }
    private void getComapany(Data data) {

            RegistrationPojo registrationPojo = new RegistrationPojo();
            registrationPojo.setFuncName(Constants.FUN_ALL_USERLIST);
            registrationPojo.setData(data);

            String json = gson.toJson(registrationPojo, RegistrationPojo.class);
            Msg.l("upload json ", json);

            networkHelper.getCompanyList(json.toString().trim())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Observer<CompanyList>() {
                        @Override
                        public void onCompleted() {


                        }

                        @Override
                        public void onError(Throwable e) {
//
                            mainView.hideProgressbar();
                            mainView.showError(e.getMessage());

                        }

                        @Override
                        public void onNext(CompanyList companyList) {

                            mainView.hideProgressbar();

                            if (companyList != null) {


                                if (companyList.getMsgCode() == 0) {

                                    mainView.showError(companyList.getMessage());



                                } else if (companyList.getMsgCode() == 1) {


                                    List<Object> objectList= new ArrayList();

                                    if (companyList.getData().size()>0){
                                        objectList.addAll(companyList.getData());
                                        mainView.showLIst(objectList,1);
                                    }


                                }
                            }
                        }
                    });



    }

    private void getInfluencer(Data data) {

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_ALL_USERLIST);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.getInfluencerList(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<InfluecncerList>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {
//
                        mainView.hideProgressbar();
                        mainView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(InfluecncerList InfluecncerList) {

                        mainView.hideProgressbar();

                        if (InfluecncerList != null) {


                            if (InfluecncerList.getMsgCode() == 0) {

                                mainView.showError(InfluecncerList.getMessage());

                            } else if (InfluecncerList.getMsgCode() == 1) {

                                List<Object> objectList= new ArrayList();

                                if (InfluecncerList.getData().size()>0){
                                    objectList.addAll(InfluecncerList.getData());
                                    mainView.showLIst(objectList,2);

                                }

                            }
                        }
                    }
                });
    }

    @Override
    public void doProfile() {

        String type=dataManager.getType();

        if (type.equals(Constants.COMPANY)){

            mainView.moveToCompanyProfileView();


        }else {

            mainView.moveToInfluencerProfileView();

        }




    }

    @Override
    public void dosetColor() {

        String type=dataManager.getType();

        if (type.equals(Constants.COMPANY)){

            mainView.showComapanyColor();

        }else {

            mainView.showInfluencerColor();

        }

    }

    @Override
    public void dochatButtonClick() {
        Constants.COUNT=0;
        mainView.moveToChatView();
    }

    @Override
    public void doFilterButtonClick() {

        String type=dataManager.getType();

        if (type.equals(Constants.COMPANY)){

            mainView.moveToInfluencerFilterView();

        }else {

            mainView.moveToCompanyFilterView();

        }

    }
}
