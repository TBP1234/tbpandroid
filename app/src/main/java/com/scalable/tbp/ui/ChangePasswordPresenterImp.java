package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Repository.ChangePasswordrepository;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by shailendra on 10/10/17.
 */

public class ChangePasswordPresenterImp implements ChangePasswordrepository.ChangePasswordPresenter {

    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    ChangePasswordrepository.ChangePasswordView changePasswordView;

    private NetworkHelper networkHelper;
    private DataManager dataManager;

    @Inject
    public ChangePasswordPresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, ChangePasswordrepository.ChangePasswordView signUpView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.changePasswordView = signUpView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager=new DataManager(sharedPreferenceHelper);



    }


    @Override
    public void doChangePasswordName(String password) {

        int uid=dataManager.getUserId();

        changePasswordView.showProgressbar();

        Data data = new Data();
        data.setUserId(uid);
        data.setPassword(password);


        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setData(data);
        registrationPojo.setFuncName(Constants.FUN_CHANGE_PASSWORD);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getSignUpdata(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        changePasswordView.showError(e.getMessage());
                        changePasswordView.hideProgressbar();

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        changePasswordView.hideProgressbar();

                        if (pojo != null) {

                            if (pojo.getMsgCode() == 0) {

                                changePasswordView.showError(pojo.getMessage());

                            } else {

                                changePasswordView.clearEditText();
                                dataManager.logOut();
                                changePasswordView.moveToSignInView();

                            }

                        }
                    }
                });
    }

    @Override
    public void dosetColor() {

        String type= dataManager.getType();
        if (type.equals(Constants.COMPANY)){

            changePasswordView.setcomapanyColor();

        }else if (type.equals(Constants.INFLUENCER)){

            changePasswordView.setInfluencerColor();

        }
    }
}
