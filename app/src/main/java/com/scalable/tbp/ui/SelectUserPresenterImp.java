package com.scalable.tbp.ui;

import com.scalable.tbp.Repository.SelectUserRepository;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.DataManagerIntractor;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Inject;

/**
 * Created by shailendra on 6/10/17.
 */

public class SelectUserPresenterImp implements SelectUserRepository.SelectUserPresenter {

    SelectUserRepository.SelectUserView selectUserView;
    SharedPreferenceHelper sharedPreferenceHelper;
    DataManagerIntractor dataManager;


    @Inject
    public SelectUserPresenterImp(SelectUserRepository.SelectUserView selectUserView, SharedPreferenceHelper sharedPreferenceHelper) {
        this.selectUserView = selectUserView;
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.dataManager= new DataManager(sharedPreferenceHelper);

    }

    @Override
    public void doSignInclick(String type) {

        dataManager.saveType(type);
        selectUserView.moveToSignInView();
    }
}
