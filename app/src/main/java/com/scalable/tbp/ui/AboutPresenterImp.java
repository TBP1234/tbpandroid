package com.scalable.tbp.ui;

import com.scalable.tbp.Repository.Aboutrepository;
import com.scalable.tbp.activity.AboutActivity;

/**
 * Created by shailendra on 6/10/17.
 */

public class AboutPresenterImp implements Aboutrepository.AboutPresenter {


    AboutActivity aboutView;

    public AboutPresenterImp(Aboutrepository.AboutView aboutView) {

        this.aboutView= (AboutActivity) aboutView;

    }

    @Override
    public void onNextClicked() {

        aboutView.goToNextScreen();

    }
}
