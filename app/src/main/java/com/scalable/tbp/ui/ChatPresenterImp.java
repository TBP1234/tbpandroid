package com.scalable.tbp.ui;

import android.app.Dialog;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.scalable.tbp.Pojo.chatHistory.ChatHistory;
import com.scalable.tbp.Pojo.chatRequest.MessageRequestPojo;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.Repository.ChatRepository;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;


import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by shailendra on 11/10/17.
 */

public class ChatPresenterImp implements ChatRepository.ChatPresenter {

    private final NetworkHelper networkHelper;
    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    ChatRepository.ChatView chatView;
    DataManager dataManager;


    @Inject
    public ChatPresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, ChatRepository.ChatView chatView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.chatView = chatView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager = new DataManager(sharedPreferenceHelper);
    }


    @Override
    public void doprofleButtunClicked() {

        String type = dataManager.getType();
        if (type.equals(Constants.COMPANY)) {

            chatView.moveToCompanyProfileView();

        } else {

            chatView.moveToInfluencerProfileView();

        }
    }

    @Override
    public void doChatButtunClicked() {

        chatView.moveToChatListView();
    }

    @Override
    public void doSendButtunClicked(final String to_id, final String msg) {
        //chatView.showProgressbar();
        final int from_id = dataManager.getUserId();
        String from_name = dataManager.getLoginUsername();
        String image = dataManager.getDefaultImage();

        final Data data = new Data();

        data.setFromid("" + from_id);
        data.setToid(to_id);
        data.setFromname(from_name);
        data.setMessage(msg);
        data.setFromimage(image);


        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_SEND_MESSAGE);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.sendmessage(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {

                        // chatView.hideProgressbar();
                        chatView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        //   chatView.hideProgressbar();


                        if (pojo != null) {

                            Msg.l("result", pojo.getMessage());

                            if (pojo.getMsgCode() == 0) {
                                if(pojo.getMessage().equalsIgnoreCase("You Are Blocked")){

                                    if(String.valueOf(from_id).equals(pojo.getBlockid())){

                                        chatView.showCustomDialog(msg);
                                    }else {
                                        chatView.showError(pojo.getMessage());

                                    }

                                }


                            } else if (pojo.getMsgCode() == 1) {

                                chatView.clearEdittext();
                                fetchChatData(to_id);

                            }

                        }
                    }
                });
    }


    @Override
    public void fetchChatData(String id) {

        final int userId = dataManager.getUserId();
        Log.e("Idek", String.valueOf(userId));
        Log.e("iddo", String.valueOf(id));
        final Data data = new Data();

        data.setUserfromId(userId);
        data.setUserid1(id);
        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_CHAT_HISTORY);
        registrationPojo.setData(data);


        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.getChatHistory(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ChatHistory>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {

                        //    chatView.hideProgressbar();
                        chatView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(ChatHistory pojo) {
                        //  chatView.hideProgressbar();
                        //chatView.clearEdittext();
                        if (pojo != null) {
                            Msg.l("result", pojo.getMessage());
                            if (pojo.getMsgCode() == 0) {
                                chatView.showError(pojo.getMessage());
                                // chatView.layoutInvisible();

                            } else if (pojo.getMsgCode() == 1) {

                                chatView.returnedBlockIdFromHistory(pojo.getFromblockid());
                                // chatView.layoutVisible();
                                if(pojo.getMessage().equalsIgnoreCase("Before send request then chat")){

                                    chatView.layoutvisiblesendrequest();
                                }else if(pojo.getMessage().equalsIgnoreCase("Pendding request accept then chat")){

                                    if(String.valueOf(userId).equals(pojo.getData().get(0).getFrom_resquestid())){

                                        chatView.layoutvisibleacceptrequest();
                                    }else {

                                        chatView.layoutvisiblependingrequest();
                                    }

                                }else {

                                    chatView.layoutVisiblechat();
                                    ArrayList arrayList = new ArrayList();
                                    arrayList.addAll(pojo.getData());
                                    chatView.showChat(arrayList);
                                }

                            }
                        }
                    }
                });

    }

    @Override
    public void sendChatRequest(String id) {
        int userId = dataManager.getUserId();
        final Data data = new Data();
        data.setFromid(String.valueOf(userId));
        data.setToid(id);
        data.setFrom_resquestid(id);
        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.SEND_REQUEST);
        registrationPojo.setData(data);
        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Log.e("json", json);

        networkHelper.getChatRequest(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<MessageRequestPojo>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                        chatView.showError(throwable.getMessage());
                    }

                    @Override
                    public void onNext(MessageRequestPojo messageRequestPojo) {

                        if(messageRequestPojo!=null) {

                            if (messageRequestPojo.getMessage().equalsIgnoreCase("Your Request is successfully Send")) {

                                chatView.getPendingRequestMessage();

                                //chatView.showError(messageRequestPojo.getMessage());
                                chatView.layoutvisiblependingrequest();

                            } else {

                                chatView.showError(messageRequestPojo.getMessage());
                            }

                        }

                    }
                });
    }

    @Override
    public void setColor() {
        String type = dataManager.getType();
        if (type.equals(Constants.COMPANY)) {
            chatView.showComapanyColor();

        } else {

            chatView.showInfluencerColor();

        }

    }

    @Override
    public void setChatUser(String id) {

        dataManager.setCurrentChatUser(id);

    }

    @Override
    public void acceptChatRequest(String id) {
        int userId = dataManager.getUserId();
        Data data = new Data();
        data.setUserid_1(String.valueOf(userId));
        data.setUserid_2(id);
        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.ACCEPT_REQUEST);
        registrationPojo.setData(data);
        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Log.e("json", json);
        networkHelper.acceptChatRequest(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<MessageRequestPojo>() {


                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        chatView.showError(throwable.getMessage());

                    }

                    @Override
                    public void onNext(MessageRequestPojo messageRequestPojo) {

                        if (messageRequestPojo != null) {

                            if(messageRequestPojo.getMessage().equalsIgnoreCase("Accept Request successfully"))

                            chatView.showError(messageRequestPojo.getMessage());
                            chatView.layoutVisiblechat();
                        }

                    }
                });


    }

    @Override
    public void ignoreChatRequest(String id) {

         int userId = dataManager.getUserId();
        Data data = new Data();
        data.setUserid_1(String.valueOf(userId));
        data.setUserid_2(id);
        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.REMOVE_REQUEST);
        registrationPojo.setData(data);
        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Log.e("json", json);
        networkHelper.rejectChatRequest(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<MessageRequestPojo>() {


                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        chatView.showError(throwable.getMessage());

                    }

                    @Override
                    public void onNext(MessageRequestPojo messageRequestPojo) {
                        if (messageRequestPojo != null) {

                            if(messageRequestPojo.getMessage().equalsIgnoreCase("Cancel request successfully")){

                                chatView.showError(messageRequestPojo.getMessage());
                                chatView.layoutvisiblesendrequest();
                            }


                        }
                    }
                });
    }

    @Override
    public void blockSelf() {
        int userId = dataManager.getUserId();
        Data data = new Data();
        data.setUserid(String.valueOf(userId));
        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.BLOCK_Self_REQUEST);
        registrationPojo.setData(data);
        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Log.e("json", json);

        networkHelper.block(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<MessageRequestPojo>() {


                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        chatView.showError(throwable.getMessage());

                    }

                    @Override
                    public void onNext(MessageRequestPojo messageRequestPojo) {
                        if (messageRequestPojo != null) {

                                chatView.showError(messageRequestPojo.getMessage());
                                logout();

                        }
                    }
                });

    }

    @Override
    public void logout() {

        chatView.showProgressbar();
        int  userId=dataManager.getUserId();
        Data data = new Data();
        data.setUserId(userId);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_LOGOUT);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.getUserDetail(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {

                        chatView.hideProgressbar();
                        chatView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        chatView.hideProgressbar();

                        if (pojo != null) {
                            Msg.l("result", pojo.getMessage());

                            dataManager.logOut();
                            chatView.moveToLoginView();

                            if (pojo.getMsgCode() == 0) {

                                //    companyProfileView.showError(pojo.getMessage());

                            } else if (pojo.getMsgCode() == 1) {

//                                dataManager.logOut();
//                                companyProfileView.moveToLoginView();

                            }
                        }
                    }
                });
    }

    @Override
    public void blockUser(final String id, final String type, final Dialog dialog, final String msg) {

        int userId = dataManager.getUserId();
        Data data = new Data();
        if(type.equals("block")){
            data.setBlock_status("1");
        }else if(type.equals("unblock")){
            data.setBlock_status("0");
        }

        data.setUserid_1(String.valueOf(userId));
        data.setUserid_2(id);
        data.setFrom_block_userid(String.valueOf(userId));
        data.setTo_block_userid(id);
        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.BLOCK_REQUEST);
        registrationPojo.setData(data);
        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Log.e("json", json);

        networkHelper.block(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<MessageRequestPojo>() {


                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        chatView.showError(throwable.getMessage());

                    }

                    @Override
                    public void onNext(MessageRequestPojo messageRequestPojo) {
                        if (messageRequestPojo != null) {

                            if(messageRequestPojo.getMsgCode()==1) {

                                if(type.equals("block")){
                                    chatView.showError("Blocked Successfully");
                                }else if(type.equals("unblock")){
                                    chatView.showError("UnBlocked Successfully");
                                    dialog.dismiss();
                                    doSendButtunClicked(id,msg);
                                }

                            }


                        }
                    }
                });


    }
}
