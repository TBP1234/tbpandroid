package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Repository.ForgotPasswordrepository;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 6/10/17.
 */

public class ForgotPasswordPresenterImp implements ForgotPasswordrepository.ForgotPasswordPresenter {


    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    ForgotPasswordrepository.ForgotPasswordView forgotPasswordView ;

    private NetworkHelper networkHelper;
    private DataManager dataManager;

    @Inject
    public ForgotPasswordPresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson,ForgotPasswordrepository.ForgotPasswordView forgotPasswordView ) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.forgotPasswordView = forgotPasswordView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager=new DataManager(sharedPreferenceHelper);

    }


    @Override
    public void doforgotPassword(final String email){

        forgotPasswordView.showProgressbar();

        Data data = new Data();
        data.setEmail(email);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setData(data);
        registrationPojo.setFuncName(Constants.FUN_FORGOT_PASSWORD);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);


        networkHelper.getForgotpasswordDetail(json)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        Msg.l("error", e.getMessage());

                        forgotPasswordView.showError(e.getMessage());
                        forgotPasswordView.hideProgressbar();

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        Msg.l("data", pojo.getMessage());
                        forgotPasswordView.hideProgressbar();

                        if (pojo != null) {

                            if (pojo.getMsgCode() == 0) {

                                forgotPasswordView.showError(pojo.getMessage());

                            } else {

                                 forgotPasswordView.clearEditText();
                                 forgotPasswordView.showError(pojo.getMessage());
                                 forgotPasswordView.moveToresetpasswordView(email);

                            }

                        }
                    }
                });

    }

    @Override
    public void setType(String type) {

    }

    @Override
    public void geyType() {


    }
}

