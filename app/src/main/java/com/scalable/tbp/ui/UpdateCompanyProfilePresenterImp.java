package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Repository.UpdateCompanyProfilerepository;
import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.county.Datum;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 6/10/17.
 */

public class UpdateCompanyProfilePresenterImp implements UpdateCompanyProfilerepository.UpdateCompanyProfilePresenter {


    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
  UpdateCompanyProfilerepository.UpdateCompanyProfileView updateCompanyProfileView;

    private NetworkHelper networkHelper;
    private DataManager dataManager;


    @Inject
    public UpdateCompanyProfilePresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, UpdateCompanyProfilerepository.UpdateCompanyProfileView updateCompanyProfileView) {

        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.updateCompanyProfileView = updateCompanyProfileView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager=new DataManager(sharedPreferenceHelper);
    }

    @Override
    public void doUpdate(String name,String lastname,String companyName,String image,String typeOfproduct,String country,String city,String about ,String branch,String countryCode,String cityCode) {

        int userid=dataManager.getUserId();
        updateCompanyProfileView.showProgressbar();


        final Data data = new Data();
        data.setUserId(userid);
        data.setName(name);
        data.setLastName(lastname);
        data.setCompanyName(companyName);
        data.setImage(image);
        data.setTypeOfProduct(typeOfproduct);
        data.setCountry(country);
        data.setCity(city);
        data.setAbout(about);
        data.setBranch(branch);
        data.setCityCode(cityCode);
        data.setCountryCode(countryCode);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setData(data);
        registrationPojo.setFuncName(Constants.FUN_EDIT_COMPANY_PROFILE);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getSignUpdata(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        updateCompanyProfileView.showError(e.getMessage());
                        updateCompanyProfileView.hideProgressbar();

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        updateCompanyProfileView.hideProgressbar();

                        if (pojo.getMsgCode()==1){

                            updateCompanyProfileView.clearEditText();
                            //dataManager.setUserId(pojo.getUserid());
                            updateCompanyProfileView.moveToDashboardView();

                        }

                    }
                });

    }

    @Override
    public void setType(String type) {

    }

    @Override
    public void geyType() {

    }

    @Override
    public void getCountry() {

       // updateCompanyProfileView.showProgressbar();
        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_GET_COUNTY);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getCountries(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<Country>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        updateCompanyProfileView.showError(e.getMessage());
                        updateCompanyProfileView.hideProgressbar();

                    }

                    @Override
                    public void onNext(Country country) {

                        updateCompanyProfileView.hideProgressbar();

                        if (country.getMessage().equals(Constants.SUCCESS)){

                            List<String> strings=new ArrayList<>();

                            for (Datum data:country.getData()) {

                                strings.add(data.getCountryName());
                            }

                            updateCompanyProfileView.setCountry(country,strings);

                        }

                    }
                });



    }

    @Override
    public void getStates(String id) {

    }

    @Override
    public void getCity(String id) {

        updateCompanyProfileView.showProgressbar();
        final Data data = new Data();
        data.setCountryId(id);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_GET_CITY);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getCities(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<City>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        updateCompanyProfileView.showError(e.getMessage());
                        updateCompanyProfileView.hideProgressbar();

                    }

                    @Override
                    public void onNext(City city) {

                        updateCompanyProfileView.hideProgressbar();

                        if (city.getMessage().equals(Constants.SUCCESS)){

                            List<String> strings=new ArrayList<>();

                            for (com.scalable.tbp.Pojo.city.Datum data:city.getData()) {

                                strings.add(data.getCityName());
                            }

                            updateCompanyProfileView.setCity(city,strings);

                        }

                    }
                });

    }
}
