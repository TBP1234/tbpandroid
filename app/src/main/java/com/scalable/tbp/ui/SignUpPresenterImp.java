package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Repository.SignUprepository;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by shailendra on 10/10/17.
 */

public class SignUpPresenterImp implements SignUprepository.SignUpPresenter {

    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    SignUprepository.SignUpView signUpView;

    private NetworkHelper networkHelper;
    private DataManager dataManager;

    @Inject
    public SignUpPresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, SignUprepository.SignUpView signUpView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.signUpView = signUpView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager=new DataManager(sharedPreferenceHelper);



    }

    @Override
    public void doSignUp(String type, String uname, String password, String email) {


        String utype=dataManager.getType();
        final String token=dataManager.getDeviceToken();
        signUpView.showProgressbar();

        Data data = new Data();
        data.setEmail(email);
        data.setPassword(password);
        data.setType(utype);
        data.setUsername(uname);
        data.setDevicetype(Constants.DEVICE_TYPE);
        data.setDevicetoken(token);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setData(data);
        registrationPojo.setFuncName("creatProfile");

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getSignUpdata(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        signUpView.showError(e.getMessage());
                        signUpView.hideProgressbar();

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        signUpView.hideProgressbar();

                        if (pojo != null) {

                            if (pojo.getMsgCode() == 0) {

                                signUpView.showError(pojo.getMessage());

                            } else {

                                //dataManager.setUserId(pojo.getUserid());
                                signUpView.clearEditText();
                                signUpView.moveToConfimView(pojo.getUserid());

                            }

                        }
                    }
                });
    }

    @Override
    public void setInfoMsg() {

        String type= dataManager.getType();
        if (type.equals(Constants.COMPANY)){
            signUpView.InfoMsg("Company Information");

        }else {

            signUpView.InfoMsg("Influencer Information");
        }

    }

    @Override
    public void setColor() {

     String type=dataManager.getType();

        if (type.equals(Constants.COMPANY)){

            signUpView.setColor(0);

        }else {

            signUpView.setColor(1);

        }


    }

    public void doUser() {
        
        networkHelper.getusers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<String>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Msg.l("error", e.getMessage());
                    }

                    @Override
                    public void onNext(String s) {
                        Msg.l("data", s);

                    }
                });

    }

}
