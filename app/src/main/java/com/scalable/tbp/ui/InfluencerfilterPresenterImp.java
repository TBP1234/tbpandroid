package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.county.Datum;
import com.scalable.tbp.Pojo.influecncerList.InfluecncerList;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.R;
import com.scalable.tbp.Repository.InluencerFilterRepository;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 11/10/17.
 */

public class InfluencerfilterPresenterImp implements InluencerFilterRepository.InfluencerFilterPresenter {

    private final NetworkHelper networkHelper;
    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    InluencerFilterRepository.InfluencerFilterView influencerFilterView;
    DataManager dataManager;

    @Inject
    public InfluencerfilterPresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, InluencerFilterRepository.InfluencerFilterView influencerFilterView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.influencerFilterView = influencerFilterView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager = new DataManager(sharedPreferenceHelper);
    }

    @Override
    public void doSearchButtonClick(String name,String country,String city,int checkedid,String insta,String face,String snap) {

         influencerFilterView.showProgressbar();

        String gendr="";
        switch (checkedid){

            case R.id.rbtn_male:
                gendr="male";
                break;

            case R.id.rbtn_female:
                gendr="female";
                break;


            case R.id.rbtn_other:
                gendr="other";
                break;

        }


        int instagram=0;
        int facebook=0;
        int snapchat=0;


        instagram=Integer.parseInt(insta);
        facebook=Integer.parseInt(face);
        snapchat=Integer.parseInt(snap);


        final Data data = new Data();
        data.setType(Constants.INFLUENCER);
        data.setCountry(country);
        data.setCity(city);
        data.setName(name);
        data.setInstagramfollowers(""+instagram*1000);
        data.setFacebookfollowers(""+facebook*1000);
        data.setSnapchatfollowers(""+snapchat*1000);
        data.setGender(""+gendr);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_FIND_COMPANY);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.findInfluecncer(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<InfluecncerList>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {

                        influencerFilterView.hideProgressbar();
                        influencerFilterView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(InfluecncerList pojo) {

                        influencerFilterView.hideProgressbar();
                        influencerFilterView.clearEditText();

                        if (pojo != null) {

                            Msg.l("result", pojo.getMessage());
                            if (pojo.getMsgCode() == 0) {
                                influencerFilterView.showError(pojo.getMessage());
                             //   influencerFilterView.moveToMainView(null);

                            } else if (pojo.getMsgCode() == 1) {

                                influencerFilterView.moveToMainView(pojo);

                            }

                        }
                    }
                });
    }

    @Override
    public void getCountry() {
        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_GET_COUNTY);
        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);
        networkHelper.getCountries(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<Country>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        influencerFilterView.showError(e.getMessage());
                        influencerFilterView.hideProgressbar();

                    }

                    @Override
                    public void onNext(Country country) {

                        influencerFilterView.hideProgressbar();

                        if (country.getMessage().equals(Constants.SUCCESS)){

                            List<String> strings=new ArrayList<>();

                            for (Datum data:country.getData()) {

                                strings.add(data.getCountryName());
                            }

                            influencerFilterView.setCountry(country,strings);

                        }

                    }
                });
    }
    @Override
    public void getCity(String id) {

        final Data data = new Data();
        data.setCountryId(id);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_GET_CITY);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getCities(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<City>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        influencerFilterView.showError(e.getMessage());
                        influencerFilterView.hideProgressbar();

                    }

                    @Override
                    public void onNext(City city) {

                        influencerFilterView.hideProgressbar();

                        if (city.getMessage().equals(Constants.SUCCESS)){

                            List<String> strings=new ArrayList<>();

                            for (com.scalable.tbp.Pojo.city.Datum data:city.getData()) {

                                strings.add(data.getCityName());
                            }

                            influencerFilterView.setCity(city,strings);
                        }
                    }
                });
    }
}
