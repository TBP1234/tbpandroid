package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.companyList.CompanyList;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.county.Datum;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Repository.CompanyFilterRepository;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 11/10/17.
 */

public class CompanyfilterPresenterImp implements CompanyFilterRepository.CompanyFilterPresenter {

    private final NetworkHelper networkHelper;
    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    CompanyFilterRepository.CompanyFilterView companyFilterView;
    DataManager dataManager;

    @Inject
    public CompanyfilterPresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, CompanyFilterRepository.CompanyFilterView companyFilterView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.companyFilterView = companyFilterView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager = new DataManager(sharedPreferenceHelper);
    }

    @Override
    public void doSearchButtonClick(String name,String country,String city,String branch) {

        int userId=dataManager.getUserId();

        companyFilterView.showProgressbar();

        final Data data = new Data();

        data.setType(Constants.COMPANY);
        data.setCountry(country);
        data.setCity(city);
        data.setName(name);
        data.setInstagramfollowers("");
        data.setFacebookfollowers("");
        data.setSnapchatfollowers("");

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_FIND_COMPANY);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.findCompany(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<CompanyList>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {

                        companyFilterView.hideProgressbar();
                        companyFilterView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(CompanyList pojo) {

                        companyFilterView.hideProgressbar();
                        companyFilterView.clearEditText();

                        if (pojo != null) {

                            Msg.l("result", pojo.getMessage());
                            if (pojo.getMsgCode() == 0) {

                                companyFilterView.showError(pojo.getMessage());
                                //companyFilterView.moveToMainView(null);

                            } else if (pojo.getMsgCode() == 1) {


                                companyFilterView.moveToMainView(pojo);

                            }

                        }
                    }
                });

    }

    @Override
    public void getCountry() {

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_GET_COUNTY);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getCountries(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<Country>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        companyFilterView.showError(e.getMessage());
                        companyFilterView.hideProgressbar();

                    }

                    @Override
                    public void onNext(Country country) {

                        companyFilterView.hideProgressbar();

                        if (country.getMessage().equals(Constants.SUCCESS)){

                            List<String> strings=new ArrayList<>();

                            for (Datum data:country.getData()) {

                                strings.add(data.getCountryName());
                            }

                            companyFilterView.setCountry(country,strings);

                        }

                    }
                });

    }


    @Override
    public void getCity(String id) {

        final Data data = new Data();
        data.setCountryId(id);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_GET_CITY);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("json ", json);

        networkHelper.getCities(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<City>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        companyFilterView.showError(e.getMessage());
                        companyFilterView.hideProgressbar();

                    }

                    @Override
                    public void onNext(City city) {

                        companyFilterView.hideProgressbar();

                        if (city.getMessage().equals(Constants.SUCCESS)){

                            List<String> strings=new ArrayList<>();

                            for (com.scalable.tbp.Pojo.city.Datum data:city.getData()) {

                                strings.add(data.getCityName());
                            }

                            companyFilterView.setCity(city,strings);

                        }

                    }
                });




    }
}
