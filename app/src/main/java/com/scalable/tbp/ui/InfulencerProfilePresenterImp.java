package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Pojo.registrationPojo.Datum;
import com.scalable.tbp.Repository.InfluencerProfileRepository;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 11/10/17.
 */

public class InfulencerProfilePresenterImp implements InfluencerProfileRepository.InfluencerProfilePresenter {

    private final NetworkHelper networkHelper;
    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;

    InfluencerProfileRepository.InfluencerProfileView influencerProfileView;

    DataManager dataManager;

    @Inject
    public InfulencerProfilePresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, InfluencerProfileRepository.InfluencerProfileView influencerProfileView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.influencerProfileView = influencerProfileView;

        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager = new DataManager(sharedPreferenceHelper);
    }

    @Override
    public void fetchaUserdetail(String uid) {

        influencerProfileView.showProgressbar();

        int userId=0;

        if (uid!=null){

            userId=Integer.parseInt(uid);



        }else {

            userId=dataManager.getUserId();
            influencerProfileView.showLogEdit();

        }

        Data data = new Data();
        data.setUserId(userId);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_USER_DETAIL);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.getUserDetail(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        influencerProfileView.hideProgressbar();
                        influencerProfileView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        influencerProfileView.hideProgressbar();

                        if (pojo != null) {
                            Msg.l("result", pojo.getMessage());
                            if (pojo.getMsgCode() == 0) {

                                influencerProfileView.showError(pojo.getMessage());

                            } else if (pojo.getMsgCode() == 1) {

                                if (pojo.getData().size()>0){
                                    influencerProfileView.showData(pojo.getData().get(0));

                                }else {

                                    influencerProfileView.showError("fetching data error...");
                                }


                            }
                        }
                    }
                });
    }

    @Override
    public void logOut() {

        influencerProfileView.showProgressbar();
        int  userId=dataManager.getUserId();
        Data data = new Data();
        data.setUserId(userId);

        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_LOGOUT);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.getUserDetail(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ResultPojo>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        influencerProfileView.hideProgressbar();
                        influencerProfileView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(ResultPojo pojo) {

                        influencerProfileView.hideProgressbar();

                        if (pojo != null) {
                            Msg.l("result", pojo.getMessage());


                                dataManager.logOut();
                                influencerProfileView.moveToLoginView();
                            if (pojo.getMsgCode() == 0) {

                               // influencerProfileView.showError(pojo.getMessage());

                            } else if (pojo.getMsgCode() == 1) {

//                                dataManager.logOut();
//                                influencerProfileView.moveToLoginView();
                            }
                        }
                    }
                });
        }

    @Override
    public void goToChangePassword() {

        influencerProfileView.moveToChangePasswordView();
    }

    @Override
    public void setColor() {

        String type=dataManager.getType();
        if (type.equals(Constants.COMPANY)){

            influencerProfileView.showComapanyColor();

        }else {

            influencerProfileView.showInfluencerColor();

        }

    }

    @Override
    public void dochatButtonClicked(String id) {

        if (id!=null && !id.isEmpty()){

            influencerProfileView.moveToChatView();

        }else {

            influencerProfileView.moveToChatListView();

        }


    }

    @Override
    public void doInstaProfileButtonClicked(Datum datum) {

        if (!datum.getInstagramprofileurl().isEmpty())
        influencerProfileView.showWebUrl(datum.getInstagramprofileurl());

    }

    @Override
    public void dofaceProfileButtonClicked(Datum datum) {
        if (!datum.getFacebookprofileurl().isEmpty())
            influencerProfileView.showWebUrl(datum.getFacebookprofileurl());

    }

    @Override
    public void dosnapProfileButtonClicked(Datum datum) {


        //influencerProfileView.showWebUrl("");

    }
}
