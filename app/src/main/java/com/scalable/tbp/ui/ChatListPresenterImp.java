package com.scalable.tbp.ui;

import com.google.gson.Gson;
import com.scalable.tbp.Pojo.chatList.ChatList;
import com.scalable.tbp.Pojo.registrationPojo.Data;
import com.scalable.tbp.Pojo.registrationPojo.RegistrationPojo;
import com.scalable.tbp.Pojo.requestList.RequestListPojo;
import com.scalable.tbp.Pojo.requestList.SEndRequestListPozo;
import com.scalable.tbp.Repository.ChatListRepository;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.Msg;
import com.scalable.tbp.utility.NetworkHelper;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shailendra on 11/10/17.
 */

public class ChatListPresenterImp implements ChatListRepository.ChatListPresenter {

    private final NetworkHelper networkHelper;
    SharedPreferenceHelper sharedPreferenceHelper;
    Retrofit retrofit;
    Gson gson;
    ChatListRepository.ChatListView chatListView;
    DataManager dataManager;
    List<Object> list;

    @Inject
    public ChatListPresenterImp(SharedPreferenceHelper sharedPreferenceHelper, Retrofit retrofit, Gson gson, ChatListRepository.ChatListView chatListView) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
        this.retrofit = retrofit;
        this.gson = gson;
        this.chatListView = chatListView;
        this.networkHelper = new NetworkHelper(retrofit);
        this.dataManager = new DataManager(sharedPreferenceHelper);
    }





    @Override
    public void doProfileButtunClicked() {

        String type=dataManager.getType();
        if (type.equals(Constants.COMPANY)){

            chatListView.moveToComapanyProfileView();

        }else {

            chatListView.moveToInfluencerProfileView();

        }
    }

    @Override
    public void fetchChatData() {

        int userid=dataManager.getUserId();

        chatListView.showProgressbar();

        final Data data = new Data();
        data.setUserfromId(userid);
        RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.FUN_CHAT_LIST);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("upload json ", json);

        networkHelper.getChatList(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ChatList>() {
                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onError(Throwable e) {

                        chatListView.hideProgressbar();
                        chatListView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(ChatList pojo) {

                        if (pojo != null) {

                            list= new ArrayList<>();

                            if (pojo.getMsgCode() == 0) {

                                //chatListView.showError(pojo.getMessage());

                            } else if (pojo.getMsgCode() == 1) {


                                list.addAll(pojo.getData());

                                //chatListView.showChat(list);

                            }

                            sendRequestList();

                        }
                    }
                });

    }

    @Override
    public void setColor() {


        String type=dataManager.getType();
        if (type.equals(Constants.COMPANY)){

            chatListView.showComapanyColor();

        }else {

            chatListView.showInfluencerColor();

        }


    }

    @Override
    public void sendRequestList() {
        int userid = dataManager.getUserId();


        final Data data = new Data();
        data.setUserid(String.valueOf(userid));
        final RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.SEND_REQUEST_LIST);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("sendRequestlist json ", json);

        networkHelper.getSendRequestList(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<SEndRequestListPozo>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        chatListView.hideProgressbar();
                        chatListView.showError(throwable.getMessage());
                    }

                    @Override
                    public void onNext(SEndRequestListPozo requestListPojo) {
                        if (registrationPojo!=null){
                            if (String.valueOf(requestListPojo.getMsgCode()).equals("0")){
                                //chatListView.showError(requestListPojo.getMessage());
                            }else if (String.valueOf(requestListPojo.getMsgCode()).equals("1")){
                                List<com.scalable.tbp.Pojo.requestList.SendList> requestList = requestListPojo.getData();
                                list.addAll(requestList);
                                //chatListView.showChat(finallist);

                            }

                            recieveRequestList();

                        }

                    }
                });
    }

    @Override
    public void recieveRequestList() {
        int userid = dataManager.getUserId();


        final Data data = new Data();
        data.setUserid(String.valueOf(userid));
        final RegistrationPojo registrationPojo = new RegistrationPojo();
        registrationPojo.setFuncName(Constants.RECIEVE_REQUEST_LIST);
        registrationPojo.setData(data);

        String json = gson.toJson(registrationPojo, RegistrationPojo.class);
        Msg.l("recieveRequestlist json ", json);

        networkHelper.getRecieveRequestList(json.toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<RequestListPojo>() {


                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        chatListView.hideProgressbar();
                        chatListView.showError(throwable.getMessage());
                    }

                    @Override
                    public void onNext(RequestListPojo requestListPojo) {
                        chatListView.hideProgressbar();
                        if (requestListPojo!=null){
                            if (String.valueOf(requestListPojo.getMsgCode()).equals("0")){

                            }else if (String.valueOf(requestListPojo.getMsgCode()).equals("1")){
                                List<com.scalable.tbp.Pojo.requestList.Datum> datum = requestListPojo.getData();
                                list.addAll(datum);

                            }

                            chatListView.showChat(list);
                        }

                    }
                });
    }
}
