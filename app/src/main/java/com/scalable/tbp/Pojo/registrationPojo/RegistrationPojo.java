
package com.scalable.tbp.Pojo.registrationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegistrationPojo implements Serializable {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("function_name")
    @Expose
    private String funcName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RegistrationPojo() {
    }

    /**
     * 
     * @param funcName
     * @param data
     */
    public RegistrationPojo(Data data, String funcName) {
        super();
        this.data = data;
        this.funcName = funcName;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

}
