
package com.scalable.tbp.Pojo.registrationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable{

    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("active_status")
    @Expose
    private String activestatus;

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("addedDate")
    @Expose
    private String addedDate;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("about")
    @Expose
    private String about;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("branch")
    @Expose
    private String branch;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("instagram_followers")
    @Expose
    private String instagramFollowers;
    @SerializedName("facebook_followers")
    @Expose
    private String facebookFollowers;
    @SerializedName("snapchat_followers")
    @Expose
    private String snapchatFollowers;

    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;


    @SerializedName("cityCode")
    @Expose
    private String cityCode;


    @SerializedName("countryCode")
    @Expose
    private String countryCode;

    @SerializedName("companyName")
    @Expose
    private String companyName;

    @SerializedName("typeOfProduct")
    @Expose
    private String typeOfProduct;


    @SerializedName("snapchat_id")
    @Expose
    private String snapchatid;

    @SerializedName("facebook_profile_url")
    @Expose
    private String facebookprofileurl;

    @SerializedName("instagram_profile_url")
    @Expose
    private String instagramprofileurl;

    @SerializedName("url")
    @Expose
    private String url;




    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }

    /**
     * 
     * @param addedDate
     * @param lastName
     * @param username
     * @param email
     * @param gender
     * @param image
     * @param firstname
     */
    public Datum(String firstname, String lastName, String image, String username, String email, String gender, String addedDate) {
        super();
        this.firstname = firstname;
        this.lastName = lastName;
        this.image = image;
        this.username = username;
        this.email = email;
        this.gender = gender;
        this.addedDate = addedDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getActivestatus() {
        return activestatus;
    }

    public void setActivestatus(String activestatus) {
        this.activestatus = activestatus;
    }

    public String getInstagramFollowers() {
        return instagramFollowers;
    }

    public void setInstagramFollowers(String instagramFollowers) {
        this.instagramFollowers = instagramFollowers;
    }

    public String getFacebookFollowers() {
        return facebookFollowers;
    }

    public void setFacebookFollowers(String facebookFollowers) {
        this.facebookFollowers = facebookFollowers;
    }

    public String getSnapchatFollowers() {
        return snapchatFollowers;
    }

    public void setSnapchatFollowers(String snapchatFollowers) {
        this.snapchatFollowers = snapchatFollowers;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }


    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTypeOfProduct() {
        return typeOfProduct;
    }

    public void setTypeOfProduct(String typeOfProduct) {
        this.typeOfProduct = typeOfProduct;
    }

    public String getSnapchatid() {
        return snapchatid;
    }

    public void setSnapchatid(String snapchatid) {
        this.snapchatid = snapchatid;
    }

    public String getFacebookprofileurl() {
        return facebookprofileurl;
    }

    public void setFacebookprofileurl(String facebookprofileurl) {
        this.facebookprofileurl = facebookprofileurl;
    }

    public String getInstagramprofileurl() {
        return instagramprofileurl;
    }

    public void setInstagramprofileurl(String instagramprofileurl) {
        this.instagramprofileurl = instagramprofileurl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
