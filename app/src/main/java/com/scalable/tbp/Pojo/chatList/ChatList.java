
package com.scalable.tbp.Pojo.chatList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChatList {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("msg_code")
    @Expose
    private int msgCode;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ChatList() {
    }

    /**
     * 
     * @param message
     * @param msgCode
     * @param data
     */
    public ChatList(List<Datum> data, String message, int msgCode) {
        super();
        this.data = data;
        this.message = message;
        this.msgCode = msgCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }

}
