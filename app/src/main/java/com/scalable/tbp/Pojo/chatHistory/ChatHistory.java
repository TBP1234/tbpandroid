
package com.scalable.tbp.Pojo.chatHistory;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatHistory {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("msg_code")
    @Expose
    private int msgCode;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("toblockid")
    @Expose
    private String toblockid;
    @SerializedName("fromblockid")
    @Expose
    private String fromblockid;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ChatHistory() {
    }

    /**
     * 
     * @param message
     * @param msgCode
     * @param data
     */
    public ChatHistory(String message, int msgCode, List<Datum> data) {
        super();
        this.message = message;
        this.msgCode = msgCode;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getToblockid() {
        return toblockid;
    }

    public void setToblockid(String toblockid) {
        this.toblockid = toblockid;
    }

    public String getFromblockid() {
        return fromblockid;
    }

    public void setFromblockid(String fromblockid) {
        this.fromblockid = fromblockid;
    }
}
