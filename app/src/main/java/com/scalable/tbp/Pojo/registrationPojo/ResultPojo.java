
package com.scalable.tbp.Pojo.registrationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ResultPojo implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;


    @SerializedName("msg_code")
    @Expose
    private int msgCode;

    @SerializedName("user_id")
    @Expose
    private int userid;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("blockid")
    @Expose
    private String blockid;

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;




    /**
     * No args constructor for use in serialization
     */
    public ResultPojo() {
    }

    /**
     * @param message
     * @param msgCode
     */
    public ResultPojo(String message, int msgCode) {
        super();
        this.message = message;
        this.msgCode = msgCode;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(int msgCode) {
        this.msgCode = msgCode;
    }


    public int getUserid() {
        return userid;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getBlockid() {
        return blockid;
    }

    public void setBlockid(String blockid) {
        this.blockid = blockid;
    }
}
