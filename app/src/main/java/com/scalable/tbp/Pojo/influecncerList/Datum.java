
package com.scalable.tbp.Pojo.influecncerList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum  implements Serializable{

    @SerializedName("userId")
    @Expose
    private String userId;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("firstname")
    @Expose
    private String firstname;

    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("companyName")
    @Expose
    private String companyName;

    @SerializedName("typeOfProduct")
    @Expose
    private String typeOfProduct;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("about")
    @Expose
    private String about;

    @SerializedName("branch")
    @Expose
    private String branch;

    @SerializedName("facebook_id")
    @Expose
    private String facebookId;

    @SerializedName("instagram_id")
    @Expose
    private String instagramId;

    @SerializedName("spanchat_id")
    @Expose
    private String spanchatId;

    @SerializedName("instagram_followers")
    @Expose
    private String instagramFollowers;

    @SerializedName("facebook_followers")
    @Expose
    private String facebookFollowers;

    @SerializedName("snapchat_followers")
    @Expose
    private String snapchatFollowers;

    @SerializedName("addedDate")
    @Expose
    private String addedDate;

    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;


    @SerializedName("image")
    @Expose
    private String image;


    @SerializedName("facebook_profile_url")
    @Expose
    private String facebookprofileurl;

    @SerializedName("instagram_profile_url")
    @Expose
    private String instagramprofileurl;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }

    /**
     * 
     * @param addedDate
     * @param lastName
     * @param updatedDate
     * @param facebookId
     * @param about
     * @param instagramId
     * @param firstname
     * @param typeOfProduct
     * @param type
     * @param companyName
     * @param snapchatFollowers
     * @param country
     * @param city
     * @param facebookFollowers
     * @param instagramFollowers
     * @param spanchatId
     * @param username
     * @param email
     * @param userId
     * @param branch
     * @param gender
     */
    public Datum(String userId, String type, String firstname, String lastName, String username, String gender, String email, String companyName, String typeOfProduct, String country, String city, String about, String branch, String facebookId, String instagramId, String spanchatId, String instagramFollowers, String facebookFollowers, String snapchatFollowers, String addedDate, String updatedDate) {
        super();
        this.userId = userId;
        this.type = type;
        this.firstname = firstname;
        this.lastName = lastName;
        this.username = username;
        this.gender = gender;
        this.email = email;
        this.companyName = companyName;
        this.typeOfProduct = typeOfProduct;
        this.country = country;
        this.city = city;
        this.about = about;
        this.branch = branch;
        this.facebookId = facebookId;
        this.instagramId = instagramId;
        this.spanchatId = spanchatId;
        this.instagramFollowers = instagramFollowers;
        this.facebookFollowers = facebookFollowers;
        this.snapchatFollowers = snapchatFollowers;
        this.addedDate = addedDate;
        this.updatedDate = updatedDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTypeOfProduct() {
        return typeOfProduct;
    }

    public void setTypeOfProduct(String typeOfProduct) {
        this.typeOfProduct = typeOfProduct;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getInstagramId() {
        return instagramId;
    }

    public void setInstagramId(String instagramId) {
        this.instagramId = instagramId;
    }

    public String getSpanchatId() {
        return spanchatId;
    }

    public void setSpanchatId(String spanchatId) {
        this.spanchatId = spanchatId;
    }

    public String getInstagramFollowers() {
        return instagramFollowers;
    }

    public void setInstagramFollowers(String instagramFollowers) {
        this.instagramFollowers = instagramFollowers;
    }

    public String getFacebookFollowers() {
        return facebookFollowers;
    }

    public void setFacebookFollowers(String facebookFollowers) {
        this.facebookFollowers = facebookFollowers;
    }

    public String getSnapchatFollowers() {
        return snapchatFollowers;
    }

    public void setSnapchatFollowers(String snapchatFollowers) {
        this.snapchatFollowers = snapchatFollowers;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFacebookprofileurl() {
        return facebookprofileurl;
    }

    public void setFacebookprofileurl(String facebookprofileurl) {
        this.facebookprofileurl = facebookprofileurl;
    }

    public String getInstagramprofileurl() {
        return instagramprofileurl;
    }

    public void setInstagramprofileurl(String instagramprofileurl) {
        this.instagramprofileurl = instagramprofileurl;
    }
}
