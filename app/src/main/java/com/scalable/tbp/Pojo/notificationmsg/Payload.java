
package com.scalable.tbp.Pojo.notificationmsg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Payload implements Serializable {

    @SerializedName("from_type")
    @Expose
    private Object fromType;
    @SerializedName("from_id")
    @Expose
    private String fromId;
    @SerializedName("to_id")
    @Expose
    private String toId;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("from_image")
    @Expose
    private String fromImage;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("from_name")
    @Expose
    private String fromName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Payload() {
    }

    /**
     * 
     * @param message
     * @param id
     * @param fromId
     * @param title
     * @param toId
     * @param fromName
     * @param fromImage
     * @param type
     * @param fromType
     */
    public Payload(Object fromType, String fromId, String toId, int id, String fromImage, String message, String title, String type, String fromName) {
        super();
        this.fromType = fromType;
        this.fromId = fromId;
        this.toId = toId;
        this.id = id;
        this.fromImage = fromImage;
        this.message = message;
        this.title = title;
        this.type = type;
        this.fromName = fromName;
    }

    public Object getFromType() {
        return fromType;
    }

    public void setFromType(Object fromType) {
        this.fromType = fromType;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFromImage() {
        return fromImage;
    }

    public void setFromImage(String fromImage) {
        this.fromImage = fromImage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

}
