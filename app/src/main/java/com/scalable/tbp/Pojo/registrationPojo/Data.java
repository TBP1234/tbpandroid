
package com.scalable.tbp.Pojo.registrationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Data implements Serializable {

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("userId")
    @Expose
    private int userId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("companyName")
    @Expose
    private String companyName;

    @SerializedName("image")
    @Expose
    private String Image;


    @SerializedName("typeOfProduct")
    @Expose
    private String typeOfProduct;

    @SerializedName("country")
    @Expose
    private String Country;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("instagram_followers")
    @Expose
    private String instagramfollowers;

    @SerializedName("facebook_followers")
    @Expose
    private String facebookfollowers;


    @SerializedName("snapchat_followers")
    @Expose
    private String snapchatfollowers;


    @SerializedName("instagram")
    @Expose
    private String instagram;

    @SerializedName("facebook")
    @Expose
    private String facebook;


    @SerializedName("snapchat")
    @Expose
    private String snapchat;


    @SerializedName("about")
    @Expose
    private String about;

    @SerializedName("branch")
    @Expose
    private String branch;

    @SerializedName("countryId")
    @Expose
    private String countryId;


    @SerializedName("cityCode")
    @Expose
    private String cityCode;


    @SerializedName("countryCode")
    @Expose
    private String countryCode;

    @SerializedName("snapchat_id")
    @Expose
    private String snapchatid;


    @SerializedName("from_id")
    @Expose
    private String fromid;

    @SerializedName("from_name")
    @Expose
    private String fromname;


    @SerializedName("to_id")
    @Expose
    private String toid;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("device_type")
    @Expose
    private String devicetype;

    @SerializedName("device_token")
    @Expose
    private String devicetoken;


    @SerializedName("user_id1")
    @Expose
    private String userid1;

    @SerializedName("user_id")
    @Expose
    private int userfromId;

    @SerializedName("from_resquestid")
    @Expose
    private String from_resquestid;

    @SerializedName("request_status")
    @Expose
    private String request_status;

    @SerializedName("facebook_profile_url")
    @Expose
    private String facebookprofileurl;



    @SerializedName("instagram_profile_url")
    @Expose
    private String instagramprofileurl;

    @SerializedName("from_image")
    @Expose
    private String fromimage;

    @SerializedName("userid_1")
    @Expose
    private String userid_1;

    @SerializedName("userid_2")
    @Expose
    private String userid_2;

    @SerializedName("userid")
    @Expose
    private String userid;


    @SerializedName("otp")
    @Expose
    private String otp;

    @SerializedName("block_status")
    @Expose
    private String block_status;

    @SerializedName("from_block_userid")
    @Expose
    private String from_block_userid;

    @SerializedName("to_block_userid")
    @Expose
    private String to_block_userid;
    /**
     * No args constructor for use in serialization
     */

    public Data() {

    }

    /**
     * @param username
     * @param email
     * @param type
     * @param password
     */
    public Data(String type, String username, String email, String password) {
        super();
        this.type = type;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getInstagramfollowers() {
        return instagramfollowers;
    }

    public void setInstagramfollowers(String instagramfollowers) {
        this.instagramfollowers = instagramfollowers;
    }

    public String getFacebookfollowers() {
        return facebookfollowers;
    }

    public void setFacebookfollowers(String facebookfollowers) {
        this.facebookfollowers = facebookfollowers;
    }

    public String getSnapchatfollowers() {
        return snapchatfollowers;
    }

    public void setSnapchatfollowers(String snapchatfollowers) {
        this.snapchatfollowers = snapchatfollowers;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getSnapchat() {
        return snapchat;
    }

    public void setSnapchat(String snapchat) {
        this.snapchat = snapchat;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getTypeOfProduct() {
        return typeOfProduct;
    }

    public void setTypeOfProduct(String typeOfProduct) {
        this.typeOfProduct = typeOfProduct;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSnapchatid() {
        return snapchatid;
    }

    public void setSnapchatid(String snapchatid) {
        this.snapchatid = snapchatid;
    }


    public String getFromid() {
        return fromid;
    }

    public void setFromid(String fromid) {
        this.fromid = fromid;
    }

    public String getFromname() {
        return fromname;
    }

    public void setFromname(String fromname) {
        this.fromname = fromname;
    }

    public String getToid() {
        return toid;
    }

    public void setToid(String toid) {
        this.toid = toid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }

    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }

    public String getUserid1() {
        return userid1;
    }

    public void setUserid1(String userid1) {
        this.userid1 = userid1;
    }

    public int getUserfromId() {
        return userfromId;
    }

    public void setUserfromId(int userfromId) {
        this.userfromId = userfromId;
    }

    public String getFacebookprofileurl() {
        return facebookprofileurl;
    }

    public void setFacebookprofileurl(String facebookprofileurl) {
        this.facebookprofileurl = facebookprofileurl;
    }

    public String getInstagramprofileurl() {
        return instagramprofileurl;
    }

    public void setInstagramprofileurl(String instagramprofileurl) {
        this.instagramprofileurl = instagramprofileurl;
    }

    public String getFromimage() {
        return fromimage;
    }

    public void setFromimage(String fromimage) {
        this.fromimage = fromimage;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }



    public String getFrom_resquestid() {
        return from_resquestid;
    }

    public void setFrom_resquestid(String from_resquestid) {
        this.from_resquestid = from_resquestid;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public String getUserid_1() {
        return userid_1;
    }

    public void setUserid_1(String userid_1) {
        this.userid_1 = userid_1;
    }

    public String getUserid_2() {
        return userid_2;
    }

    public void setUserid_2(String userid_2) {
        this.userid_2 = userid_2;
    }


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBlock_status() {
        return block_status;
    }

    public void setBlock_status(String block_status) {
        this.block_status = block_status;
    }

    public String getFrom_block_userid() {
        return from_block_userid;
    }

    public void setFrom_block_userid(String from_block_userid) {
        this.from_block_userid = from_block_userid;
    }

    public String getTo_block_userid() {
        return to_block_userid;
    }

    public void setTo_block_userid(String to_block_userid) {
        this.to_block_userid = to_block_userid;
    }
}


