
package com.scalable.tbp.Pojo.chatHistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("message_id")
    @Expose
    private String messageId;
    @SerializedName("from_id")
    @Expose
    private String fromId;
    @SerializedName("to_id")
    @Expose
    private String toId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("added_date")
    @Expose
    private String addedDate;
    @SerializedName("user_id")
    @Expose
    private String user_id;
    @SerializedName("user_id1")
    @Expose
    private String user_id1;
    @SerializedName("from_resquestid")
    @Expose
    private String from_resquestid;
    @SerializedName("request_acceptstatus")
    @Expose
    private String request_acceptstatus;



    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }

    /**
     * 
     * @param message
     * @param addedDate
     * @param fromId
     * @param username
     * @param toId
     * @param image
     * @param messageId
     */
    public Datum(String messageId, String fromId, String toId, String username, String image, String message, String addedDate) {
        super();
        this.messageId = messageId;
        this.fromId = fromId;
        this.toId = toId;
        this.username = username;
        this.image = image;
        this.message = message;
        this.addedDate = addedDate;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id1() {
        return user_id1;
    }

    public void setUser_id1(String user_id1) {
        this.user_id1 = user_id1;
    }

    public String getFrom_resquestid() {
        return from_resquestid;
    }

    public void setFrom_resquestid(String from_resquestid) {
        this.from_resquestid = from_resquestid;
    }

    public String getRequest_acceptstatus() {
        return request_acceptstatus;
    }

    public void setRequest_acceptstatus(String request_acceptstatus) {
        this.request_acceptstatus = request_acceptstatus;
    }
}
