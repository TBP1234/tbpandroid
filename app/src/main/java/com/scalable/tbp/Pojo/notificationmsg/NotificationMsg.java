
package com.scalable.tbp.Pojo.notificationmsg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationMsg  implements Serializable{

    @SerializedName("is_background")
    @Expose
    private boolean isBackground;
    @SerializedName("payload")
    @Expose
    private Payload payload;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    /**
     * No args constructor for use in serialization
     * 
     */
    public NotificationMsg() {
    }

    /**
     * 
     * @param timestamp
     * @param isBackground
     * @param payload
     */
    public NotificationMsg(boolean isBackground, Payload payload, String timestamp) {
        super();
        this.isBackground = isBackground;
        this.payload = payload;
        this.timestamp = timestamp;
    }

    public boolean isIsBackground() {
        return isBackground;
    }

    public void setIsBackground(boolean isBackground) {
        this.isBackground = isBackground;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

}
