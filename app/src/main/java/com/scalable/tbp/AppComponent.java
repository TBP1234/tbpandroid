package com.scalable.tbp;


import com.google.gson.Gson;
import com.scalable.tbp.dagger.AppModule;
import com.scalable.tbp.dagger.NetworkModule;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.DataManagerIntractor;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by shailendra on 27/7/17.
 */


@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {
    // downstream components need these exposed with the return type
    // method name does not really matter
    Retrofit retrofit();

    SharedPreferenceHelper sharedPreferences();

    Connectivity getConnetivity();

    Gson getGson();

    DataManagerIntractor getdatamanager();

}

