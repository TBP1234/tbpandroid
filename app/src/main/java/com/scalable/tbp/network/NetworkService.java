package com.scalable.tbp.network;

import com.scalable.tbp.Pojo.chatHistory.ChatHistory;
import com.scalable.tbp.Pojo.chatList.ChatList;
import com.scalable.tbp.Pojo.chatRequest.MessageRequestPojo;
import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.companyList.CompanyList;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.influecncerList.InfluecncerList;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.Pojo.requestList.RequestListPojo;
import com.scalable.tbp.Pojo.requestList.SEndRequestListPozo;
import com.scalable.tbp.Pojo.state.State;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by shailendra on 7/10/17.
 */

public interface NetworkService {

    //SingUp
    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo> getSingUpDetail(@Body String body);
    @POST("/tbp/webservices/service.php/")
    Observable<MessageRequestPojo> getMassageRequest(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<MessageRequestPojo> blockuser(@Body String body);


    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo> getSignInDetail(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo> getCreatComapanyProfileDetail(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo> getInfulencerProfileDetail(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo> getForgotpasswordDetail(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo> getForgotusernameDetail(@Body String body);


    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo> getUserDetail(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<RequestListPojo>getRecieveRequestList(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<CompanyList> getCompanyList(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<InfluecncerList> getInfluencerList(@Body String body);


    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo>logout(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<RequestListPojo>getRequestList(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<SEndRequestListPozo>getSendRequestList(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<Country>getCountries(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<State>getStates(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<City>getCities(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo>updateCompanyProfile(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo>updateInfluencerProfile(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<ResultPojo>sendMessage(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<ChatList>getChatList(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<ChatHistory>getchatHistory(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<InfluecncerList>findInfluecncer(@Body String body);

    @POST("/tbp/webservices/service.php/")
    Observable<CompanyList>findCompany(@Body String body);

    @GET("/users")
    Observable<String> getusers();

}
