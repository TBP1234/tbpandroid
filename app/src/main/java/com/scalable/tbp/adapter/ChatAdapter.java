package com.scalable.tbp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.chatHistory.Datum;
import com.scalable.tbp.R;
import com.scalable.tbp.utility.CircleTransform;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.SharedPreferenceHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import me.himanshusoni.chatmessageview.ChatMessageView;

/**
 * Created by shailendra on 31/10/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    Context context;
    List<Object> objectList;


    SharedPreferenceHelper sharedPreferenceHelper;
    DataManager dataManager;
    public static int LEFT_LAYOUT = 1;
    public static int RIGHT_LAYOUT = 2;



    public ChatAdapter(Context context, List<Object> objectList) {
        this.context = context;
        this.objectList = objectList;
        this.sharedPreferenceHelper = new SharedPreferenceHelper(MyApplication.get(context));
        this.dataManager = new DataManager(sharedPreferenceHelper);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == ChatAdapter.LEFT_LAYOUT) {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chatting_right, parent, false);

            //   itemView.setOnClickListener(this);

            return new MyViewHolder(itemView);

        } else {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chatting_left, parent, false);


            return new MyViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        if (objectList.get(position) instanceof Datum) {


            Datum datum= (Datum) objectList.get(position);

            if (getItemViewType(position) == LEFT_LAYOUT) {

                holder.chattimeTxt.setText(datum.getAddedDate());
                holder.chattextRight.setText(datum.getMessage());

            }
            if (getItemViewType(position) == RIGHT_LAYOUT) {

                holder.chattimeTxt.setText(datum.getAddedDate());
                holder.chattextRight.setText(datum.getMessage());



            }

            String type=dataManager.getType();
            if(datum.getImage()!=null) {
                if (type.equals(Constants.COMPANY)) {

                    Picasso.with(context).load(datum.getImage().trim()).transform(new CircleTransform()).placeholder(context.getResources().getDrawable(R.drawable.default_profile_a)).into(holder.chatingImageImg);

                } else {

                    Picasso.with(context).load(datum.getImage().trim()).transform(new CircleTransform()).placeholder(context.getResources().getDrawable(R.drawable.default_profile_b)).into(holder.chatingImageImg);

                }
            }

        }


    }


    @Override
    public int getItemViewType(int position) {

        int type = 0;

        Datum datum = (Datum) objectList.get(position);

        int userID = dataManager.getUserId();

        if (datum.getFromId().equals("" + userID)) {

            type = ChatAdapter.RIGHT_LAYOUT;

        } else {

            type = ChatAdapter.LEFT_LAYOUT;
        }

        return type;

    }

    @Override
    public int getItemCount() {

        return objectList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView chattimeTxt;
        EmojiconTextView chattextRight;
        ImageView chatingImageImg;
        ChatMessageView chatMessageView;

        public MyViewHolder(View itemView) {
            super(itemView);

            chattimeTxt= (TextView) itemView.findViewById(R.id.chattime_txt);
            chattextRight= (EmojiconTextView) itemView.findViewById(R.id.chattext_right);
            chatingImageImg= (ImageView) itemView.findViewById(R.id.chating_image_img);
            chatMessageView= (ChatMessageView) itemView.findViewById(R.id.chatMessageView);
            chatMessageView.setDrawingCacheEnabled(true);

        }
    }


}
