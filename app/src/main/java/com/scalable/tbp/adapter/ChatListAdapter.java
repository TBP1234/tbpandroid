package com.scalable.tbp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scalable.tbp.MyApplication;
import com.scalable.tbp.Pojo.chatList.Datum;
import com.scalable.tbp.R;
import com.scalable.tbp.utility.CircleTransform;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.SharedPreferenceHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by shailendra on 31/10/17.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {

    Context context;
    List<Object> objectList;

    SharedPreferenceHelper sharedPreferenceHelper;
    DataManager dataManager;
    public static int LEFT_LAYOUT = 1;
    public static int RIGHT_LAYOUT = 2;


    public ChatListAdapter(Context context, List<Object> objectList) {
        this.context = context;
        this.objectList = objectList;
        this.sharedPreferenceHelper=new SharedPreferenceHelper(MyApplication.get(context));
        this.dataManager=new DataManager(sharedPreferenceHelper);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_list, parent, false);

        //   itemView.setOnClickListener(this);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (objectList.get(position) instanceof Datum) {

            Datum datum = (Datum) objectList.get(position);

            holder.chattimeTxt.setText(datum.getAddedDate());
            holder.chattextright.setText(datum.getMessage());
            holder.chatname.setText(datum.getFname()+" "+datum.getLname());

            String type=dataManager.getType();
            if (type.equals(Constants.COMPANY)){

                if (datum.getImage()!=null && !datum.getImage().trim().equals("")){

                    Picasso.with(context).load(datum.getImage().trim()).transform(new CircleTransform()).placeholder(context.getResources().getDrawable(R.drawable.default_profile_a)).into(holder.chatingImageImg);


                }else {

                    holder.chatingImageImg.setBackgroundResource(R.drawable.default_profile_a);
                }


            }else {

                if (datum.getImage()!=null && !datum.getImage().trim().equals("")) {
                    Picasso.with(context).load(datum.getImage().trim()).transform(new CircleTransform()).placeholder(context.getResources().getDrawable(R.drawable.default_profile_b)).into(holder.chatingImageImg);

                }else {


                    holder.chatingImageImg.setBackgroundResource(R.drawable.default_profile_b);
                }

            }





        }else if (objectList!=null&&objectList.get(position) instanceof com.scalable.tbp.Pojo.requestList.Datum) {

            com.scalable.tbp.Pojo.requestList.Datum datum = (com.scalable.tbp.Pojo.requestList.Datum) objectList.get(position);
            holder.chatname.setText(datum.getFname() + " " + datum.getLname());
            holder.chattimeTxt.setText(datum.getTime());
            holder.chattextright.setText("You have chat request");
            String type = dataManager.getType();
            if (type.equals(Constants.COMPANY)) {

                if (datum.getImage() != null && !datum.getImage().trim().equals("")) {

                    Picasso.with(context).load(datum.getImage().trim()).transform(new CircleTransform()).placeholder(context.getResources().getDrawable(R.drawable.default_profile_a)).into(holder.chatingImageImg);
                } else {
                    holder.chatingImageImg.setBackgroundResource(R.drawable.default_profile_a);

                }

            } else {
                if (datum.getImage() != null && !datum.getImage().trim().equals("")) {
                    Picasso.with(context).load(datum.getImage().trim()).transform(new CircleTransform()).placeholder(context.getResources().getDrawable(R.drawable.default_profile_b)).into(holder.chatingImageImg);

                } else {

                    holder.chatingImageImg.setBackgroundResource(R.drawable.default_profile_b);
                }
            }
        }else if(objectList!=null&&objectList.get(position) instanceof com.scalable.tbp.Pojo.requestList.SendList){

            com.scalable.tbp.Pojo.requestList.SendList datum = (com.scalable.tbp.Pojo.requestList.SendList) objectList.get(position);
            holder.chatname.setText(datum.getFname() + " " + datum.getLname());
            holder.chattimeTxt.setText(datum.getTime());
            holder.chattextright.setText("Sent request");
            String type = dataManager.getType();
            if (type.equals(Constants.COMPANY)) {

                if (datum.getImage() != null && !datum.getImage().trim().equals("")) {

                    Picasso.with(context).load(datum.getImage().trim()).transform(new CircleTransform()).placeholder(context.getResources().getDrawable(R.drawable.default_profile_a)).into(holder.chatingImageImg);
                } else {
                    holder.chatingImageImg.setBackgroundResource(R.drawable.default_profile_a);

                }

            } else {
                if (datum.getImage() != null && !datum.getImage().trim().equals("")) {
                    Picasso.with(context).load(datum.getImage().trim()).transform(new CircleTransform()).placeholder(context.getResources().getDrawable(R.drawable.default_profile_b)).into(holder.chatingImageImg);

                } else {

                    holder.chatingImageImg.setBackgroundResource(R.drawable.default_profile_b);
                }
            }
        }

    }

    @Override
    public int getItemCount() {

        return objectList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView chattimeTxt;
        TextView chatname;
        TextView chattextright;
        ImageView chatingImageImg;




        public MyViewHolder(View itemView) {
            super(itemView);


            chattimeTxt= (TextView) itemView.findViewById(R.id.chattime_txt);
            chattextright= (TextView) itemView.findViewById(R.id.chattext_right);
            chatingImageImg= (ImageView) itemView.findViewById(R.id.chating_image_img);
            chatname= (TextView) itemView.findViewById(R.id.txt_name);

        }
    }

}
