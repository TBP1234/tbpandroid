package com.scalable.tbp.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scalable.tbp.Pojo.companyList.Datum;
import com.scalable.tbp.R;
import com.scalable.tbp.activity.CompanyProfileActvity;
import com.scalable.tbp.activity.InfluencerProfileActivity;
import com.scalable.tbp.utility.CircleTransform;
import com.scalable.tbp.utility.HelperMethods;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by shailendra on 23/10/17.
 */

public class MainScreenAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private Context context;
    private List<Object> list;
    private int type;

    public MainScreenAdapter(Context context, List<Object> list, int type) {
        this.context = context;
        this.list = list;
        this.type = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (type == 1) {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.companylist_row, parent, false);

         //   itemView.setOnClickListener(this);

            return new MyViewHolder(itemView);

        } else {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.influencerlist_row, parent, false);


            return new MyViewHolderinfluencer(itemView);
        }

    }


    @Override
    public int getItemViewType(int position) {


        if (type == 1) {

            return 1;

        }
        if (type == 2) {

            return 2;

        }

        return 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Object o = list.get(position);


        if (type == 1) {

            MyViewHolder myViewHolder = (MyViewHolder) holder;

            if (o instanceof Datum) ;
            {
                final Datum datum = (Datum) o;
                myViewHolder.header.setText(datum.getCompanyName());
                myViewHolder.header1.setText(datum.getCity() + " ," + datum.getCountry());
                myViewHolder.header2.setText("Branch :" + datum.getBranch());

                myViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent= new Intent(context, CompanyProfileActvity.class);
                        intent.putExtra("uid",datum.getUserId());
                        context.startActivity(intent);
                    }
                });
            }
        }


        if (type == 2) {

            final MyViewHolderinfluencer myViewHolder = (MyViewHolderinfluencer) holder;

            if (o instanceof com.scalable.tbp.Pojo.influecncerList.Datum) ;
            {
                final com.scalable.tbp.Pojo.influecncerList.Datum datum = (com.scalable.tbp.Pojo.influecncerList.Datum) o;
                myViewHolder.header.setText(datum.getFirstname() + " " + datum.getLastName());
                myViewHolder.header1.setText(datum.getCity() + " ," + datum.getCountry());
               // myViewHolder.header2.setText("Branch :" + datum.getBranch());

                myViewHolder.facebook.setText("NA");
                myViewHolder.insta.setText("NA");
                myViewHolder.snap.setText("NA");

                if (datum.getFacebookFollowers()!=null && !datum.getFacebookFollowers().equals("")) {

                    myViewHolder.imgfcb.setVisibility(View.VISIBLE);
                    myViewHolder.facebook.setText(HelperMethods.getFollowers(datum.getFacebookFollowers()));
                    myViewHolder.imgfcb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (!datum.getFacebookprofileurl().isEmpty()){

                                Uri uriUrl = Uri.parse(datum.getFacebookprofileurl());
                                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                context.startActivity(launchBrowser);

                            }
                           }
                    });

                }


                if (datum.getInstagramFollowers()!=null &&!datum.getInstagramFollowers().equals("")) {

                    myViewHolder.imginsta.setVisibility(View.VISIBLE);
                    myViewHolder.insta.setText(HelperMethods.getFollowers(datum.getInstagramFollowers()));
                    myViewHolder.imginsta.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (!datum.getInstagramprofileurl().isEmpty()){

                                Uri uriUrl = Uri.parse(datum.getInstagramprofileurl());
                                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                                context.startActivity(launchBrowser);

                            }
                        }
                    });

                }


                if (datum.getSnapchatFollowers()!=null &&!datum.getSnapchatFollowers().equals("")) {

                    myViewHolder.imgsnp.setVisibility(View.VISIBLE);
                    myViewHolder.snap.setText(HelperMethods.getFollowers(datum.getSnapchatFollowers()));
//                    myViewHolder.imgfcb.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//                            if (!datum.getFacebookprofileurl().isEmpty()){
//
//                                Uri uriUrl = Uri.parse(datum.getFacebookprofileurl());
//                                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
//                                context.startActivity(launchBrowser);
//
//                            }
//                        }
//                    });

                }

                myViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent= new Intent(context, InfluencerProfileActivity.class);
                        intent.putExtra("uid",datum.getUserId());
                        context.startActivity(intent);
                    }
                });

                if (datum.getImage()!=null && ! datum.getImage().equals("")){

                      Picasso.with(context).load(datum.getImage().trim()).transform(new CircleTransform()).placeholder(context.getResources().getDrawable(R.drawable.default_profile_a)).into(myViewHolder.imageView);

                }
            }
        }
    }

    @Override
    public int getItemCount() {

        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {


        LinearLayout linearLayout;
        TextView header;
        TextView header1;
        TextView header2;

        public MyViewHolder(View itemView) {
            super(itemView);

            header = (TextView) itemView.findViewById(R.id.txt_header);
            header1 = (TextView) itemView.findViewById(R.id.txt_header1);
            header2 = (TextView) itemView.findViewById(R.id.txt_header2);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.ln);
        }
    }

    public class MyViewHolderinfluencer extends RecyclerView.ViewHolder {


        TextView header;
        TextView header1;
        TextView header2;
        TextView snap;
        TextView insta;
        TextView facebook;
        ImageView imageView;
        ImageView imginsta;
        ImageView imgfcb;
        ImageView imgsnp;
        LinearLayout linearLayout;

        public MyViewHolderinfluencer(View itemView) {
            super(itemView);

            header = (TextView) itemView.findViewById(R.id.txt_header);
            header1 = (TextView) itemView.findViewById(R.id.txt_header1);
            header2 = (TextView) itemView.findViewById(R.id.txt_header2);
            snap = (TextView) itemView.findViewById(R.id.txt_snp);
            insta = (TextView) itemView.findViewById(R.id.txt_insta);
            facebook = (TextView) itemView.findViewById(R.id.txt_fcb);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            imgfcb = (ImageView) itemView.findViewById(R.id.img_fcb);
            imgsnp = (ImageView) itemView.findViewById(R.id.img_snp);
            imginsta = (ImageView) itemView.findViewById(R.id.img_insta);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.ln);


        }
    }

}
