package com.scalable.tbp;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.scalable.tbp.dagger.AppModule;
import com.scalable.tbp.dagger.NetworkModule;
import com.scalable.tbp.utility.Constants;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by shailendra on 9/10/17.
 */

public class MyApplication extends Application {

    private AppComponent appComponent;

    public static MyApplication get(Context context) {
       return (MyApplication) context.getApplicationContext();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }



    @Override
    public void onCreate() {
        super.onCreate();
        appComponent= DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule(Constants.BASE_URL))
                .build();



        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/futuralightbt.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );




    }

    public  AppComponent getAppComponent() {

         return appComponent;

    }
}

