package com.scalable.tbp.utility;

/**
 * Created by shailendra on 31/10/17.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.scalable.tbp.Pojo.notificationmsg.NotificationMsg;
import com.scalable.tbp.Pojo.notificationmsg.Payload;
import com.scalable.tbp.R;
import com.scalable.tbp.activity.ChatActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

     private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Msg.l(TAG, "From: " + remoteMessage.getData().get("title1"));


        SharedPreferenceHelper sharedPreferenceHelper= new SharedPreferenceHelper(getApplication());
        DataManager dataManager= new DataManager(sharedPreferenceHelper);

            String To_ID=dataManager.getCurrentChatUser();


            String fromId=remoteMessage.getData().get("from_id");
            String toID=remoteMessage.getData().get("to_id");
            String msg=remoteMessage.getNotification().getBody();
            String titie=remoteMessage.getData().get("title1");
            String fromanme=remoteMessage.getData().get("fromname");
            String fromimage=remoteMessage.getData().get("from_image");

            Payload payload= new Payload();
            payload.setFromId(fromId);
            payload.setToId(toID);
            payload.setFromName(fromanme);
            payload.setMessage(msg);
            payload.setTitle(titie);
            payload.setFromImage(fromimage);


            NotificationMsg notificationMsg= new NotificationMsg();
            notificationMsg.setPayload(payload);

            if (payload.getFromId().equals(To_ID)){

              // send broadcast
                Intent i = new Intent(Constants.MSG_BROADCAST).putExtra("data",notificationMsg);
                this.sendBroadcast(i);


            }else {

                //send notification
                Constants.COUNT++;
                sendNotification(notificationMsg.getPayload().getMessage(),notificationMsg.getPayload().getFromName(),notificationMsg.getPayload().getFromId());
                Intent i = new Intent(Constants.MSG_BROADCAST_Main).putExtra("data",notificationMsg);
                this.sendBroadcast(i);

            }



        }

//        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            Msg.l(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    //}
    // [END receive_message]

//    /**
//     * Schedule a job using FirebaseJobDispatcher.
//     */
//    private void scheduleJob() {
//        // [START dispatch_job]
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(MyJobService.class)
//                .setTag("my-job-tag")
//                .build();
//        dispatcher.schedule(myJob);
//        // [END dispatch_job]
//    }
//
//    /**
//     * Handle time allotted to BroadcastReceivers.
//     */
//    private void handleNow() {
//        Log.d(TAG, "Short lived task is done.");
//    }
//
//    /**
//     * Create and show a simple notification containing the received FCM message.
//     *
//     * @param messageBody FCM message body received.
//     */
    private void sendNotification(String messageBody,String name,String id) {

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("data",name);
        intent.putExtra("id",id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(name)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                      //  .setFullScreenIntent(pendingIntent,true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);




        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}