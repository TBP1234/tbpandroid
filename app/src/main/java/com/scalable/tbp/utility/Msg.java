package com.scalable.tbp.utility;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by shailendra on 9/10/17.
 */

public class Msg {


    public static void l( String tag,String msg){


        Log.e(tag,msg);
    }


    public static void t(Context context, String msg){

        Toast.makeText(context,msg, Toast.LENGTH_SHORT).show();

    }



}
