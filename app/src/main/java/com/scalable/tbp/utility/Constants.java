package com.scalable.tbp.utility;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

/**
 * Created by shailendra on 27/7/17.
 */

public class Constants {

    /**
     * Base Url of our application
     *
     */
    //public static final String BASE_URL="http://impetrosys.com/";
    public static final String BASE_URL="http://energiia.co.in/";
   // public static final String BASE_URL="http://energiia.co.in/tbp/webservices/push/service.php/";
   // public static final String BASE_URL="https://jsonplaceholder.typicode.com/";

    public static final String COMPANY="company";
    public static final String INFLUENCER="influencer";
    public static final String FUN_CREATE_COMPANY_PROFILE ="completeProfile";
    public static final String FUN_CREATE_INFLUENCER_PROFILE ="influencer_update_social";
    public static final String FUN_FORGOT_PASSWORD ="userforgot_password";
    public static final String FUN_FORGOT_USER_NANME ="userforgot_username";
    public static final String FUN_USER_DETAIL ="userDetail";
    public static final String FUN_ALL_USERLIST = "all_userList";
    public static final String FUN_LOGOUT = "logoutUser";
    public static final String FUN_CHANGE_PASSWORD = "userchangePassword";
    public static final String FUN_RESET_PASSWORD = "resetPassword";
    public static final String FUN_GET_COUNTY = "getCountries";
    public static final String FUN_GET_CITY = "getCities";
    public static final String FUN_EDIT_INFLUENCER_PROFILE = "edit_influenceUser";
    public static final String FUN_EDIT_COMPANY_PROFILE = "edit_companyUser";
    public static final String PREF_USERNAME = "username";
    public static final String PREF_PASSWORD = "password";
    public static final String PREF_REMEMBER_ME ="rememberMe";
    public static final String SUCCESS = "success";
    public static final String FUN_SEND_MESSAGE ="send_message";
    public static final String FUN_CHAT_LIST ="chatList";
    public static final String FUN_FIND_COMPANY ="findCompany";
    public static final String FUN_CHAT_HISTORY="chat_history";
    public static final String PREF_LOGIN_USERNAME ="LoginUserName" ;
    public static final String DEVICE_TYPE ="android" ;
    public static final String TOKEN ="token";
    public static final String FACEBOOK_PROFILE_URL ="http://facebook.com/profile.php?=";
    public static final String INSTAGRAM_PROFILE_URL ="http://instagram.com/_u/";
    public static final String MSG_BROADCAST ="broadcastMessage";
    public static final String MSG_BROADCAST_Main ="broadcastMessageMain";
    public static final String CURRENT_CHAT_USER ="CURRENTCAHTUSER";
    public static final String CompleteProfileStatus ="CompleteProfileStatus";
    public static final String DEFAULT_IMAGE = "IMAGE";
    public static final String DEFAULT_IMAGE_PATH ="IMAGE_PATH" ;
    public static final String SEND_REQUEST ="sendRequest" ;
    public static final String ACCEPT_REQUEST ="acceptRequest" ;
    public static final String REMOVE_REQUEST ="cancelRequest" ;
    public static final String BLOCK_REQUEST ="blockRequest" ;
    public static final String BLOCK_Self_REQUEST ="blockuser" ;
    public static final String SEND_REQUEST_LIST ="sendrequestList" ;
    public static final String RECIEVE_REQUEST_LIST ="userRequestlist" ;
    public static  int COUNT =0;


    public static Bitmap getCircularBitmap(Bitmap bitmap) {

        Bitmap output;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }
}
