package com.scalable.tbp.utility;

import java.util.Set;

/**
 * Created by shailendra on 12/10/17.
 */

public class DataManager implements DataManagerIntractor {


    SharedPreferenceHelper sharedPreferenceHelper;

    public DataManager(SharedPreferenceHelper sharedPreferenceHelper) {

        this.sharedPreferenceHelper = sharedPreferenceHelper;
    }

    @Override
    public void saveType(String s) {

        sharedPreferenceHelper.setSharedPreferenceString("type",s);

    }

    @Override
    public String getType() {

        String type=sharedPreferenceHelper.getSharedPreferenceString("type","");

        return type;
    }

    @Override
    public String getUserName() {

        String username=sharedPreferenceHelper.getSharedPreferenceString(Constants.PREF_USERNAME,"");
        return username;
    }

    @Override
    public String getPassword() {


        String password=sharedPreferenceHelper.getSharedPreferenceString(Constants.PREF_PASSWORD,"");

        return password;
    }


    @Override
    public void setUserId(int s) {

        sharedPreferenceHelper.setSharedPreferenceInt("userId",s);

    }

    @Override
    public void setRemeberMe(Boolean aBoolean) {

        if (aBoolean){

            sharedPreferenceHelper.setSharedPreferenceBoolean(Constants.PREF_REMEMBER_ME,true);

        }else {

            sharedPreferenceHelper.setSharedPreferenceBoolean(Constants.PREF_REMEMBER_ME,false);
        }
    }

    @Override
    public void setUserName(String userName) {

        sharedPreferenceHelper.setSharedPreferenceString(Constants.PREF_USERNAME,userName);
    }

    @Override
    public boolean getRememberMe() {

        boolean aBoolean=sharedPreferenceHelper.getSharedPreferenceBoolean(Constants.PREF_REMEMBER_ME,false);

        return aBoolean;
    }

    @Override
    public void setPassword(String password) {

        sharedPreferenceHelper.setSharedPreferenceString(Constants.PREF_PASSWORD,password);
    }

    @Override
    public int getUserId() {

        int userId=sharedPreferenceHelper.getSharedPreferenceInt("userId",0);
        return userId;

    }

    @Override
    public void setloginusername(String username) {

        sharedPreferenceHelper.setSharedPreferenceString(Constants.PREF_LOGIN_USERNAME,username);

    }

    @Override
    public String getLoginUsername() {
        String usename=sharedPreferenceHelper.getSharedPreferenceString(Constants.PREF_LOGIN_USERNAME,"");
        return usename;
    }

    @Override
    public void logOut() {

        boolean b=false;
        String username="";
        String password="";
        String token=getDeviceToken();
        if (getRememberMe()){

             username = getUserName();
             password=getPassword();
            b=getRememberMe();
        }


        sharedPreferenceHelper.clear();

        if (b){

            setPassword(password);
            setRemeberMe(b);
            setUserName(username);

        }

        setDeviceToken(token);


    }

    @Override
    public void setDeviceToken(String deviceToken) {

        sharedPreferenceHelper.setSharedPreferenceString(Constants.TOKEN,deviceToken);

    }

    @Override
    public String getDeviceToken() {
        String usename=sharedPreferenceHelper.getSharedPreferenceString(Constants.TOKEN,"");
        return usename;

    }

    @Override
    public void setCurrentChatUser(String id) {

        sharedPreferenceHelper.setSharedPreferenceString(Constants.CURRENT_CHAT_USER, id);

    }

    @Override
    public String getCurrentChatUser() {
        String chatUser=sharedPreferenceHelper.getSharedPreferenceString(Constants.CURRENT_CHAT_USER,"");
        return chatUser;
    }

    @Override
    public void setDefaultImage(String id) {
        sharedPreferenceHelper.setSharedPreferenceString(Constants.DEFAULT_IMAGE, id);

    }


    @Override
    public String getDefaultImage() {

        String defaultimage=sharedPreferenceHelper.getSharedPreferenceString(Constants.DEFAULT_IMAGE,"");
        if (defaultimage.contains("http")){

            return defaultimage;

        }else {

           String dimage=getDefaultImagePath()+defaultimage;
            return dimage;
        }



    }

    @Override
    public void setDefaultImagePath(String path) {

        sharedPreferenceHelper.setSharedPreferenceString(Constants.DEFAULT_IMAGE_PATH, path);

    }

    @Override
    public String getDefaultImagePath() {
        String path=sharedPreferenceHelper.getSharedPreferenceString(Constants.DEFAULT_IMAGE,"");
        return path;

    }

    @Override
    public void setTimeList(String time) {

        sharedPreferenceHelper.setStringSet("timelist",time);
    }

    @Override
    public Set<String> getTimeList() {
        Set<String> timelist= sharedPreferenceHelper.getStringSet("timelist","");
        return timelist;
    }


}
