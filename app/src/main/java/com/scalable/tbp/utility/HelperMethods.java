package com.scalable.tbp.utility;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by shailendra on 10/10/17.
 */

public class HelperMethods {


    public static ProgressDialog getPrgressDiloge(Context context, String msg) {


        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage(msg);
        return progressDialog;
    }

    public static String getFollowers(String followers) {

        int flw;

        try {

            flw = Integer.parseInt(followers);

        } catch (Exception e) {

            flw = 0;
        }

        String newFollowers = "";
        if (flw > 999&&flw<99999) {

            newFollowers = String.format("%.1f", (float) flw / 1000) + "K";


        }else if (flw >99999 ){

            newFollowers = String.format("%.1f", (float) flw / 1000000) + "M";

        }else if(flw == 0){
            newFollowers = "NA";
        }else {
            newFollowers = "" + flw;
        }

        return newFollowers;

    }
}
