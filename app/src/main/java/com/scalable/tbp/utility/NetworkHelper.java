package com.scalable.tbp.utility;

import com.scalable.tbp.Pojo.chatHistory.ChatHistory;
import com.scalable.tbp.Pojo.chatList.ChatList;
import com.scalable.tbp.Pojo.chatRequest.MessageRequestPojo;
import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.companyList.CompanyList;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.influecncerList.InfluecncerList;
import com.scalable.tbp.Pojo.registrationPojo.ResultPojo;
import com.scalable.tbp.Pojo.requestList.RequestListPojo;
import com.scalable.tbp.Pojo.requestList.SEndRequestListPozo;
import com.scalable.tbp.network.NetworkService;

import retrofit2.Retrofit;
import rx.Observable;

/**
 * Created by shailendra on 10/10/17.
 */

public class NetworkHelper {

    Retrofit retrofit;

    public NetworkHelper(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public Observable<ResultPojo> getSignUpdata(String data){

       return retrofit.create(NetworkService.class).getSingUpDetail(data);

    }

    public Observable<MessageRequestPojo> getChatRequest(String data){

        return retrofit.create(NetworkService.class).getMassageRequest(data);

    }
    public Observable<MessageRequestPojo> acceptChatRequest(String data){

        return retrofit.create(NetworkService.class).getMassageRequest(data);

    }

    public Observable<RequestListPojo>getRecieveRequestList(String data){
        return retrofit.create(NetworkService.class).getRecieveRequestList(data);

    }

    public Observable<MessageRequestPojo> rejectChatRequest(String data){

        return retrofit.create(NetworkService.class).getMassageRequest(data);

    }

    public Observable<MessageRequestPojo> block(String data){

        return retrofit.create(NetworkService.class).blockuser(data);

    }

    public Observable<RequestListPojo>getRequestList(String data){
        return retrofit.create(NetworkService.class).getRequestList(data);

    }

    public Observable<SEndRequestListPozo>getSendRequestList(String data){
        return retrofit.create(NetworkService.class).getSendRequestList(data);

    }

    public Observable<String> getusers(){

        return retrofit.create(NetworkService.class).getusers();

    }

    public Observable<ResultPojo> getSignIndetail(String data){

        return retrofit.create(NetworkService.class).getSignInDetail(data);

    }

    public Observable<ResultPojo> getCreatComapanyProfileDetail(String data){

        return retrofit.create(NetworkService.class).getCreatComapanyProfileDetail(data);

    }

    public Observable<ResultPojo> getInfulencerProfileDetail(String data){

        return retrofit.create(NetworkService.class).getInfulencerProfileDetail(data);

    }

    public Observable<ResultPojo> getForgotpasswordDetail(String data){

        return retrofit.create(NetworkService.class).getForgotpasswordDetail(data);

    }
    public Observable<ResultPojo> getForgotusernameDetail(String data){

        return retrofit.create(NetworkService.class).getForgotusernameDetail(data);

    }

    public Observable<ResultPojo> getUserDetail(String data){

        return retrofit.create(NetworkService.class).getUserDetail(data);

    }

    public Observable<CompanyList> getCompanyList(String data){

        return retrofit.create(NetworkService.class).getCompanyList(data);

    }

    public Observable<InfluecncerList> getInfluencerList(String data){

        return retrofit.create(NetworkService.class).getInfluencerList(data);

    }

    public Observable<ResultPojo> logout(String data){

        return retrofit.create(NetworkService.class).logout(data);

    }

    public Observable<Country> getCountries(String data){

        return retrofit.create(NetworkService.class).getCountries(data);

    }
    public Observable<City> getCities(String data){

        return retrofit.create(NetworkService.class).getCities(data);

    }
    public Observable<ResultPojo> updateCompanyProfile(String data){

        return retrofit.create(NetworkService.class).updateCompanyProfile(data);

    }

    public Observable<ResultPojo> updateInfluencerProfile(String data){

        return retrofit.create(NetworkService.class).updateInfluencerProfile(data);

    }

    public Observable<ChatHistory>getChatHistory(String data){
        return retrofit.create(NetworkService.class).getchatHistory(data);

    }

    public Observable<ResultPojo>sendmessage(String data){
        return retrofit.create(NetworkService.class).sendMessage(data);

    }

    public Observable<ChatList>getChatList(String data){
        return retrofit.create(NetworkService.class).getChatList(data);

    }

    public Observable<InfluecncerList>findInfluecncer(String data){
        return retrofit.create(NetworkService.class).findInfluecncer(data);
    }

    public Observable<CompanyList>findCompany(String data){
        return retrofit.create(NetworkService.class).findCompany(data);
    }


}
