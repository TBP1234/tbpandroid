package com.scalable.tbp.utility;

import java.util.Set;

/**
 * Created by shailendra on 12/10/17.
 */

public interface DataManagerIntractor {

    void saveType(String s);

    String getType();

    String getUserName();

    String getPassword();

    void setUserId(int s);

    void setRemeberMe(Boolean aBoolean);

    void setUserName(String userName);

    boolean getRememberMe();

    void setPassword(String password);

    int getUserId();

    void setloginusername(String username);

    String getLoginUsername();

    void logOut();

    void setDeviceToken(String deviceToken);

    String getDeviceToken();

    void setCurrentChatUser(String id);

    String getCurrentChatUser();

    void setDefaultImage(String id);

    String getDefaultImage();
    void setDefaultImagePath(String path);
    String getDefaultImagePath();
    void setTimeList(String time);
    Set<String> getTimeList();


}
