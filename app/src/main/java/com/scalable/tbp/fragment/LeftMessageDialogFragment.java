package com.scalable.tbp.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scalable.tbp.MyApplication;
import com.scalable.tbp.R;
import com.scalable.tbp.activity.ChatActivity;
import com.scalable.tbp.ui.ChatPresenterImp;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

@SuppressLint("ValidFragment")
public class LeftMessageDialogFragment extends DialogFragment {
    @BindView(R.id.btn_cnf_ok)
    Button btnCnfOk;

    Unbinder unbinder;

    @BindView(R.id.tct_first)
    TextView tctFirst;

    @BindView(R.id.txt_second)
    TextView txt_second;

    @BindView(R.id.lin_bg)
    LinearLayout linBg;
    SharedPreferenceHelper sharedPreferenceHelper;
    private DataManager dataManager;
    String msg;
    Dialog dialog;
    String type;
    ChatActivity activity;

    @SuppressLint("ValidFragment")
    public LeftMessageDialogFragment(String msg, String type, ChatActivity activity) {
        // Required empty public constructor
        this.msg=msg;
        this.type=type;
        this.activity=activity;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_left_message_dialog, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }


    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sharedPreferenceHelper = new SharedPreferenceHelper(MyApplication.get(getActivity()));
        dataManager = new DataManager(sharedPreferenceHelper);

        if (dataManager.getType().equals(Constants.COMPANY)) {

            linBg.setBackgroundColor(getResources().getColor(R.color.first));

            btnCnfOk.setTextColor(getResources().getColor(R.color.first));
        }else {
            linBg.setBackgroundColor(getResources().getColor(R.color.second));
            btnCnfOk.setTextColor(getResources().getColor(R.color.second));
        }

        tctFirst.setText(msg);

        if(type.equals("1")){

            txt_second.setText("For again login contact to admin");

        }else if(type.equals("0")){

            txt_second.setVisibility(View.GONE);
        }

        btnCnfOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //getActivity().finish();
                if(type.equals("0")){
                    dialog.dismiss();
                }else if(type.equals("1")){
                    activity.dialogOkButtonListner();
                }

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
