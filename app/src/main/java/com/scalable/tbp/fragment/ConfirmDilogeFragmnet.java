package com.scalable.tbp.fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scalable.tbp.MyApplication;
import com.scalable.tbp.R;
import com.scalable.tbp.activity.CreateCompanyProfileActivity;
import com.scalable.tbp.activity.CreateInfluencerProfileActivity;
import com.scalable.tbp.utility.Constants;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class ConfirmDilogeFragmnet extends DialogFragment {


    @BindView(R.id.btn_cnf_ok)
    Button btnCnfOk;

    Unbinder unbinder;

    SharedPreferenceHelper sharedPreferenceHelper;
    @BindView(R.id.tct_first)
    TextView tctFirst;

    @BindView(R.id.txt_second)
    TextView txtSecond;

    @BindView(R.id.txt_third)
    TextView txtThird;

    @BindView(R.id.lin_bg)
    LinearLayout linBg;

    private DataManager dataManager;
    int userId;

    @SuppressLint("ValidFragment")
    public ConfirmDilogeFragmnet(int userId) {
        this.userId=userId;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirm_diloge_fragmnet, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

         sharedPreferenceHelper = new SharedPreferenceHelper(MyApplication.get(getActivity()));
         dataManager = new DataManager(sharedPreferenceHelper);


        if (dataManager.getType().equals(Constants.COMPANY)) {

            linBg.setBackgroundColor(getResources().getColor(R.color.first));
            tctFirst.setText("You have successfully created company");
            txtSecond.setText("Please complete your profile detail on next screen");
            txtThird.setText("");
            btnCnfOk.setTextColor(getResources().getColor(R.color.first));


        }else {

            linBg.setBackgroundColor(getResources().getColor(R.color.second));
            tctFirst.setText("You have successfully created your Account");
            txtSecond.setText("Please complete your profile detail on next screen");
            txtThird.setText("");
            btnCnfOk.setTextColor(getResources().getColor(R.color.second));


        }

            btnCnfOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (dataManager.getType().equals(Constants.COMPANY)) {

                    Intent intent = new Intent(getActivity(), CreateCompanyProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("userId",userId);
                    getActivity().startActivity(intent);
                    getActivity().finish();

                } else if (dataManager.getType().equals(Constants.INFLUENCER)) {

                    Intent intent = new Intent(getActivity(), CreateInfluencerProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("userId",userId);
                    getActivity().startActivity(intent);
                    getActivity().finish();

                }

            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
