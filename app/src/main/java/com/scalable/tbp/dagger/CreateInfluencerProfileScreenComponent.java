package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.CreateInfluencerProfileActivity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = CreatInfluencerProfileSreenModule.class)
public interface CreateInfluencerProfileScreenComponent {

    void inject(CreateInfluencerProfileActivity activity);

}
