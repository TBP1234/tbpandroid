package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.SignInActvity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = SignInSreenModule.class)
public interface SignInScreenComponent {

    void inject(SignInActvity activity);

}
