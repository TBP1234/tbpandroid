package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.SignInrepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class SignInSreenModule {

    private final SignInrepository.SignInView mView;

    public SignInSreenModule( SignInrepository.SignInView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    SignInrepository.SignInView providesMainScreenContractView() {
        return mView;
    }

}
