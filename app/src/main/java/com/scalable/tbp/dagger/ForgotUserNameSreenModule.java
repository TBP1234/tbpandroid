package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.ForgotUsernamerepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class ForgotUserNameSreenModule {

    private final ForgotUsernamerepository.ForgotUsernameView mView;

    public ForgotUserNameSreenModule(ForgotUsernamerepository.ForgotUsernameView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    ForgotUsernamerepository.ForgotUsernameView providesMainScreenContractView() {
        return mView;
    }

}
