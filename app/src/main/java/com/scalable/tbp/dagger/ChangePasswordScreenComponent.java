package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.ChangePasswordActvity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = ChangePasswordSreenModule.class)
public interface ChangePasswordScreenComponent {

    void inject(ChangePasswordActvity activity);

}
