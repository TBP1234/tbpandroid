package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.Mainrepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class MainSreenModule {

    private final Mainrepository.MainView mView;

    public MainSreenModule( Mainrepository.MainView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    Mainrepository.MainView providesMainScreenContractView() {
        return mView;
    }

}
