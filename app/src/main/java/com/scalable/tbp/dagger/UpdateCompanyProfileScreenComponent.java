package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.UpdateCompanyProfileActivity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = UpdateCompanyProfileSreenModule.class)
public interface UpdateCompanyProfileScreenComponent {

    void inject(UpdateCompanyProfileActivity activity);

}
