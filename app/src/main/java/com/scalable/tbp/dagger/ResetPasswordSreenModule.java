package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.ResetPasswordrepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class ResetPasswordSreenModule {

    private final ResetPasswordrepository.ResetPasswordView resetPasswordView;

    public ResetPasswordSreenModule(ResetPasswordrepository.ResetPasswordView resetPasswordView) {
        this.resetPasswordView = resetPasswordView;
    }

    @Provides
    @CustomScope
    ResetPasswordrepository.ResetPasswordView providesMainScreenContractView() {
        return resetPasswordView;
    }
}
