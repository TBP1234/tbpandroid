package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.CreateCompanyProfileActivity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = CreatCompanyProfileSreenModule.class)
public interface CreateCompanyProfileScreenComponent {

    void inject(CreateCompanyProfileActivity activity);

}
