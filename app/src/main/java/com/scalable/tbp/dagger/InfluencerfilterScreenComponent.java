package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.InfluencerFillterActivity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = InfluencerFilterSreenModule.class)
public interface InfluencerfilterScreenComponent {

    void inject(InfluencerFillterActivity activity);

}
