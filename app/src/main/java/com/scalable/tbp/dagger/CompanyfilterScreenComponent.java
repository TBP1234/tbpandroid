package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.CompanyFiltterActivity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = ComapanyFilterSreenModule.class)
public interface CompanyfilterScreenComponent {

    void inject(CompanyFiltterActivity activity);

}
