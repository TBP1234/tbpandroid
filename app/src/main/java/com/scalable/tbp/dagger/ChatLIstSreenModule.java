package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.ChatListRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class ChatLIstSreenModule {

    private final ChatListRepository.ChatListView mView;
    public ChatLIstSreenModule(ChatListRepository.ChatListView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    ChatListRepository.ChatListView providesMainScreenContractView() {
        return mView;
    }

}
