package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.CreateCompanyProfilerepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class CreatCompanyProfileSreenModule {

    private  CreateCompanyProfilerepository.CreateCompanyProfileView mView;

    public CreatCompanyProfileSreenModule(CreateCompanyProfilerepository.CreateCompanyProfileView mView) {
        this.mView = mView;

    }

    @Provides
    @CustomScope
    CreateCompanyProfilerepository.CreateCompanyProfileView  providesMainScreenContractView() {
        return mView;

    }

}
