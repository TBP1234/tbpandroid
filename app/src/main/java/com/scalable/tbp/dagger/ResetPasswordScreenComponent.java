package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.ResetPasswordActvity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = ResetPasswordSreenModule.class)
public interface ResetPasswordScreenComponent {

    void inject(ResetPasswordActvity activity);

}
