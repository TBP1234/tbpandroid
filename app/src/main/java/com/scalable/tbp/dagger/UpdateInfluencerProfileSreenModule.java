package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.UpdateInfuencerProfilerepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class UpdateInfluencerProfileSreenModule {

    private UpdateInfuencerProfilerepository.UpdateInfuencerProfileView mView;

    public UpdateInfluencerProfileSreenModule(UpdateInfuencerProfilerepository.UpdateInfuencerProfileView mView) {
        this.mView = mView;

    }

    @Provides
    @CustomScope
    UpdateInfuencerProfilerepository.UpdateInfuencerProfileView providesMainScreenContractView() {
        return mView;

    }

}
