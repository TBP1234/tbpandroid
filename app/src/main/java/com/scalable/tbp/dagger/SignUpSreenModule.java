package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.SignUprepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class SignUpSreenModule {

    private final SignUprepository.SignUpView mView;

    public SignUpSreenModule(SignUprepository.SignUpView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    SignUprepository.SignUpView providesMainScreenContractView() {
        return mView;
    }

}
