package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.CompanyProfileRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class CompanyProfileScreenModule {

    private final CompanyProfileRepository.CompanyProfileView mView;

    public CompanyProfileScreenModule(CompanyProfileRepository.CompanyProfileView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    CompanyProfileRepository.CompanyProfileView providesMainScreenContractView() {
        return mView;
    }

}
