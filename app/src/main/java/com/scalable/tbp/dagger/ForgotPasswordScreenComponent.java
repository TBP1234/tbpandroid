package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.ForgotPasswordActvity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = ForgotPasswordSreenModule.class)
public interface ForgotPasswordScreenComponent {

    void inject(ForgotPasswordActvity activity);

}
