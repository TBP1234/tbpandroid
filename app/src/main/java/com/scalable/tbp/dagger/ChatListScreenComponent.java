package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.ChatListActivity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = ChatLIstSreenModule.class)
public interface ChatListScreenComponent {

    void inject(ChatListActivity activity);

}
