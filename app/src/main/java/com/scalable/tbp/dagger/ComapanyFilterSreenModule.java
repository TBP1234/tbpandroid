package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.CompanyFilterRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class ComapanyFilterSreenModule {

    private final CompanyFilterRepository.CompanyFilterView mView;

    public ComapanyFilterSreenModule(CompanyFilterRepository.CompanyFilterView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    CompanyFilterRepository.CompanyFilterView providesMainScreenContractView() {
        return mView;
    }

}
