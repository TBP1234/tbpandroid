package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.ForgotUserNameActvity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = ForgotUserNameSreenModule.class)
public interface ForgotUserNameScreenComponent {

    void inject(ForgotUserNameActvity activity);

}
