package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.ChangePasswordrepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class ChangePasswordSreenModule {

    private final ChangePasswordrepository.ChangePasswordView changePasswordView;

    public ChangePasswordSreenModule( ChangePasswordrepository.ChangePasswordView changePasswordView ) {
        this.changePasswordView = changePasswordView;
    }

    @Provides
    @CustomScope
    ChangePasswordrepository.ChangePasswordView providesMainScreenContractView() {
        return changePasswordView;
    }
}
