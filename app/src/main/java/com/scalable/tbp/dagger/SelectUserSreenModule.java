package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.SelectUserRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class SelectUserSreenModule {

    private final SelectUserRepository.SelectUserView mView;

    public SelectUserSreenModule(SelectUserRepository.SelectUserView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    SelectUserRepository.SelectUserView providesMainScreenContractView() {
        return mView;
    }

}
