package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.UpdateInfluencerProfileActvity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = UpdateInfluencerProfileSreenModule.class)
public interface UpdateInfluencerProfileScreenComponent {

    void inject(UpdateInfluencerProfileActvity activity);

}
