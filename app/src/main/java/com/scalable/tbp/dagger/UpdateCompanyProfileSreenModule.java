package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.UpdateCompanyProfilerepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class UpdateCompanyProfileSreenModule {

    private UpdateCompanyProfilerepository.UpdateCompanyProfileView  mView;

    public UpdateCompanyProfileSreenModule(UpdateCompanyProfilerepository.UpdateCompanyProfileView mView) {
        this.mView = mView;

    }

    @Provides
    @CustomScope
    UpdateCompanyProfilerepository.UpdateCompanyProfileView providesMainScreenContractView() {
        return mView;

    }

}
