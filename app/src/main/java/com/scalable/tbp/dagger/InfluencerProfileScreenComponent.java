package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.InfluencerProfileActivity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = InfluencerProfileScreenModule.class)
public interface InfluencerProfileScreenComponent {

    void inject(InfluencerProfileActivity activity);

}
