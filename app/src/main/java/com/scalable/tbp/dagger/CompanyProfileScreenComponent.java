package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.CompanyProfileActvity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = CompanyProfileScreenModule.class)
public interface CompanyProfileScreenComponent {

    void inject(CompanyProfileActvity activity);

}
