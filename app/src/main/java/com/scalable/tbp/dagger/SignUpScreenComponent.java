package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.SignUpActivity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = SignUpSreenModule.class)
public interface SignUpScreenComponent {

    void inject(SignUpActivity activity);

}
