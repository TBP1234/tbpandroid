package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.InluencerFilterRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class InfluencerFilterSreenModule {

    private final InluencerFilterRepository.InfluencerFilterView mView;

    public InfluencerFilterSreenModule(InluencerFilterRepository.InfluencerFilterView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    InluencerFilterRepository.InfluencerFilterView providesMainScreenContractView() {
        return mView;
    }

}
