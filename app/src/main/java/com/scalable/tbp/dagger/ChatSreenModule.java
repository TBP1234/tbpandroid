package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.ChatRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class ChatSreenModule {

    private final ChatRepository.ChatView mView;

    public ChatSreenModule( ChatRepository.ChatView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    ChatRepository.ChatView providesMainScreenContractView() {
        return mView;
    }

}
