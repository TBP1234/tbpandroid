package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.ForgotPasswordrepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class ForgotPasswordSreenModule {

    private final ForgotPasswordrepository.ForgotPasswordView mView;

    public ForgotPasswordSreenModule(ForgotPasswordrepository.ForgotPasswordView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    ForgotPasswordrepository.ForgotPasswordView providesMainScreenContractView() {
        return mView;
    }

}
