package com.scalable.tbp.dagger;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.scalable.tbp.utility.Connectivity;
import com.scalable.tbp.utility.DataManagerIntractor;
import com.scalable.tbp.utility.DataManager;
import com.scalable.tbp.utility.SharedPreferenceHelper;

import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by shailendra on 9/10/17.
 */

@Module
public class NetworkModule {


    String mBaseUrl;
    public NetworkModule(String mBaseUrl) {
        this.mBaseUrl = mBaseUrl;
    }

    @Provides
    @Singleton
    SharedPreferenceHelper providesSharedPreferences(Application application) {

        return new SharedPreferenceHelper(application);
    }

    @Provides
    @Singleton
    Cache provideHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setLenient();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    Connectivity provideConnectivity(Application Application) {
        Connectivity connectivity= new Connectivity(Application);
        return connectivity;

    }

    @Provides
    @Singleton
    DataManagerIntractor provideDatamanger(SharedPreferenceHelper sharedPreferenceHelper) {
        DataManagerIntractor dataManager= new DataManager(sharedPreferenceHelper);
        return dataManager;


    }


    @Provides
    @Singleton
    OkHttpClient provideOkhttpClient(Cache cache) {


        OkHttpClient.Builder client = new OkHttpClient.Builder().hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.addInterceptor(interceptor);
        client.cache(cache);


        OkHttpClient okHttpClient=client.build();

        return  okHttpClient;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {

        Retrofit retrofit = new Retrofit.Builder()
                 .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(mBaseUrl)
                .client(okHttpClient)
                .build();
        return retrofit;
    }
}
