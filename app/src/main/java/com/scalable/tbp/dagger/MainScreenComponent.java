package com.scalable.tbp.dagger;

import com.scalable.tbp.AppComponent;
import com.scalable.tbp.activity.MainActivity;

import dagger.Component;

/**
 * Created by shailendra on 28/7/17.
 */


@CustomScope
@Component(dependencies = AppComponent.class, modules = MainSreenModule.class)
public interface MainScreenComponent {

    void inject(MainActivity activity);

}
