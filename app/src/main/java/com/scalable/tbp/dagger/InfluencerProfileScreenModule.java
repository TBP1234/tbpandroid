package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.InfluencerProfileRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class InfluencerProfileScreenModule {
    private final InfluencerProfileRepository.InfluencerProfileView mView;

    public InfluencerProfileScreenModule(InfluencerProfileRepository.InfluencerProfileView mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    InfluencerProfileRepository.InfluencerProfileView providesMainScreenContractView() {
        return mView;
    }

}
