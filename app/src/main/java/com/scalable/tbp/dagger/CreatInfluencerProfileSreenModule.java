package com.scalable.tbp.dagger;

import com.scalable.tbp.Repository.CreateInfuencerProfilerepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shailendra on 10/10/17.
 */

@Module
public class CreatInfluencerProfileSreenModule {

    private CreateInfuencerProfilerepository.CreateInfuencerProfileView mView;

    public CreatInfluencerProfileSreenModule(CreateInfuencerProfilerepository.CreateInfuencerProfileView mView) {
        this.mView = mView;

    }

    @Provides
    @CustomScope
    CreateInfuencerProfilerepository.CreateInfuencerProfileView providesMainScreenContractView() {
        return mView;

    }

}
