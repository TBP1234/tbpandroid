package com.scalable.tbp.Repository;

import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.influecncerList.InfluecncerList;

import java.util.List;

/**
 * Created by shailendra on 6/10/17.
 */

public interface InluencerFilterRepository {

     interface InfluencerFilterView extends BaseView {

         void clearEditText();
         void moveToCompanyProfileView();
         void moveToInfluencerProfileView();
         void moveToChatView();
         void setCountry(Country country, List list);
         void showCountry();
         void setCity(City city, List list);
         void showCity();
         void moveToMainView(InfluecncerList  influecncerList);


     }

    interface InfluencerFilterPresenter{

        void doSearchButtonClick(String name,String country,String city,int gender,String insta,String face,String snap);
        void getCountry();
        void getCity(String id);


    }

}
