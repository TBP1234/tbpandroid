package com.scalable.tbp.Repository;

import com.scalable.tbp.Pojo.registrationPojo.Datum;

/**
 * Created by shailendra on 6/10/17.
 */

public interface CompanyProfileRepository {

     interface CompanyProfileView  extends BaseView{

         void clearEditText();
         void showData(Datum datum);
         void showlogEdit();
         void moveToLoginView();
         void moveToEditProfileView(Datum datum);
         void moveToChangePasswordView();
         void moveToChatView(String name);
         void moveToChatListView();

         void showInfluencerColor();

         void showComapanyColor();

     }

    interface CompanyProfilePresenter{


        void fetchaUserdetail(String uid);
        void logOut();
        void GoChangePassword();
        void dochatButtonClicked(String id);
        void dosetColor();

    }

}
