package com.scalable.tbp.Repository;

import java.util.List;

/**
 * Created by shailendra on 6/10/17.
 */

public interface ChatListRepository {

    interface ChatListView extends BaseView {
        void showChat(List<Object> arrayList);

        void clearEdittext();

        void moveToInfluencerProfileView();

        void moveToComapanyProfileView();

        void showInfluencerColor();

        void showComapanyColor();
    }

    interface ChatListPresenter {

        void doProfileButtunClicked();

        void fetchChatData();

        void setColor();

        void sendRequestList();
        void recieveRequestList();
    }
}
