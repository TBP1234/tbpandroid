package com.scalable.tbp.Repository;

import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.county.Country;
import com.scalable.tbp.Pojo.registrationPojo.Datum;

import java.util.List;

/**
 * Created by shailendra on 6/10/17.
 */

public interface UpdateCompanyProfilerepository {

     interface UpdateCompanyProfileView extends BaseView {

         void clearEditText();
         void moveToDashboardView();
         void setCountry(Country country, List list);
         void showCountry();
         void setCity(City city, List list);
         void showData(Datum datum);
         void showCity();


     }

    interface UpdateCompanyProfilePresenter{

        void doUpdate(String name, String lastname, String companyName, String image, String typeOfproduct, String country, String city, String about, String branch, String countryCode, String cityCode);
        void setType(String type);
        void geyType();
        void getCountry();
        void getStates(String id);
        void getCity(String id);
    }

}
