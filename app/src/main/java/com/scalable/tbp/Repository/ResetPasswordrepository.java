package com.scalable.tbp.Repository;

/**
 * Created by shailendra on 6/10/17.
 */

public interface ResetPasswordrepository {

     interface  ResetPasswordView extends BaseView {

         void clearEditText();
         void moveToSignInView();
         void setcomapanyColor();
         void setInfluencerColor();
         void init();

     }

    interface  ResetPasswordPresenter{

        void doChangePasswordName(String password,String otp,String email);
        void dosetColor();

    }

}
