package com.scalable.tbp.Repository;

import android.app.Dialog;

import com.scalable.tbp.Pojo.registrationPojo.Datum;

import java.util.ArrayList;

/**
 * Created by shailendra on 6/10/17.
 */

public interface ChatRepository {

    interface ChatView extends BaseView {

        void moveToDashboardView(String s, Datum datum);

        void showChat(ArrayList<Object> arrayList);

        void moveToCompanyProfileView();

        void clearEdittext();

        void moveToInfluencerProfileView();

        void moveToChatListView();

        void showInfluencerColor();

        void showComapanyColor();
        void layoutVisiblechat();
        void layoutvisiblesendrequest();
        void layoutvisiblependingrequest();
        void layoutvisibleacceptrequest();
        void getPendingRequestMessage();
        void moveToLoginView();
        void showCustomDialog(String msg);
        void returnedBlockIdFromHistory(String id);



    }

    interface ChatPresenter {

        void doprofleButtunClicked();

        void doChatButtunClicked();

        void doSendButtunClicked(String to_id, String msg);

        void fetchChatData(String id);
        void sendChatRequest(String to_id);

        void setColor();

        void setChatUser(String id);
        void acceptChatRequest(String id);
        void ignoreChatRequest(String id);
        void blockSelf();
        void logout();
        void blockUser(String id, String type, Dialog dialog,String msg);


    }
}
