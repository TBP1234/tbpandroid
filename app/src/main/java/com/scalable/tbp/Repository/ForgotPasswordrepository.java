package com.scalable.tbp.Repository;

/**
 * Created by shailendra on 6/10/17.
 */

public interface ForgotPasswordrepository {

     interface ForgotPasswordView extends BaseView {

         void clearEditText();
         void moveToSignInView();
         void moveToresetpasswordView(String email);

     }

    interface ForgotPasswordPresenter{

        void doforgotPassword(String email);
        void setType(String type);
        void geyType();
    }

}
