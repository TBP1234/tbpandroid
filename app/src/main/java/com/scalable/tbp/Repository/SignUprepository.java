package com.scalable.tbp.Repository;

/**
 * Created by shailendra on 6/10/17.
 */

public interface SignUprepository {

     interface SignUpView extends BaseView {

         void clearEditText();
         void moveToConfimView(int userId);
         void InfoMsg(String infMsg);
         void setColor(int color);
         void openWebView();


     }

    interface SignUpPresenter{

        void doSignUp(String type ,String uname,String password,String email);
        void setInfoMsg();
        void setColor();


    }

}
