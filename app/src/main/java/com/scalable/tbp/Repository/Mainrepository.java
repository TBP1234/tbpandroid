package com.scalable.tbp.Repository;

import java.util.List;

/**
 * Created by shailendra on 6/10/17.
 */

public interface Mainrepository {

     interface MainView extends BaseView {

         void clearEditText();
         void moveToCompanyProfileView();
         void moveToInfluencerProfileView();
         void moveToCompanyFilterView();
         void moveToInfluencerFilterView();
         void moveToChatView();
         void showLIst(List<Object> list,int type);
         void showInfluencerColor();
         void showComapanyColor();
         void updateCount();
     }

    interface MainPresenter{

        void getObjectList();
        void doProfile();
        void dosetColor();
        void dochatButtonClick();
        void doFilterButtonClick();


     }

}
