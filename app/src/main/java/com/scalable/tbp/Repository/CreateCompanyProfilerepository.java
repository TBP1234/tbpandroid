package com.scalable.tbp.Repository;

import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.county.Country;

import java.util.List;

/**
 * Created by shailendra on 6/10/17.
 */

public interface CreateCompanyProfilerepository {

     interface CreateCompanyProfileView extends BaseView {

         void clearEditText();
         void moveToDashboardView();
         void setCountry(Country country, List list);
         void showCountry();
         void setCity(City city,List list);
         void showCity();

     }

    interface CreateCompanyProfilePresenter{

        void dosave(String name,String lastname,String companyName,String image,String typeOfproduct,String country,String city,String about ,String branch,String countryCode,String cityCode,int userId);
        void setType(String type);
        void geyType();
        void getCountry();
        void getStates(String id);
        void getCity(String id);
    }

}
