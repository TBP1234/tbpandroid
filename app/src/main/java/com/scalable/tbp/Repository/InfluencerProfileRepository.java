package com.scalable.tbp.Repository;

import com.scalable.tbp.Pojo.registrationPojo.Datum;

/**
 * Created by shailendra on 6/10/17.
 */

public interface InfluencerProfileRepository {

    interface InfluencerProfileView extends BaseView {

        void showData(Datum datum);

        void moveToLoginView();

        void moveToEditProfileView(Datum datum);

        void moveToChangePasswordView();

        void moveToChatView();

        void moveToChatListView();

        void showLogEdit();

        void showInfluencerColor();

        void showComapanyColor();

        void showWebUrl(String url);

    }

    interface InfluencerProfilePresenter {

        void fetchaUserdetail(String s);

        void logOut();

        void goToChangePassword();

        void setColor();

        void dochatButtonClicked(String id);

        void doInstaProfileButtonClicked(Datum id);

        void dofaceProfileButtonClicked(Datum id);

        void dosnapProfileButtonClicked(Datum id);

    }

}
