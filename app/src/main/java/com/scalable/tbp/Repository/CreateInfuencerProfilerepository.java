package com.scalable.tbp.Repository;

import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.county.Country;

import java.util.List;

/**
 * Created by shailendra on 6/10/17.
 */

public interface CreateInfuencerProfilerepository {

     interface CreateInfuencerProfileView extends BaseView {

         void clearEditText();
         void moveToDashboardView();
         void setCountry(Country country, List list);
         void showCountry();
         void setCity(City city, List list);
         void showCity();


     }

    interface CreateInfuencerProfilePresenter{

        void dosave(String name,String lastName,int gender,String instagram,String facebook,String snapchat,String country,String city,String about,String image,String countryCode,String cityCode,String facebbokurl,String instaUrl,int userId);
        void setType(String type);
        void geyType();
        void getCountry();
        void getCity(String id);

    }

}
