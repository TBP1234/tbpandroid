package com.scalable.tbp.Repository;

import com.scalable.tbp.Pojo.city.City;
import com.scalable.tbp.Pojo.companyList.CompanyList;
import com.scalable.tbp.Pojo.county.Country;

import java.util.List;

/**
 * Created by shailendra on 6/10/17.
 */

public interface CompanyFilterRepository {

     interface CompanyFilterView extends BaseView {

         void clearEditText();
         void setCountry(Country country, List list);
         void showCountry();
         void setCity(City city, List list);
         void showCity();

         void moveToMainView(CompanyList companyList);

     }

    interface CompanyFilterPresenter{

        void doSearchButtonClick(String name,String country,String city,String branch);
        void getCountry();
        void getCity(String id);
    }

}
