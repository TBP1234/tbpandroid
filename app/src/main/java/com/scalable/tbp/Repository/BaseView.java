package com.scalable.tbp.Repository;

/**
 * Created by shailendra on 10/10/17.
 */

interface BaseView {

    void showProgressbar();
    void hideProgressbar();
    void showError(String s);
    void init();
    void bindevent();

}
