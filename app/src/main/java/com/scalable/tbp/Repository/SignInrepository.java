package com.scalable.tbp.Repository;

import com.scalable.tbp.Pojo.registrationPojo.Datum;

/**
 * Created by shailendra on 6/10/17.
 */

public interface SignInrepository {

     interface SignInView extends BaseView {

         void clearEditText();
         void moveToDashboardView(String s, Datum datum);
         void moveToForgotPassWordView();
         void moveToForgotUsernameView();
         void moveToSignUPView();
         void moveToCompanyCreatProfileView(int userId);
         void moveToInfluencerCreatProfileView(int userId);
         void showTypeCompany();
         void showTypeInfluencer();
         void setrememberMe(Boolean aBoolean);
         void setUserPassword(String username,String password);
         void setColor(int color);

     }

    interface SignInPresenter{

        void doSignIn(String uname, String password);
        void setType(String type);
        void geyType();
        void setRememberMe(Boolean aBoolean);
        void showRememberMe();
        void setColor();
    }

}
