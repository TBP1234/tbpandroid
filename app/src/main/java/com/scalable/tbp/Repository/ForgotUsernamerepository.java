package com.scalable.tbp.Repository;

/**
 * Created by shailendra on 6/10/17.
 */

public interface ForgotUsernamerepository {

     interface  ForgotUsernameView extends BaseView {

         void clearEditText();
         void moveToSignInView();


     }

    interface  ForgotUsernamePresenter{

        void doforgotUserName(String email);
        void setType(String type);
        void geyType();
    }

}
