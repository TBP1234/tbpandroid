package com.scalable.tbp.Repository;

/**
 * Created by shailendra on 6/10/17.
 */

public interface SelectUserRepository {

     interface SelectUserView  extends BaseView{

         void moveToSignInView();
     }

    interface SelectUserPresenter{


        void doSignInclick(String type);


    }

}
