package com.scalable.tbp.Repository;

/**
 * Created by shailendra on 6/10/17.
 */

public interface ChangePasswordrepository {

     interface  ChangePasswordView extends BaseView {

         void clearEditText();
         void moveToSignInView();
         void setcomapanyColor();
         void setInfluencerColor();
         void init();

     }

    interface  ChangePasswordPresenter{

        void doChangePasswordName(String password);
        void dosetColor();

    }

}
